start:
	vendor/bin/sculpin generate --watch --server

generate:
	vendor/bin/sculpin generate --env=prod --output-dir=htdocs

deploy:
	git push origin master
	git push gandi master
	ssh 63639@git.sd6.gpaas.net deploy games.anthony-dessalles.com.git