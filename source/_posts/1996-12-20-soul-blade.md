---
title: "Soul Blade"
slug: "soul-blade"
post_date: "20/12/1996"
files:
playlists:
  84:
    title: "Soul Blade PS1 2020"
    publishedAt: "12/09/2020"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyK2cSUdyNrtHBmnNxWhRuFo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS1"
updates:
  - "Soul Blade PS1 2020"
---
{% include 'article.html' %}
