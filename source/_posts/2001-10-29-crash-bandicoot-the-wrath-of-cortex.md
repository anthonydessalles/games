---
title: "Crash Bandicoot The Wrath of Cortex"
french_title: "Crash Bandicoot La Vengeance de Cortex"
slug: "crash-bandicoot-the-wrath-of-cortex"
post_date: "29/10/2001"
files:
playlists:
  1018:
    title: "Crash Bandicoot La Vengeance de Cortex XB 2020"
    publishedAt: "20/01/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyIXQTTuwg9HzbJ7DQbonLi9" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "XB"
updates:
  - "Crash Bandicoot La Vengeance de Cortex XB 2020"
---
{% include 'article.html' %}
