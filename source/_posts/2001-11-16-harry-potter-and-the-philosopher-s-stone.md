---
title: "Harry Potter and the Philosopher's Stone"
french_title: "Harry Potter à l'École des Sorciers"
slug: "harry-potter-and-the-philosopher-s-stone"
post_date: "16/11/2001"
files:
  27359:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215128.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215128.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27360:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215147.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215147.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27361:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215206.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215206.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27362:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215218.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215218.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27363:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215233.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215233.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27364:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215244.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215244.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27365:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215258.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215258.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27366:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215316.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215316.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27367:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215325.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215325.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27368:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215336.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215336.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27369:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215352.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215352.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27370:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215405.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215405.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27371:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215420.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215420.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27372:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215432.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215432.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27373:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215443.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215443.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27374:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215454.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215454.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27375:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215503.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215503.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27376:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215512.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215512.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27377:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215526.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215526.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27378:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215536.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215536.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27379:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215545.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215545.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27380:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215556.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215556.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27381:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215611.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215611.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27382:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215625.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215625.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27383:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215644.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215644.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27384:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215659.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215659.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27385:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215727.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215727.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27386:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215738.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215738.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27387:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215749.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215749.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27388:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215801.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215801.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27389:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215812.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215812.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27390:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215821.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215821.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27391:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215830.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215830.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27392:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215840.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215840.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27393:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215850.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215850.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27394:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215859.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215859.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27395:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215910.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215910.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27396:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215946.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215946.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27397:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-215954.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-215954.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27398:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-220005.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-220005.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27399:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-220015.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-220015.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27400:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-220029.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-220029.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27401:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-220040.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-220040.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27402:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-220056.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-220056.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27403:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-220107.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-220107.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
  27404:
    filename: "Harry Potter and the Sorcerer's Stone (USA)-230525-220117.png"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Harry Potter à l'École des Sorciers PS1 2023 - Screenshots/Harry Potter and the Sorcerer's Stone (USA)-230525-220117.png"
    path: "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
playlists:
tags:
  - "TonyTonyX042"
categories:
  - "PS1"
updates:
  - "Harry Potter à l'École des Sorciers PS1 2023 - Screenshots"
---
{% include 'article.html' %}
