---
title: "Lego Batman 2 DC Super Heroes"
slug: "lego-batman-2-dc-super-heroes"
post_date: "19/06/2012"
files:
playlists:
  2068:
    title: "Lego Batman 2 DC Super Heroes XB360 2023"
    publishedAt: "27/06/2023"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJwFiAuWw5QVi_55nJUgIwO" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "DC"
  - "XB360"
updates:
  - "Lego Batman 2 DC Super Heroes XB360 2023"
---
{% include 'article.html' %}
