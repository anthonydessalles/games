---
title: "Tomb Raider Legend"
slug: "tomb-raider-legend"
post_date: "07/04/2006"
files:
  68313:
    filename: "Screenshot 2025-02-07 135158.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider Legend XB360 2025 - Screenshots/Screenshot 2025-02-07 135158.png"
    path: "Tomb Raider Legend XB360 2025 - Screenshots"
  68314:
    filename: "Screenshot 2025-02-07 135215.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider Legend XB360 2025 - Screenshots/Screenshot 2025-02-07 135215.png"
    path: "Tomb Raider Legend XB360 2025 - Screenshots"
  68315:
    filename: "Screenshot 2025-02-07 135225.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider Legend XB360 2025 - Screenshots/Screenshot 2025-02-07 135225.png"
    path: "Tomb Raider Legend XB360 2025 - Screenshots"
  68316:
    filename: "Screenshot 2025-02-07 135237.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider Legend XB360 2025 - Screenshots/Screenshot 2025-02-07 135237.png"
    path: "Tomb Raider Legend XB360 2025 - Screenshots"
  68317:
    filename: "Screenshot 2025-02-07 135315.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider Legend XB360 2025 - Screenshots/Screenshot 2025-02-07 135315.png"
    path: "Tomb Raider Legend XB360 2025 - Screenshots"
  68318:
    filename: "Screenshot 2025-02-07 135343.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider Legend XB360 2025 - Screenshots/Screenshot 2025-02-07 135343.png"
    path: "Tomb Raider Legend XB360 2025 - Screenshots"
  68319:
    filename: "Screenshot 2025-02-07 135435.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider Legend XB360 2025 - Screenshots/Screenshot 2025-02-07 135435.png"
    path: "Tomb Raider Legend XB360 2025 - Screenshots"
  68320:
    filename: "Screenshot 2025-02-07 135453.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider Legend XB360 2025 - Screenshots/Screenshot 2025-02-07 135453.png"
    path: "Tomb Raider Legend XB360 2025 - Screenshots"
  68321:
    filename: "Screenshot 2025-02-07 135504.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider Legend XB360 2025 - Screenshots/Screenshot 2025-02-07 135504.png"
    path: "Tomb Raider Legend XB360 2025 - Screenshots"
playlists:
  2284:
    title: "Tomb Raider Legend Steam 2023"
    publishedAt: "23/11/2023"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLzSmDs-45PG4dqXIi7LyO9" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "XB360"
  - "Steam"
updates:
  - "Tomb Raider Legend XB360 2025 - Screenshots"
  - "Tomb Raider Legend Steam 2023"
---
{% include 'article.html' %}