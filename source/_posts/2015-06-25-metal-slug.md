---
title: "Metal Slug"
slug: "metal-slug"
post_date: "25/06/2015"
files:
playlists:
  830:
    title: "Metal Slug Steam 2020"
    publishedAt: "02/12/2020"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJv5-ZUAJQpEgjB0dUP41h4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "Steam"
updates:
  - "Metal Slug Steam 2020"
---
{% include 'article.html' %}
