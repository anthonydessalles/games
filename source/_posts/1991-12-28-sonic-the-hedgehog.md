---
title: "Sonic the Hedgehog"
slug: "sonic-the-hedgehog"
post_date: "28/12/1991"
files:
  8772:
    filename: "Sonic the Hedgehog-201117-115628.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog MD 2020 - Screenshots/Sonic the Hedgehog-201117-115628.png"
    path: "Sonic the Hedgehog MD 2020 - Screenshots"
  8773:
    filename: "Sonic the Hedgehog-201117-115634.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog MD 2020 - Screenshots/Sonic the Hedgehog-201117-115634.png"
    path: "Sonic the Hedgehog MD 2020 - Screenshots"
  8774:
    filename: "Sonic the Hedgehog-201117-115640.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog MD 2020 - Screenshots/Sonic the Hedgehog-201117-115640.png"
    path: "Sonic the Hedgehog MD 2020 - Screenshots"
  8775:
    filename: "Sonic the Hedgehog-201117-115651.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog MD 2020 - Screenshots/Sonic the Hedgehog-201117-115651.png"
    path: "Sonic the Hedgehog MD 2020 - Screenshots"
  8776:
    filename: "Sonic the Hedgehog-201117-115656.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog MD 2020 - Screenshots/Sonic the Hedgehog-201117-115656.png"
    path: "Sonic the Hedgehog MD 2020 - Screenshots"
  8777:
    filename: "Sonic the Hedgehog-201117-115728.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog MD 2020 - Screenshots/Sonic the Hedgehog-201117-115728.png"
    path: "Sonic the Hedgehog MD 2020 - Screenshots"
  8778:
    filename: "Sonic the Hedgehog-201117-115738.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog MD 2020 - Screenshots/Sonic the Hedgehog-201117-115738.png"
    path: "Sonic the Hedgehog MD 2020 - Screenshots"
  8779:
    filename: "Sonic the Hedgehog-201117-115752.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog MD 2020 - Screenshots/Sonic the Hedgehog-201117-115752.png"
    path: "Sonic the Hedgehog MD 2020 - Screenshots"
  8780:
    filename: "Sonic the Hedgehog-201117-115834.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog MD 2020 - Screenshots/Sonic the Hedgehog-201117-115834.png"
    path: "Sonic the Hedgehog MD 2020 - Screenshots"
  8781:
    filename: "Sonic the Hedgehog-201117-115839.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog MD 2020 - Screenshots/Sonic the Hedgehog-201117-115839.png"
    path: "Sonic the Hedgehog MD 2020 - Screenshots"
  8782:
    filename: "Sonic the Hedgehog-201117-115849.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog MD 2020 - Screenshots/Sonic the Hedgehog-201117-115849.png"
    path: "Sonic the Hedgehog MD 2020 - Screenshots"
  8783:
    filename: "Sonic the Hedgehog-201117-115857.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog MD 2020 - Screenshots/Sonic the Hedgehog-201117-115857.png"
    path: "Sonic the Hedgehog MD 2020 - Screenshots"
  8784:
    filename: "Sonic the Hedgehog-201117-115906.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog MD 2020 - Screenshots/Sonic the Hedgehog-201117-115906.png"
    path: "Sonic the Hedgehog MD 2020 - Screenshots"
  8785:
    filename: "Sonic the Hedgehog-201117-115912.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog MD 2020 - Screenshots/Sonic the Hedgehog-201117-115912.png"
    path: "Sonic the Hedgehog MD 2020 - Screenshots"
  8786:
    filename: "Sonic the Hedgehog-201117-115920.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog MD 2020 - Screenshots/Sonic the Hedgehog-201117-115920.png"
    path: "Sonic the Hedgehog MD 2020 - Screenshots"
playlists:
tags:
  - "TonyTonyX042"
categories:
  - "MD"
updates:
  - "Sonic the Hedgehog MD 2020 - Screenshots"
---
{% include 'article.html' %}
