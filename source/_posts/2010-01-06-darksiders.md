---
title: "Darksiders"
slug: "darksiders"
post_date: "06/01/2010"
files:
playlists:
  2297:
    title: "Darksiders Steam 2023"
    publishedAt: "20/11/2023"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyIGrPM80r6z4kS3XPaM2KOV" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "Steam"
updates:
  - "Darksiders Steam 2023"
---
{% include 'article.html' %}