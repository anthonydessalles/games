---
title: "Tenchu Wrath of Heaven"
french_title: "Tenchu La Colère Divine"
slug: "tenchu-wrath-of-heaven"
post_date: "03/03/2003"
files:
playlists:
  1657:
    title: "Tenchu La Colère Divine PS2 2022"
    publishedAt: "17/05/2022"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJ-zvrJgSdjIVg-lYqj85jc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS2"
updates:
  - "Tenchu La Colère Divine PS2 2022"
---
{% include 'article.html' %}
