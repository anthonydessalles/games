---
title: "Halo Spartan Assault"
slug: "halo-spartan-assault"
post_date: "18/07/2013"
files:
playlists:
  2289:
    title: "Halo Spartan Assault Steam 2023"
    publishedAt: "22/11/2023"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLlV5wCgbTjJxgqbyQNYa9w" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "Steam"
updates:
  - "Halo Spartan Assault Steam 2023"
---
{% include 'article.html' %}