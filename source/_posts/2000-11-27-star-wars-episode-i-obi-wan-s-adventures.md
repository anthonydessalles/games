---
title: "Star Wars Episode I Obi-Wan's Adventures"
slug: "star-wars-episode-i-obi-wan-s-adventures"
post_date: "27/11/2000"
files:
playlists:
  1332:
    title: "Star Wars Episode I Obi-Wan's Adventures GBC 2021"
    publishedAt: "01/12/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLpwiJAaYBBo3-eaWo3NVjo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "GBC"
updates:
  - "Star Wars Episode I Obi-Wan's Adventures GBC 2021"
---
{% include 'article.html' %}
