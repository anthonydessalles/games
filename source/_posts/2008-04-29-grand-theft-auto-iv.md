---
title: "Grand Theft Auto IV"
slug: "grand-theft-auto-iv"
post_date: "29/04/2008"
files:
  23584:
    filename: "2022_11_30_7_53_43.bmp"
    date: "30/11/2022"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Grand Theft Auto IV PS3 2022 - Screenshots/2022_11_30_7_53_43.bmp"
    path: "Grand Theft Auto IV PS3 2022 - Screenshots"
  23585:
    filename: "2022_11_30_7_54_10.bmp"
    date: "30/11/2022"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Grand Theft Auto IV PS3 2022 - Screenshots/2022_11_30_7_54_10.bmp"
    path: "Grand Theft Auto IV PS3 2022 - Screenshots"
  23586:
    filename: "2022_11_30_7_54_24.bmp"
    date: "30/11/2022"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Grand Theft Auto IV PS3 2022 - Screenshots/2022_11_30_7_54_24.bmp"
    path: "Grand Theft Auto IV PS3 2022 - Screenshots"
  23587:
    filename: "2022_11_30_7_54_36.bmp"
    date: "30/11/2022"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Grand Theft Auto IV PS3 2022 - Screenshots/2022_11_30_7_54_36.bmp"
    path: "Grand Theft Auto IV PS3 2022 - Screenshots"
  23588:
    filename: "2022_11_30_7_55_14.bmp"
    date: "30/11/2022"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Grand Theft Auto IV PS3 2022 - Screenshots/2022_11_30_7_55_14.bmp"
    path: "Grand Theft Auto IV PS3 2022 - Screenshots"
playlists:
  38:
    title: "Grand Theft Auto IV PS3 2020"
    publishedAt: "12/09/2020"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyKqInjt9csmQD0fWtqQwZN0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS3"
updates:
  - "Grand Theft Auto IV PS3 2022 - Screenshots"
  - "Grand Theft Auto IV PS3 2020"
---
{% include 'article.html' %}
