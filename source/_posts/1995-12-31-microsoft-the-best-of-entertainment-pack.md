---
title: "Microsoft The Best of Entertainment Pack"
slug: "microsoft-the-best-of-entertainment-pack"
post_date: "31/12/1995"
files:
playlists:
  1333:
    title: "Microsoft The Best of Entertainment Pack GBC 2021"
    publishedAt: "01/12/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJprAHwLfK53VfpPY-oTpYj" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "GBC"
updates:
  - "Microsoft The Best of Entertainment Pack GBC 2021"
---
{% include 'article.html' %}
