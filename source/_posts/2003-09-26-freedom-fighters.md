---
title: "Freedom Fighters"
slug: "freedom-fighters"
post_date: "26/09/2003"
files:
playlists:
  834:
    title: "Freedom Fighters Steam 2020"
    publishedAt: "02/12/2020"
    itemsCount: "3"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJZR6IaBYFxbJXY08DZ0KZZ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "Steam"
updates:
  - "Freedom Fighters Steam 2020"
---
{% include 'article.html' %}
