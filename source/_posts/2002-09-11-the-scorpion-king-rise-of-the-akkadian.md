---
title: "The Scorpion King Rise of the Akkadian"
french_title: "Le Roi Scorpion L'Ascension de L'Akkadien"
slug: "the-scorpion-king-rise-of-the-akkadian"
post_date: "11/09/2002"
files:
playlists:
  999:
    title: "Le Roi Scorpion L'Ascension de L'Akkadien GC 2020"
    publishedAt: "21/01/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJ9aGTGlLtKow2IC4JR1BO8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "GC"
updates:
  - "Le Roi Scorpion L'Ascension de L'Akkadien GC 2020"
---
{% include 'article.html' %}
