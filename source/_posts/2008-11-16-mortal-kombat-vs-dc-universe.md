---
title: "Mortal Kombat vs DC Universe"
slug: "mortal-kombat-vs-dc-universe"
post_date: "16/11/2008"
files:
playlists:
  993:
    title: "Mortal Kombat vs DC Universe PS3 2020"
    publishedAt: "21/01/2021"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyKxj5TwNPwSHB5F5-Ab9Shj" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  2045:
    title: "Mortal Kombat vs DC Universe PS3 2023"
    publishedAt: "27/06/2023"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJlAwI-xBHYZaSfn84xvux5" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "DC"
  - "PS3"
updates:
  - "Mortal Kombat vs DC Universe PS3 2020"
  - "Mortal Kombat vs DC Universe PS3 2023"
---
{% include 'article.html' %}
