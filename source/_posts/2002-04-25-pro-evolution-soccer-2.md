---
title: "Pro Evolution Soccer 2"
slug: "pro-evolution-soccer-2"
post_date: "25/04/2002"
files:
playlists:
  988:
    title: "Pro Evolution Soccer 2 PS2 2020"
    publishedAt: "22/01/2021"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyK7WgG8dhjDD6s9WW-THk45" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS2"
updates:
  - "Pro Evolution Soccer 2 PS2 2020"
---
{% include 'article.html' %}
