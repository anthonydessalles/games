---
title: "The Legend of Zelda The Wind Waker"
slug: "the-legend-of-zelda-the-wind-waker"
post_date: "13/12/2002"
files:
playlists:
  970:
    title: "The Legend of Zelda The Wind Waker GC 2020"
    publishedAt: "25/01/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLyh7OmtEi0rQKU2luLRaQv" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "GC"
updates:
  - "The Legend of Zelda The Wind Waker GC 2020"
---
{% include 'article.html' %}
