---
title: "Legends of Wrestling II"
slug: "legends-of-wrestling-ii"
post_date: "25/11/2002"
files:
playlists:
  1002:
    title: "Legends of Wrestling II PS2 2020"
    publishedAt: "21/01/2021"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyKxGA0-2uvpH3QjAtKBP5PI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS2"
updates:
  - "Legends of Wrestling II PS2 2020"
---
{% include 'article.html' %}
