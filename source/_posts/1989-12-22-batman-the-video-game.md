---
title: "Batman The Video Game"
slug: "batman-the-video-game"
post_date: "22/12/1989"
files:
  5101:
    filename: "Batman The Video Game-201113-172010.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game GB 2020 - Screenshots/Batman The Video Game-201113-172010.png"
    path: "Batman The Video Game GB 2020 - Screenshots"
  5102:
    filename: "Batman The Video Game-201113-172022.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game GB 2020 - Screenshots/Batman The Video Game-201113-172022.png"
    path: "Batman The Video Game GB 2020 - Screenshots"
  5103:
    filename: "Batman The Video Game-201113-172034.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game GB 2020 - Screenshots/Batman The Video Game-201113-172034.png"
    path: "Batman The Video Game GB 2020 - Screenshots"
  5104:
    filename: "Batman The Video Game-201113-172059.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game GB 2020 - Screenshots/Batman The Video Game-201113-172059.png"
    path: "Batman The Video Game GB 2020 - Screenshots"
  5105:
    filename: "Batman The Video Game-201113-172108.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game GB 2020 - Screenshots/Batman The Video Game-201113-172108.png"
    path: "Batman The Video Game GB 2020 - Screenshots"
  5106:
    filename: "Batman The Video Game-201113-172135.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game GB 2020 - Screenshots/Batman The Video Game-201113-172135.png"
    path: "Batman The Video Game GB 2020 - Screenshots"
  5107:
    filename: "Batman The Video Game-201113-172228.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game GB 2020 - Screenshots/Batman The Video Game-201113-172228.png"
    path: "Batman The Video Game GB 2020 - Screenshots"
  5108:
    filename: "Batman The Video Game-201113-172324.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game GB 2020 - Screenshots/Batman The Video Game-201113-172324.png"
    path: "Batman The Video Game GB 2020 - Screenshots"
  5109:
    filename: "Batman The Video Game-201113-172425.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game GB 2020 - Screenshots/Batman The Video Game-201113-172425.png"
    path: "Batman The Video Game GB 2020 - Screenshots"
  5110:
    filename: "Batman The Video Game-201113-172450.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game GB 2020 - Screenshots/Batman The Video Game-201113-172450.png"
    path: "Batman The Video Game GB 2020 - Screenshots"
  5111:
    filename: "Batman The Video Game-201113-172457.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game GB 2020 - Screenshots/Batman The Video Game-201113-172457.png"
    path: "Batman The Video Game GB 2020 - Screenshots"
  5112:
    filename: "Batman The Video Game-201113-172612.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game GB 2020 - Screenshots/Batman The Video Game-201113-172612.png"
    path: "Batman The Video Game GB 2020 - Screenshots"
  5113:
    filename: "Batman The Video Game-201113-172740.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game GB 2020 - Screenshots/Batman The Video Game-201113-172740.png"
    path: "Batman The Video Game GB 2020 - Screenshots"
  5114:
    filename: "Batman The Video Game-201113-172751.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game GB 2020 - Screenshots/Batman The Video Game-201113-172751.png"
    path: "Batman The Video Game GB 2020 - Screenshots"
  5115:
    filename: "Batman The Video Game-201113-172758.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game GB 2020 - Screenshots/Batman The Video Game-201113-172758.png"
    path: "Batman The Video Game GB 2020 - Screenshots"
  5116:
    filename: "Batman The Video Game-201113-172812.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game GB 2020 - Screenshots/Batman The Video Game-201113-172812.png"
    path: "Batman The Video Game GB 2020 - Screenshots"
  5117:
    filename: "Batman The Video Game-201113-172827.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game GB 2020 - Screenshots/Batman The Video Game-201113-172827.png"
    path: "Batman The Video Game GB 2020 - Screenshots"
  5118:
    filename: "Batman The Video Game-201113-172851.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game GB 2020 - Screenshots/Batman The Video Game-201113-172851.png"
    path: "Batman The Video Game GB 2020 - Screenshots"
  5119:
    filename: "Batman The Video Game-201113-172956.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game GB 2020 - Screenshots/Batman The Video Game-201113-172956.png"
    path: "Batman The Video Game GB 2020 - Screenshots"
  5120:
    filename: "Batman The Video Game-201113-173025.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game GB 2020 - Screenshots/Batman The Video Game-201113-173025.png"
    path: "Batman The Video Game GB 2020 - Screenshots"
  5121:
    filename: "Batman The Video Game-201116-162731.png"
    date: "16/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game MD 2020 - Screenshots/Batman The Video Game-201116-162731.png"
    path: "Batman The Video Game MD 2020 - Screenshots"
  5122:
    filename: "Batman The Video Game-201116-162742.png"
    date: "16/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game MD 2020 - Screenshots/Batman The Video Game-201116-162742.png"
    path: "Batman The Video Game MD 2020 - Screenshots"
  5123:
    filename: "Batman The Video Game-201116-162758.png"
    date: "16/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game MD 2020 - Screenshots/Batman The Video Game-201116-162758.png"
    path: "Batman The Video Game MD 2020 - Screenshots"
  5124:
    filename: "Batman The Video Game-201116-162803.png"
    date: "16/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game MD 2020 - Screenshots/Batman The Video Game-201116-162803.png"
    path: "Batman The Video Game MD 2020 - Screenshots"
  5125:
    filename: "Batman The Video Game-201116-162811.png"
    date: "16/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game MD 2020 - Screenshots/Batman The Video Game-201116-162811.png"
    path: "Batman The Video Game MD 2020 - Screenshots"
  5126:
    filename: "Batman The Video Game-201116-162817.png"
    date: "16/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game MD 2020 - Screenshots/Batman The Video Game-201116-162817.png"
    path: "Batman The Video Game MD 2020 - Screenshots"
  5127:
    filename: "Batman The Video Game-201116-162829.png"
    date: "16/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game MD 2020 - Screenshots/Batman The Video Game-201116-162829.png"
    path: "Batman The Video Game MD 2020 - Screenshots"
  5128:
    filename: "Batman The Video Game-201116-162835.png"
    date: "16/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game MD 2020 - Screenshots/Batman The Video Game-201116-162835.png"
    path: "Batman The Video Game MD 2020 - Screenshots"
  5129:
    filename: "Batman The Video Game-201116-162849.png"
    date: "16/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game MD 2020 - Screenshots/Batman The Video Game-201116-162849.png"
    path: "Batman The Video Game MD 2020 - Screenshots"
  5130:
    filename: "Batman The Video Game-201116-162920.png"
    date: "16/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game MD 2020 - Screenshots/Batman The Video Game-201116-162920.png"
    path: "Batman The Video Game MD 2020 - Screenshots"
  5131:
    filename: "Batman The Video Game-201116-162932.png"
    date: "16/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game MD 2020 - Screenshots/Batman The Video Game-201116-162932.png"
    path: "Batman The Video Game MD 2020 - Screenshots"
  5132:
    filename: "Batman The Video Game-201116-162956.png"
    date: "16/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game MD 2020 - Screenshots/Batman The Video Game-201116-162956.png"
    path: "Batman The Video Game MD 2020 - Screenshots"
  5133:
    filename: "Batman The Video Game-201116-163003.png"
    date: "16/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game MD 2020 - Screenshots/Batman The Video Game-201116-163003.png"
    path: "Batman The Video Game MD 2020 - Screenshots"
  5134:
    filename: "Batman The Video Game-201116-163015.png"
    date: "16/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game MD 2020 - Screenshots/Batman The Video Game-201116-163015.png"
    path: "Batman The Video Game MD 2020 - Screenshots"
  25537:
    filename: "Batman The Video Game-230331-110106.png"
    date: "31/03/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game NES 2023 - Screenshots/Batman The Video Game-230331-110106.png"
    path: "Batman The Video Game NES 2023 - Screenshots"
  25538:
    filename: "Batman The Video Game-230331-110115.png"
    date: "31/03/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game NES 2023 - Screenshots/Batman The Video Game-230331-110115.png"
    path: "Batman The Video Game NES 2023 - Screenshots"
  25539:
    filename: "Batman The Video Game-230331-110124.png"
    date: "31/03/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game NES 2023 - Screenshots/Batman The Video Game-230331-110124.png"
    path: "Batman The Video Game NES 2023 - Screenshots"
  25540:
    filename: "Batman The Video Game-230331-110134.png"
    date: "31/03/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game NES 2023 - Screenshots/Batman The Video Game-230331-110134.png"
    path: "Batman The Video Game NES 2023 - Screenshots"
  25541:
    filename: "Batman The Video Game-230331-110143.png"
    date: "31/03/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game NES 2023 - Screenshots/Batman The Video Game-230331-110143.png"
    path: "Batman The Video Game NES 2023 - Screenshots"
  25542:
    filename: "Batman The Video Game-230331-110156.png"
    date: "31/03/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game NES 2023 - Screenshots/Batman The Video Game-230331-110156.png"
    path: "Batman The Video Game NES 2023 - Screenshots"
  25543:
    filename: "Batman The Video Game-230331-110253.png"
    date: "31/03/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game NES 2023 - Screenshots/Batman The Video Game-230331-110253.png"
    path: "Batman The Video Game NES 2023 - Screenshots"
  25544:
    filename: "Batman The Video Game-230331-110548.png"
    date: "31/03/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game NES 2023 - Screenshots/Batman The Video Game-230331-110548.png"
    path: "Batman The Video Game NES 2023 - Screenshots"
  25545:
    filename: "Batman The Video Game-230331-110611.png"
    date: "31/03/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game NES 2023 - Screenshots/Batman The Video Game-230331-110611.png"
    path: "Batman The Video Game NES 2023 - Screenshots"
  25546:
    filename: "Batman The Video Game-230331-110626.png"
    date: "31/03/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Video Game NES 2023 - Screenshots/Batman The Video Game-230331-110626.png"
    path: "Batman The Video Game NES 2023 - Screenshots"
playlists:
  749:
    title: "Batman The Video Game GB 2020"
    publishedAt: "06/12/2020"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyKKeOWM4VgUttFKfO2WE-LG" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "GB"
  - "MD"
  - "NES"
updates:
  - "Batman The Video Game GB 2020 - Screenshots"
  - "Batman The Video Game MD 2020 - Screenshots"
  - "Batman The Video Game NES 2023 - Screenshots"
  - "Batman The Video Game GB 2020"
---
{% include 'article.html' %}
