---
title: "Rocky Balboa"
slug: "rocky-balboa"
post_date: "26/01/2007"
files:
  8660:
    filename: "Rocky Balboa-201120-183910.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-183910.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8661:
    filename: "Rocky Balboa-201120-183920.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-183920.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8662:
    filename: "Rocky Balboa-201120-183926.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-183926.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8663:
    filename: "Rocky Balboa-201120-183935.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-183935.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8664:
    filename: "Rocky Balboa-201120-183943.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-183943.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8665:
    filename: "Rocky Balboa-201120-184009.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184009.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8666:
    filename: "Rocky Balboa-201120-184018.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184018.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8667:
    filename: "Rocky Balboa-201120-184029.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184029.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8668:
    filename: "Rocky Balboa-201120-184040.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184040.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8669:
    filename: "Rocky Balboa-201120-184049.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184049.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8670:
    filename: "Rocky Balboa-201120-184103.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184103.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8671:
    filename: "Rocky Balboa-201120-184116.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184116.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8672:
    filename: "Rocky Balboa-201120-184126.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184126.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8673:
    filename: "Rocky Balboa-201120-184143.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184143.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8674:
    filename: "Rocky Balboa-201120-184152.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184152.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8675:
    filename: "Rocky Balboa-201120-184201.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184201.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8676:
    filename: "Rocky Balboa-201120-184211.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184211.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8677:
    filename: "Rocky Balboa-201120-184221.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184221.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8678:
    filename: "Rocky Balboa-201120-184237.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184237.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8679:
    filename: "Rocky Balboa-201120-184248.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184248.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8680:
    filename: "Rocky Balboa-201120-184306.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184306.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8681:
    filename: "Rocky Balboa-201120-184318.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184318.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8682:
    filename: "Rocky Balboa-201120-184349.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184349.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8683:
    filename: "Rocky Balboa-201120-184357.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184357.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8684:
    filename: "Rocky Balboa-201120-184406.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184406.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8685:
    filename: "Rocky Balboa-201120-184415.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184415.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8686:
    filename: "Rocky Balboa-201120-184426.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184426.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8687:
    filename: "Rocky Balboa-201120-184505.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184505.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8688:
    filename: "Rocky Balboa-201120-184521.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184521.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8689:
    filename: "Rocky Balboa-201120-184530.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184530.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8690:
    filename: "Rocky Balboa-201120-184540.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184540.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8691:
    filename: "Rocky Balboa-201120-184630.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184630.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8692:
    filename: "Rocky Balboa-201120-184647.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184647.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8693:
    filename: "Rocky Balboa-201120-184704.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184704.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8694:
    filename: "Rocky Balboa-201120-184718.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184718.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8695:
    filename: "Rocky Balboa-201120-184736.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184736.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8696:
    filename: "Rocky Balboa-201120-184756.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184756.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8697:
    filename: "Rocky Balboa-201120-184809.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184809.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8698:
    filename: "Rocky Balboa-201120-184825.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184825.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8699:
    filename: "Rocky Balboa-201120-184837.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184837.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8700:
    filename: "Rocky Balboa-201120-184847.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184847.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8701:
    filename: "Rocky Balboa-201120-184857.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184857.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8702:
    filename: "Rocky Balboa-201120-184906.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184906.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
  8703:
    filename: "Rocky Balboa-201120-184939.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Rocky Balboa PSP 2020 - Screenshots/Rocky Balboa-201120-184939.png"
    path: "Rocky Balboa PSP 2020 - Screenshots"
playlists:
tags:
  - "TonyTonyX042"
categories:
  - "PSP"
updates:
  - "Rocky Balboa PSP 2020 - Screenshots"
---
{% include 'article.html' %}
