---
title: "WWE All Stars"
slug: "wwe-all-stars"
post_date: "29/03/2011"
files:
playlists:
  965:
    title: "WWE All Stars Wii 2020"
    publishedAt: "25/01/2021"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyL1lfU3CSSHvPem9KvYJrpa" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "Wii"
updates:
  - "WWE All Stars Wii 2020"
---
{% include 'article.html' %}
