---
title: "Worms Open Warfare"
slug: "worms-open-warfare"
post_date: "17/03/2006"
files:
  27327:
    filename: "Worms Open Warfare PSP 20230525 ULES00268_00001.jpg"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Worms Open Warfare PSP 2023 - Screenshots/Worms Open Warfare PSP 20230525 ULES00268_00001.jpg"
    path: "Worms Open Warfare PSP 2023 - Screenshots"
  27328:
    filename: "Worms Open Warfare PSP 20230525 ULES00268_00002.jpg"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Worms Open Warfare PSP 2023 - Screenshots/Worms Open Warfare PSP 20230525 ULES00268_00002.jpg"
    path: "Worms Open Warfare PSP 2023 - Screenshots"
  27329:
    filename: "Worms Open Warfare PSP 20230525 ULES00268_00003.jpg"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Worms Open Warfare PSP 2023 - Screenshots/Worms Open Warfare PSP 20230525 ULES00268_00003.jpg"
    path: "Worms Open Warfare PSP 2023 - Screenshots"
  27330:
    filename: "Worms Open Warfare PSP 20230525 ULES00268_00004.jpg"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Worms Open Warfare PSP 2023 - Screenshots/Worms Open Warfare PSP 20230525 ULES00268_00004.jpg"
    path: "Worms Open Warfare PSP 2023 - Screenshots"
  27331:
    filename: "Worms Open Warfare PSP 20230525 ULES00268_00005.jpg"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Worms Open Warfare PSP 2023 - Screenshots/Worms Open Warfare PSP 20230525 ULES00268_00005.jpg"
    path: "Worms Open Warfare PSP 2023 - Screenshots"
  27332:
    filename: "Worms Open Warfare PSP 20230525 ULES00268_00006.jpg"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Worms Open Warfare PSP 2023 - Screenshots/Worms Open Warfare PSP 20230525 ULES00268_00006.jpg"
    path: "Worms Open Warfare PSP 2023 - Screenshots"
  27333:
    filename: "Worms Open Warfare PSP 20230525 ULES00268_00007.jpg"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Worms Open Warfare PSP 2023 - Screenshots/Worms Open Warfare PSP 20230525 ULES00268_00007.jpg"
    path: "Worms Open Warfare PSP 2023 - Screenshots"
  27334:
    filename: "Worms Open Warfare PSP 20230525 ULES00268_00008.jpg"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Worms Open Warfare PSP 2023 - Screenshots/Worms Open Warfare PSP 20230525 ULES00268_00008.jpg"
    path: "Worms Open Warfare PSP 2023 - Screenshots"
  27335:
    filename: "Worms Open Warfare PSP 20230525 ULES00268_00009.jpg"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Worms Open Warfare PSP 2023 - Screenshots/Worms Open Warfare PSP 20230525 ULES00268_00009.jpg"
    path: "Worms Open Warfare PSP 2023 - Screenshots"
  27336:
    filename: "Worms Open Warfare PSP 20230525 ULES00268_00010.jpg"
    date: "25/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Worms Open Warfare PSP 2023 - Screenshots/Worms Open Warfare PSP 20230525 ULES00268_00010.jpg"
    path: "Worms Open Warfare PSP 2023 - Screenshots"
playlists:
tags:
  - "TonyTonyX042"
categories:
  - "PSP"
updates:
  - "Worms Open Warfare PSP 2023 - Screenshots"
---
{% include 'article.html' %}
