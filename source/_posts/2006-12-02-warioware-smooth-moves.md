---
title: "WarioWare Smooth Moves"
slug: "warioware-smooth-moves"
post_date: "02/12/2006"
files:
playlists:
  1244:
    title: "WarioWare Smooth Moves Wii 2021"
    publishedAt: "20/09/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJ7gWwAfbmW9ZQhoT38UPdt" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "Wii"
updates:
  - "WarioWare Smooth Moves Wii 2021"
---
{% include 'article.html' %}
