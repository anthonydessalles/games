---
title: "Batman Return of the Joker"
slug: "batman-return-of-the-joker"
post_date: "20/12/1991"
files:
  5061:
    filename: "Batman Return of the Joker-201205-174947.png"
    date: "05/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman Return of the Joker GB 2020 - Screenshots/Batman Return of the Joker-201205-174947.png"
    path: "Batman Return of the Joker GB 2020 - Screenshots"
  5062:
    filename: "Batman Return of the Joker-201205-174959.png"
    date: "05/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman Return of the Joker GB 2020 - Screenshots/Batman Return of the Joker-201205-174959.png"
    path: "Batman Return of the Joker GB 2020 - Screenshots"
  5063:
    filename: "Batman Return of the Joker-201205-175009.png"
    date: "05/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman Return of the Joker GB 2020 - Screenshots/Batman Return of the Joker-201205-175009.png"
    path: "Batman Return of the Joker GB 2020 - Screenshots"
  5064:
    filename: "Batman Return of the Joker-201205-175020.png"
    date: "05/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman Return of the Joker GB 2020 - Screenshots/Batman Return of the Joker-201205-175020.png"
    path: "Batman Return of the Joker GB 2020 - Screenshots"
  5065:
    filename: "Batman Return of the Joker-201205-175033.png"
    date: "05/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman Return of the Joker GB 2020 - Screenshots/Batman Return of the Joker-201205-175033.png"
    path: "Batman Return of the Joker GB 2020 - Screenshots"
  25505:
    filename: "Batman Return of the Joker-230330-155143.png"
    date: "30/03/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman Return of the Joker NES 2023 - Screenshots/Batman Return of the Joker-230330-155143.png"
    path: "Batman Return of the Joker NES 2023 - Screenshots"
  25506:
    filename: "Batman Return of the Joker-230330-155247.png"
    date: "30/03/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman Return of the Joker NES 2023 - Screenshots/Batman Return of the Joker-230330-155247.png"
    path: "Batman Return of the Joker NES 2023 - Screenshots"
  25507:
    filename: "Batman Return of the Joker-230330-155257.png"
    date: "30/03/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman Return of the Joker NES 2023 - Screenshots/Batman Return of the Joker-230330-155257.png"
    path: "Batman Return of the Joker NES 2023 - Screenshots"
  25508:
    filename: "Batman Return of the Joker-230330-155316.png"
    date: "30/03/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman Return of the Joker NES 2023 - Screenshots/Batman Return of the Joker-230330-155316.png"
    path: "Batman Return of the Joker NES 2023 - Screenshots"
  25509:
    filename: "Batman Return of the Joker-230330-155405.png"
    date: "30/03/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman Return of the Joker NES 2023 - Screenshots/Batman Return of the Joker-230330-155405.png"
    path: "Batman Return of the Joker NES 2023 - Screenshots"
  25510:
    filename: "Batman Return of the Joker-230330-155430.png"
    date: "30/03/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman Return of the Joker NES 2023 - Screenshots/Batman Return of the Joker-230330-155430.png"
    path: "Batman Return of the Joker NES 2023 - Screenshots"
  25511:
    filename: "Batman Return of the Joker-230330-155444.png"
    date: "30/03/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman Return of the Joker NES 2023 - Screenshots/Batman Return of the Joker-230330-155444.png"
    path: "Batman Return of the Joker NES 2023 - Screenshots"
  25512:
    filename: "Batman Return of the Joker-230330-155507.png"
    date: "30/03/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman Return of the Joker NES 2023 - Screenshots/Batman Return of the Joker-230330-155507.png"
    path: "Batman Return of the Joker NES 2023 - Screenshots"
  25513:
    filename: "Batman Return of the Joker-230330-155515.png"
    date: "30/03/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman Return of the Joker NES 2023 - Screenshots/Batman Return of the Joker-230330-155515.png"
    path: "Batman Return of the Joker NES 2023 - Screenshots"
playlists:
  813:
    title: "Batman Return of the Joker GB 2020"
    publishedAt: "05/12/2020"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLNoCNfYeBJs9cKGuCnssbP" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "GB"
  - "NES"
updates:
  - "Batman Return of the Joker GB 2020 - Screenshots"
  - "Batman Return of the Joker NES 2023 - Screenshots"
  - "Batman Return of the Joker GB 2020"
---
{% include 'article.html' %}
