---
title: "God of War III"
slug: "god-of-war-iii"
post_date: "16/03/2010"
files:
playlists:
  1006:
    title: "God of War III PS3 2020"
    publishedAt: "21/01/2021"
    itemsCount: "3"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLpqaLoIvAicXZNi3cZk4VI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS3"
updates:
  - "God of War III PS3 2020"
---
{% include 'article.html' %}
