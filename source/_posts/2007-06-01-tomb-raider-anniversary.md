---
title: "Tomb Raider Anniversary"
slug: "tomb-raider-anniversary"
post_date: "01/06/2007"
files:
  68296:
    filename: "Screenshot 2025-02-07 151652.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider Anniversary XB360 2025 - Screenshots/Screenshot 2025-02-07 151652.png"
    path: "Tomb Raider Anniversary XB360 2025 - Screenshots"
  68297:
    filename: "Screenshot 2025-02-07 151726.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider Anniversary XB360 2025 - Screenshots/Screenshot 2025-02-07 151726.png"
    path: "Tomb Raider Anniversary XB360 2025 - Screenshots"
  68298:
    filename: "Screenshot 2025-02-07 151735.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider Anniversary XB360 2025 - Screenshots/Screenshot 2025-02-07 151735.png"
    path: "Tomb Raider Anniversary XB360 2025 - Screenshots"
  68299:
    filename: "Screenshot 2025-02-07 151750.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider Anniversary XB360 2025 - Screenshots/Screenshot 2025-02-07 151750.png"
    path: "Tomb Raider Anniversary XB360 2025 - Screenshots"
  68300:
    filename: "Screenshot 2025-02-07 151810.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider Anniversary XB360 2025 - Screenshots/Screenshot 2025-02-07 151810.png"
    path: "Tomb Raider Anniversary XB360 2025 - Screenshots"
  68301:
    filename: "Screenshot 2025-02-07 151845.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider Anniversary XB360 2025 - Screenshots/Screenshot 2025-02-07 151845.png"
    path: "Tomb Raider Anniversary XB360 2025 - Screenshots"
  68302:
    filename: "Screenshot 2025-02-07 151915.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider Anniversary XB360 2025 - Screenshots/Screenshot 2025-02-07 151915.png"
    path: "Tomb Raider Anniversary XB360 2025 - Screenshots"
  68303:
    filename: "Screenshot 2025-02-07 151935.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider Anniversary XB360 2025 - Screenshots/Screenshot 2025-02-07 151935.png"
    path: "Tomb Raider Anniversary XB360 2025 - Screenshots"
  68304:
    filename: "Screenshot 2025-02-07 151951.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider Anniversary XB360 2025 - Screenshots/Screenshot 2025-02-07 151951.png"
    path: "Tomb Raider Anniversary XB360 2025 - Screenshots"
  68305:
    filename: "Screenshot 2025-02-07 152023.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider Anniversary XB360 2025 - Screenshots/Screenshot 2025-02-07 152023.png"
    path: "Tomb Raider Anniversary XB360 2025 - Screenshots"
  68306:
    filename: "Screenshot 2025-02-07 152048.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider Anniversary XB360 2025 - Screenshots/Screenshot 2025-02-07 152048.png"
    path: "Tomb Raider Anniversary XB360 2025 - Screenshots"
  68307:
    filename: "Screenshot 2025-02-07 152102.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider Anniversary XB360 2025 - Screenshots/Screenshot 2025-02-07 152102.png"
    path: "Tomb Raider Anniversary XB360 2025 - Screenshots"
  68308:
    filename: "Screenshot 2025-02-07 152133.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider Anniversary XB360 2025 - Screenshots/Screenshot 2025-02-07 152133.png"
    path: "Tomb Raider Anniversary XB360 2025 - Screenshots"
  68309:
    filename: "Screenshot 2025-02-07 152203.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider Anniversary XB360 2025 - Screenshots/Screenshot 2025-02-07 152203.png"
    path: "Tomb Raider Anniversary XB360 2025 - Screenshots"
  68310:
    filename: "Screenshot 2025-02-07 152226.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider Anniversary XB360 2025 - Screenshots/Screenshot 2025-02-07 152226.png"
    path: "Tomb Raider Anniversary XB360 2025 - Screenshots"
  68311:
    filename: "Screenshot 2025-02-07 152249.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider Anniversary XB360 2025 - Screenshots/Screenshot 2025-02-07 152249.png"
    path: "Tomb Raider Anniversary XB360 2025 - Screenshots"
  68312:
    filename: "Screenshot 2025-02-07 152301.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider Anniversary XB360 2025 - Screenshots/Screenshot 2025-02-07 152301.png"
    path: "Tomb Raider Anniversary XB360 2025 - Screenshots"
playlists:
tags:
  - "TonyTonyX042"
categories:
  - "XB360"
updates:
  - "Tomb Raider Anniversary XB360 2025 - Screenshots"
---
{% include 'article.html' %}