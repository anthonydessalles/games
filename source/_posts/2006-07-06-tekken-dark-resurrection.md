---
title: "Tekken Dark Resurrection"
slug: "tekken-dark-resurrection"
post_date: "06/07/2006"
files:
  10826:
    filename: "Tekken Dark Resurrection-201120-202924.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-202924.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10827:
    filename: "Tekken Dark Resurrection-201120-202933.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-202933.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10828:
    filename: "Tekken Dark Resurrection-201120-202939.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-202939.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10829:
    filename: "Tekken Dark Resurrection-201120-202949.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-202949.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10830:
    filename: "Tekken Dark Resurrection-201120-203004.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-203004.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10831:
    filename: "Tekken Dark Resurrection-201120-203031.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-203031.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10832:
    filename: "Tekken Dark Resurrection-201120-203039.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-203039.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10833:
    filename: "Tekken Dark Resurrection-201120-203116.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-203116.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10834:
    filename: "Tekken Dark Resurrection-201120-203125.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-203125.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10835:
    filename: "Tekken Dark Resurrection-201120-203133.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-203133.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10836:
    filename: "Tekken Dark Resurrection-201120-203141.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-203141.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10837:
    filename: "Tekken Dark Resurrection-201120-203151.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-203151.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10838:
    filename: "Tekken Dark Resurrection-201120-203157.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-203157.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10839:
    filename: "Tekken Dark Resurrection-201120-203209.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-203209.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10840:
    filename: "Tekken Dark Resurrection-201120-203216.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-203216.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10841:
    filename: "Tekken Dark Resurrection-201120-203228.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-203228.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10842:
    filename: "Tekken Dark Resurrection-201120-203248.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-203248.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10843:
    filename: "Tekken Dark Resurrection-201120-203255.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-203255.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10844:
    filename: "Tekken Dark Resurrection-201120-203308.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-203308.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10845:
    filename: "Tekken Dark Resurrection-201120-203320.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-203320.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10846:
    filename: "Tekken Dark Resurrection-201120-203332.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-203332.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10847:
    filename: "Tekken Dark Resurrection-201120-203345.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-203345.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10848:
    filename: "Tekken Dark Resurrection-201120-203354.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-203354.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10849:
    filename: "Tekken Dark Resurrection-201120-203406.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-203406.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10850:
    filename: "Tekken Dark Resurrection-201120-203421.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-203421.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10851:
    filename: "Tekken Dark Resurrection-201120-203440.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-203440.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10852:
    filename: "Tekken Dark Resurrection-201120-203455.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-203455.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10853:
    filename: "Tekken Dark Resurrection-201120-203504.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-203504.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  10854:
    filename: "Tekken Dark Resurrection-201120-203516.png"
    date: "20/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2020 - Screenshots/Tekken Dark Resurrection-201120-203516.png"
    path: "Tekken Dark Resurrection PSP 2020 - Screenshots"
  67174:
    filename: "202412161626 (01).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (01).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67175:
    filename: "202412161626 (02).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (02).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67176:
    filename: "202412161626 (03).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (03).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67177:
    filename: "202412161626 (04).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (04).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67178:
    filename: "202412161626 (05).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (05).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67179:
    filename: "202412161626 (06).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (06).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67180:
    filename: "202412161626 (07).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (07).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67181:
    filename: "202412161626 (08).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (08).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67182:
    filename: "202412161626 (09).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (09).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67183:
    filename: "202412161626 (10).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (10).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67184:
    filename: "202412161626 (11).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (11).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67185:
    filename: "202412161626 (12).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (12).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67186:
    filename: "202412161626 (13).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (13).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67187:
    filename: "202412161626 (14).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (14).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67188:
    filename: "202412161626 (15).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (15).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67189:
    filename: "202412161626 (16).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (16).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67190:
    filename: "202412161626 (17).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (17).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67191:
    filename: "202412161626 (18).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (18).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67192:
    filename: "202412161626 (19).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (19).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67193:
    filename: "202412161626 (20).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (20).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67194:
    filename: "202412161626 (21).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (21).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67195:
    filename: "202412161626 (22).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (22).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67196:
    filename: "202412161626 (23).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (23).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67197:
    filename: "202412161626 (24).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (24).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67198:
    filename: "202412161626 (25).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (25).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67199:
    filename: "202412161626 (26).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (26).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67200:
    filename: "202412161626 (27).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (27).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67201:
    filename: "202412161626 (28).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (28).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67202:
    filename: "202412161626 (29).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (29).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67203:
    filename: "202412161626 (30).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (30).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67204:
    filename: "202412161626 (31).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (31).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67205:
    filename: "202412161626 (32).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (32).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67206:
    filename: "202412161626 (33).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (33).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67207:
    filename: "202412161626 (34).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (34).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67208:
    filename: "202412161626 (35).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (35).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67209:
    filename: "202412161626 (36).png"
    date: "16/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412161626 (36).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67322:
    filename: "202412171243 (01).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (01).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67323:
    filename: "202412171243 (02).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (02).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67324:
    filename: "202412171243 (03).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (03).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67325:
    filename: "202412171243 (04).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (04).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67326:
    filename: "202412171243 (05).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (05).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67327:
    filename: "202412171243 (06).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (06).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67328:
    filename: "202412171243 (07).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (07).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67329:
    filename: "202412171243 (08).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (08).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67330:
    filename: "202412171243 (09).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (09).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67331:
    filename: "202412171243 (10).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (10).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67332:
    filename: "202412171243 (11).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (11).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67333:
    filename: "202412171243 (12).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (12).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67334:
    filename: "202412171243 (13).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (13).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67335:
    filename: "202412171243 (14).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (14).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67336:
    filename: "202412171243 (15).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (15).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67337:
    filename: "202412171243 (16).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (16).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67338:
    filename: "202412171243 (17).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (17).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67339:
    filename: "202412171243 (18).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (18).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67340:
    filename: "202412171243 (19).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (19).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67341:
    filename: "202412171243 (20).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (20).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67342:
    filename: "202412171243 (21).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (21).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67343:
    filename: "202412171243 (22).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (22).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67344:
    filename: "202412171243 (23).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (23).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67345:
    filename: "202412171243 (24).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (24).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67346:
    filename: "202412171243 (25).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (25).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67347:
    filename: "202412171243 (26).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (26).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67348:
    filename: "202412171243 (27).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (27).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67349:
    filename: "202412171243 (28).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (28).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67350:
    filename: "202412171243 (29).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (29).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67351:
    filename: "202412171243 (30).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (30).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67352:
    filename: "202412171243 (31).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (31).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67353:
    filename: "202412171243 (32).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (32).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67354:
    filename: "202412171243 (33).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (33).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67355:
    filename: "202412171243 (34).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (34).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67356:
    filename: "202412171243 (35).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (35).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67357:
    filename: "202412171243 (36).png"
    date: "17/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412171243 (36).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67430:
    filename: "202412181300 (01).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (01).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67431:
    filename: "202412181300 (02).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (02).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67432:
    filename: "202412181300 (03).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (03).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67433:
    filename: "202412181300 (04).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (04).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67434:
    filename: "202412181300 (05).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (05).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67435:
    filename: "202412181300 (06).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (06).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67436:
    filename: "202412181300 (07).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (07).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67437:
    filename: "202412181300 (08).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (08).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67438:
    filename: "202412181300 (09).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (09).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67439:
    filename: "202412181300 (10).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (10).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67440:
    filename: "202412181300 (11).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (11).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67441:
    filename: "202412181300 (12).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (12).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67442:
    filename: "202412181300 (13).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (13).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67443:
    filename: "202412181300 (14).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (14).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67444:
    filename: "202412181300 (15).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (15).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67445:
    filename: "202412181300 (16).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (16).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67446:
    filename: "202412181300 (17).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (17).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67447:
    filename: "202412181300 (18).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (18).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67448:
    filename: "202412181300 (19).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (19).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67449:
    filename: "202412181300 (20).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (20).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67450:
    filename: "202412181300 (21).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (21).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67451:
    filename: "202412181300 (22).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (22).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67452:
    filename: "202412181300 (23).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (23).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67453:
    filename: "202412181300 (24).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (24).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67454:
    filename: "202412181300 (25).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (25).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67455:
    filename: "202412181300 (26).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (26).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67456:
    filename: "202412181300 (27).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (27).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67457:
    filename: "202412181300 (28).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (28).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67458:
    filename: "202412181300 (29).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (29).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67459:
    filename: "202412181300 (30).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (30).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67460:
    filename: "202412181300 (31).png"
    date: "18/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412181300 (31).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67564:
    filename: "202412201256 (01).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (01).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67565:
    filename: "202412201256 (02).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (02).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67566:
    filename: "202412201256 (03).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (03).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67567:
    filename: "202412201256 (04).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (04).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67568:
    filename: "202412201256 (05).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (05).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67569:
    filename: "202412201256 (06).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (06).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67570:
    filename: "202412201256 (07).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (07).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67571:
    filename: "202412201256 (08).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (08).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67572:
    filename: "202412201256 (09).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (09).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67573:
    filename: "202412201256 (10).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (10).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67574:
    filename: "202412201256 (11).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (11).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67575:
    filename: "202412201256 (12).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (12).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67576:
    filename: "202412201256 (13).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (13).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67577:
    filename: "202412201256 (14).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (14).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67578:
    filename: "202412201256 (15).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (15).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67579:
    filename: "202412201256 (16).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (16).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67580:
    filename: "202412201256 (17).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (17).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67581:
    filename: "202412201256 (18).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (18).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67582:
    filename: "202412201256 (19).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (19).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67583:
    filename: "202412201256 (20).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (20).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67584:
    filename: "202412201256 (21).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (21).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67585:
    filename: "202412201256 (22).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (22).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67586:
    filename: "202412201256 (23).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (23).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67587:
    filename: "202412201256 (24).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (24).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67588:
    filename: "202412201256 (25).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (25).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67589:
    filename: "202412201256 (26).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (26).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67590:
    filename: "202412201256 (27).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (27).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67591:
    filename: "202412201256 (28).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (28).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67592:
    filename: "202412201256 (29).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (29).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67593:
    filename: "202412201256 (30).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (30).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67594:
    filename: "202412201256 (31).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (31).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67595:
    filename: "202412201256 (32).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (32).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67596:
    filename: "202412201256 (33).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (33).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67597:
    filename: "202412201256 (34).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (34).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67598:
    filename: "202412201256 (35).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (35).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67599:
    filename: "202412201256 (36).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (36).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67600:
    filename: "202412201256 (37).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (37).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67601:
    filename: "202412201256 (38).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (38).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67602:
    filename: "202412201256 (39).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (39).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67603:
    filename: "202412201256 (40).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (40).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67604:
    filename: "202412201256 (41).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (41).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67605:
    filename: "202412201256 (42).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (42).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67606:
    filename: "202412201256 (43).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (43).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67607:
    filename: "202412201256 (44).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (44).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67608:
    filename: "202412201256 (45).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (45).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67609:
    filename: "202412201256 (46).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (46).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67610:
    filename: "202412201256 (47).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (47).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67611:
    filename: "202412201256 (48).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (48).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67612:
    filename: "202412201256 (49).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (49).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67613:
    filename: "202412201256 (50).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (50).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67614:
    filename: "202412201256 (51).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (51).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67615:
    filename: "202412201256 (52).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (52).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67616:
    filename: "202412201256 (53).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (53).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67617:
    filename: "202412201256 (54).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (54).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67618:
    filename: "202412201256 (55).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (55).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67619:
    filename: "202412201256 (56).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (56).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67620:
    filename: "202412201256 (57).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (57).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67621:
    filename: "202412201256 (58).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (58).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67622:
    filename: "202412201256 (59).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (59).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67623:
    filename: "202412201256 (60).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (60).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67624:
    filename: "202412201256 (61).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (61).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67625:
    filename: "202412201256 (62).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (62).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67626:
    filename: "202412201256 (63).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (63).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67627:
    filename: "202412201256 (64).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (64).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67628:
    filename: "202412201256 (65).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (65).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67629:
    filename: "202412201256 (66).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (66).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67630:
    filename: "202412201256 (67).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (67).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67631:
    filename: "202412201256 (68).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (68).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67632:
    filename: "202412201256 (69).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (69).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67633:
    filename: "202412201256 (70).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (70).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67634:
    filename: "202412201256 (71).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (71).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67635:
    filename: "202412201256 (72).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (72).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67636:
    filename: "202412201256 (73).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (73).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67637:
    filename: "202412201256 (74).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (74).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67638:
    filename: "202412201256 (75).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (75).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67639:
    filename: "202412201256 (76).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (76).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67640:
    filename: "202412201256 (77).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (77).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67641:
    filename: "202412201256 (78).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (78).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67642:
    filename: "202412201256 (79).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (79).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67643:
    filename: "202412201256 (80).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (80).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67644:
    filename: "202412201256 (81).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (81).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67645:
    filename: "202412201256 (82).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (82).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67646:
    filename: "202412201256 (83).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201256 (83).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67647:
    filename: "202412201300 (01).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (01).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67648:
    filename: "202412201300 (02).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (02).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67649:
    filename: "202412201300 (03).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (03).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67650:
    filename: "202412201300 (04).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (04).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67651:
    filename: "202412201300 (05).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (05).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67652:
    filename: "202412201300 (06).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (06).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67653:
    filename: "202412201300 (07).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (07).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67654:
    filename: "202412201300 (08).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (08).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67655:
    filename: "202412201300 (09).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (09).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67656:
    filename: "202412201300 (10).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (10).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67657:
    filename: "202412201300 (11).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (11).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67658:
    filename: "202412201300 (12).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (12).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67659:
    filename: "202412201300 (13).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (13).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67660:
    filename: "202412201300 (14).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (14).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67661:
    filename: "202412201300 (15).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (15).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67662:
    filename: "202412201300 (16).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (16).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67663:
    filename: "202412201300 (17).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (17).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67664:
    filename: "202412201300 (18).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (18).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67665:
    filename: "202412201300 (19).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (19).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67666:
    filename: "202412201300 (20).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (20).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67667:
    filename: "202412201300 (21).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (21).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67668:
    filename: "202412201300 (22).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (22).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67669:
    filename: "202412201300 (23).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (23).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67670:
    filename: "202412201300 (24).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (24).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67671:
    filename: "202412201300 (25).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (25).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67672:
    filename: "202412201300 (26).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (26).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67673:
    filename: "202412201300 (27).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (27).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67674:
    filename: "202412201300 (28).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (28).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67675:
    filename: "202412201300 (29).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (29).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67676:
    filename: "202412201300 (30).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (30).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67677:
    filename: "202412201300 (31).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (31).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67678:
    filename: "202412201300 (32).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (32).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67679:
    filename: "202412201300 (33).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (33).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67680:
    filename: "202412201300 (34).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (34).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67681:
    filename: "202412201300 (35).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (35).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67682:
    filename: "202412201300 (36).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (36).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67683:
    filename: "202412201300 (37).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (37).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67684:
    filename: "202412201300 (38).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (38).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67685:
    filename: "202412201300 (39).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (39).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67686:
    filename: "202412201300 (40).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (40).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67687:
    filename: "202412201300 (41).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (41).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
  67688:
    filename: "202412201300 (42).png"
    date: "20/12/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Dark Resurrection PSP 2024 - Screenshots/202412201300 (42).png"
    path: "Tekken Dark Resurrection PSP 2024 - Screenshots"
playlists:
tags:
  - "TonyTonyX042"
categories:
  - "PSP"
updates:
  - "Tekken Dark Resurrection PSP 2020 - Screenshots"
  - "Tekken Dark Resurrection PSP 2024 - Screenshots"
---
{% include 'article.html' %}