---
title: "Batman Beyond Return of the Joker"
french_title: "Batman of the Future Return of the Joker"
slug: "batman-beyond-return-of-the-joker"
post_date: "20/11/2000"
files:
playlists:
  78:
    title: "Batman of the Future Return of the Joker PS1 2020"
    publishedAt: "12/09/2020"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyKS9vJiy8Ptu-6Qc6Mk4jbm" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS1"
updates:
  - "Batman of the Future Return of the Joker PS1 2020"
---
{% include 'article.html' %}
