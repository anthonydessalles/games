---
title: "Shadow of the Colossus"
slug: "shadow-of-the-colossus"
post_date: "18/10/2005"
files:
playlists:
  907:
    title: "Shadow of the Colossus PS2 2021"
    publishedAt: "26/01/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyIyozPAjTfOBgld6kgW9gcD" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS2"
updates:
  - "Shadow of the Colossus PS2 2021"
---
{% include 'article.html' %}
