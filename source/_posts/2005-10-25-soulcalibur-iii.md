---
title: "SoulCalibur III"
slug: "soulcalibur-iii"
post_date: "25/10/2005"
files:
playlists:
  906:
    title: "SoulCalibur III PS2 2021"
    publishedAt: "26/01/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyKUQG3eRESD5PdXimJdjrnE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS2"
updates:
  - "SoulCalibur III PS2 2021"
---
{% include 'article.html' %}
