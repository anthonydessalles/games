---
title: "Star Wars The Empire Strikes Back"
slug: "star-wars-the-empire-strikes-back"
post_date: "31/12/1992"
files:
  10161:
    filename: "Star Wars The Empire Strikes Back-201113-174855.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars The Empire Strikes Back GB 2020 - Screenshots/Star Wars The Empire Strikes Back-201113-174855.png"
    path: "Star Wars The Empire Strikes Back GB 2020 - Screenshots"
  10162:
    filename: "Star Wars The Empire Strikes Back-201113-174907.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars The Empire Strikes Back GB 2020 - Screenshots/Star Wars The Empire Strikes Back-201113-174907.png"
    path: "Star Wars The Empire Strikes Back GB 2020 - Screenshots"
  10163:
    filename: "Star Wars The Empire Strikes Back-201113-174922.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars The Empire Strikes Back GB 2020 - Screenshots/Star Wars The Empire Strikes Back-201113-174922.png"
    path: "Star Wars The Empire Strikes Back GB 2020 - Screenshots"
  10164:
    filename: "Star Wars The Empire Strikes Back-201113-174953.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars The Empire Strikes Back GB 2020 - Screenshots/Star Wars The Empire Strikes Back-201113-174953.png"
    path: "Star Wars The Empire Strikes Back GB 2020 - Screenshots"
  10165:
    filename: "Star Wars The Empire Strikes Back-201113-175005.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars The Empire Strikes Back GB 2020 - Screenshots/Star Wars The Empire Strikes Back-201113-175005.png"
    path: "Star Wars The Empire Strikes Back GB 2020 - Screenshots"
  10166:
    filename: "Star Wars The Empire Strikes Back-201113-175012.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars The Empire Strikes Back GB 2020 - Screenshots/Star Wars The Empire Strikes Back-201113-175012.png"
    path: "Star Wars The Empire Strikes Back GB 2020 - Screenshots"
  10167:
    filename: "Star Wars The Empire Strikes Back-201113-175021.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars The Empire Strikes Back GB 2020 - Screenshots/Star Wars The Empire Strikes Back-201113-175021.png"
    path: "Star Wars The Empire Strikes Back GB 2020 - Screenshots"
  10168:
    filename: "Star Wars The Empire Strikes Back-201113-175033.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars The Empire Strikes Back GB 2020 - Screenshots/Star Wars The Empire Strikes Back-201113-175033.png"
    path: "Star Wars The Empire Strikes Back GB 2020 - Screenshots"
  10169:
    filename: "Star Wars The Empire Strikes Back-201113-175139.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars The Empire Strikes Back GB 2020 - Screenshots/Star Wars The Empire Strikes Back-201113-175139.png"
    path: "Star Wars The Empire Strikes Back GB 2020 - Screenshots"
  10170:
    filename: "Star Wars The Empire Strikes Back-201113-175214.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars The Empire Strikes Back GB 2020 - Screenshots/Star Wars The Empire Strikes Back-201113-175214.png"
    path: "Star Wars The Empire Strikes Back GB 2020 - Screenshots"
  10171:
    filename: "Star Wars The Empire Strikes Back-201113-175230.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars The Empire Strikes Back GB 2020 - Screenshots/Star Wars The Empire Strikes Back-201113-175230.png"
    path: "Star Wars The Empire Strikes Back GB 2020 - Screenshots"
  10172:
    filename: "Star Wars The Empire Strikes Back-201113-175300.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars The Empire Strikes Back GB 2020 - Screenshots/Star Wars The Empire Strikes Back-201113-175300.png"
    path: "Star Wars The Empire Strikes Back GB 2020 - Screenshots"
  10173:
    filename: "Star Wars The Empire Strikes Back-201113-175308.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars The Empire Strikes Back GB 2020 - Screenshots/Star Wars The Empire Strikes Back-201113-175308.png"
    path: "Star Wars The Empire Strikes Back GB 2020 - Screenshots"
  10174:
    filename: "Star Wars The Empire Strikes Back-201113-175349.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars The Empire Strikes Back GB 2020 - Screenshots/Star Wars The Empire Strikes Back-201113-175349.png"
    path: "Star Wars The Empire Strikes Back GB 2020 - Screenshots"
  10175:
    filename: "Star Wars The Empire Strikes Back-201113-175355.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars The Empire Strikes Back GB 2020 - Screenshots/Star Wars The Empire Strikes Back-201113-175355.png"
    path: "Star Wars The Empire Strikes Back GB 2020 - Screenshots"
  10176:
    filename: "Star Wars The Empire Strikes Back-201113-175456.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars The Empire Strikes Back GB 2020 - Screenshots/Star Wars The Empire Strikes Back-201113-175456.png"
    path: "Star Wars The Empire Strikes Back GB 2020 - Screenshots"
  10177:
    filename: "Star Wars The Empire Strikes Back-201113-175559.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars The Empire Strikes Back GB 2020 - Screenshots/Star Wars The Empire Strikes Back-201113-175559.png"
    path: "Star Wars The Empire Strikes Back GB 2020 - Screenshots"
  10178:
    filename: "Star Wars The Empire Strikes Back-201113-175608.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars The Empire Strikes Back GB 2020 - Screenshots/Star Wars The Empire Strikes Back-201113-175608.png"
    path: "Star Wars The Empire Strikes Back GB 2020 - Screenshots"
  10179:
    filename: "Star Wars The Empire Strikes Back-201113-175714.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars The Empire Strikes Back GB 2020 - Screenshots/Star Wars The Empire Strikes Back-201113-175714.png"
    path: "Star Wars The Empire Strikes Back GB 2020 - Screenshots"
playlists:
  775:
    title: "Star Wars The Empire Strikes Back GB 2020"
    publishedAt: "08/12/2020"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyIVfYaDQhGodu_nEsRXVjEg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "GB"
updates:
  - "Star Wars The Empire Strikes Back GB 2020 - Screenshots"
  - "Star Wars The Empire Strikes Back GB 2020"
---
{% include 'article.html' %}
