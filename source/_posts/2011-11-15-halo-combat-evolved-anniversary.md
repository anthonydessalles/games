---
title: "Halo Combat Evolved Anniversary"
french_title: "Halo Combat Evolved Anniversaire"
slug: "halo-combat-evolved-anniversary"
post_date: "15/11/2011"
files:
  2141:
    filename: "halo-combat-evolved-anniversaire-xb360-20140430-mission-01.png"
    date: "30/04/2014"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo Combat Evolved Anniversaire XB360 2014 - Screenshots/halo-combat-evolved-anniversaire-xb360-20140430-mission-01.png"
    path: "Halo Combat Evolved Anniversaire XB360 2014 - Screenshots"
  2142:
    filename: "halo-combat-evolved-anniversaire-xb360-20140430-mission-02.png"
    date: "30/04/2014"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo Combat Evolved Anniversaire XB360 2014 - Screenshots/halo-combat-evolved-anniversaire-xb360-20140430-mission-02.png"
    path: "Halo Combat Evolved Anniversaire XB360 2014 - Screenshots"
  36162:
    filename: "202401232240 (01).png"
    date: "23/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo Combat Evolved Anniversary XB360 2024 - Screenshots/202401232240 (01).png"
    path: "Halo Combat Evolved Anniversary XB360 2024 - Screenshots"
  36163:
    filename: "202401232240 (02).png"
    date: "23/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo Combat Evolved Anniversary XB360 2024 - Screenshots/202401232240 (02).png"
    path: "Halo Combat Evolved Anniversary XB360 2024 - Screenshots"
  36164:
    filename: "202401232240 (03).png"
    date: "23/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo Combat Evolved Anniversary XB360 2024 - Screenshots/202401232240 (03).png"
    path: "Halo Combat Evolved Anniversary XB360 2024 - Screenshots"
  36165:
    filename: "202401232240 (04).png"
    date: "23/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo Combat Evolved Anniversary XB360 2024 - Screenshots/202401232240 (04).png"
    path: "Halo Combat Evolved Anniversary XB360 2024 - Screenshots"
  36166:
    filename: "202401232240 (05).png"
    date: "23/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo Combat Evolved Anniversary XB360 2024 - Screenshots/202401232240 (05).png"
    path: "Halo Combat Evolved Anniversary XB360 2024 - Screenshots"
  36167:
    filename: "202401232240 (06).png"
    date: "23/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo Combat Evolved Anniversary XB360 2024 - Screenshots/202401232240 (06).png"
    path: "Halo Combat Evolved Anniversary XB360 2024 - Screenshots"
  36168:
    filename: "202401232240 (07).png"
    date: "23/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo Combat Evolved Anniversary XB360 2024 - Screenshots/202401232240 (07).png"
    path: "Halo Combat Evolved Anniversary XB360 2024 - Screenshots"
  36169:
    filename: "202401232240 (08).png"
    date: "23/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo Combat Evolved Anniversary XB360 2024 - Screenshots/202401232240 (08).png"
    path: "Halo Combat Evolved Anniversary XB360 2024 - Screenshots"
  36170:
    filename: "202401232240 (09).png"
    date: "23/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo Combat Evolved Anniversary XB360 2024 - Screenshots/202401232240 (09).png"
    path: "Halo Combat Evolved Anniversary XB360 2024 - Screenshots"
  36171:
    filename: "202401232240 (10).png"
    date: "23/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo Combat Evolved Anniversary XB360 2024 - Screenshots/202401232240 (10).png"
    path: "Halo Combat Evolved Anniversary XB360 2024 - Screenshots"
  36172:
    filename: "202401232240 (11).png"
    date: "23/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo Combat Evolved Anniversary XB360 2024 - Screenshots/202401232240 (11).png"
    path: "Halo Combat Evolved Anniversary XB360 2024 - Screenshots"
  36173:
    filename: "202401232240 (12).png"
    date: "23/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo Combat Evolved Anniversary XB360 2024 - Screenshots/202401232240 (12).png"
    path: "Halo Combat Evolved Anniversary XB360 2024 - Screenshots"
  36174:
    filename: "202401232240 (13).png"
    date: "23/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo Combat Evolved Anniversary XB360 2024 - Screenshots/202401232240 (13).png"
    path: "Halo Combat Evolved Anniversary XB360 2024 - Screenshots"
  36175:
    filename: "202401232240 (14).png"
    date: "23/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo Combat Evolved Anniversary XB360 2024 - Screenshots/202401232240 (14).png"
    path: "Halo Combat Evolved Anniversary XB360 2024 - Screenshots"
  36176:
    filename: "202401232240 (15).png"
    date: "23/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo Combat Evolved Anniversary XB360 2024 - Screenshots/202401232240 (15).png"
    path: "Halo Combat Evolved Anniversary XB360 2024 - Screenshots"
playlists:
tags:
  - "TonyTonyX042"
categories:
  - "XB360"
updates:
  - "Halo Combat Evolved Anniversaire XB360 2014 - Screenshots"
  - "Halo Combat Evolved Anniversary XB360 2024 - Screenshots"
---
{% include 'article.html' %}