---
title: "Batman Gotham City Racer"
slug: "batman-gotham-city-racer"
post_date: "24/05/2001"
files:
playlists:
  79:
    title: "Batman Gotham City Racer PS1 2020"
    publishedAt: "12/09/2020"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJlwZ7PSgE7iNC4ax-ochXH" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS1"
updates:
  - "Batman Gotham City Racer PS1 2020"
---
{% include 'article.html' %}
