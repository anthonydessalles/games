---
title: "Tenchu Stealth Assassins"
slug: "tenchu-stealth-assassins"
post_date: "26/02/1998"
files:
  10855:
    filename: "Tenchu Stealth Assassins-201126-190039.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190039.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10856:
    filename: "Tenchu Stealth Assassins-201126-190056.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190056.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10857:
    filename: "Tenchu Stealth Assassins-201126-190113.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190113.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10858:
    filename: "Tenchu Stealth Assassins-201126-190123.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190123.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10859:
    filename: "Tenchu Stealth Assassins-201126-190139.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190139.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10860:
    filename: "Tenchu Stealth Assassins-201126-190147.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190147.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10861:
    filename: "Tenchu Stealth Assassins-201126-190159.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190159.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10862:
    filename: "Tenchu Stealth Assassins-201126-190208.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190208.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10863:
    filename: "Tenchu Stealth Assassins-201126-190225.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190225.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10864:
    filename: "Tenchu Stealth Assassins-201126-190234.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190234.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10865:
    filename: "Tenchu Stealth Assassins-201126-190244.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190244.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10866:
    filename: "Tenchu Stealth Assassins-201126-190251.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190251.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10867:
    filename: "Tenchu Stealth Assassins-201126-190306.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190306.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10868:
    filename: "Tenchu Stealth Assassins-201126-190321.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190321.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10869:
    filename: "Tenchu Stealth Assassins-201126-190331.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190331.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10870:
    filename: "Tenchu Stealth Assassins-201126-190349.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190349.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10871:
    filename: "Tenchu Stealth Assassins-201126-190359.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190359.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10872:
    filename: "Tenchu Stealth Assassins-201126-190407.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190407.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10873:
    filename: "Tenchu Stealth Assassins-201126-190417.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190417.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10874:
    filename: "Tenchu Stealth Assassins-201126-190432.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190432.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10875:
    filename: "Tenchu Stealth Assassins-201126-190439.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190439.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10876:
    filename: "Tenchu Stealth Assassins-201126-190445.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190445.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10877:
    filename: "Tenchu Stealth Assassins-201126-190452.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190452.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10878:
    filename: "Tenchu Stealth Assassins-201126-190503.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190503.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10879:
    filename: "Tenchu Stealth Assassins-201126-190521.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190521.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10880:
    filename: "Tenchu Stealth Assassins-201126-190527.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190527.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10881:
    filename: "Tenchu Stealth Assassins-201126-190550.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190550.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10882:
    filename: "Tenchu Stealth Assassins-201126-190555.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190555.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10883:
    filename: "Tenchu Stealth Assassins-201126-190605.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190605.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10884:
    filename: "Tenchu Stealth Assassins-201126-190621.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190621.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10885:
    filename: "Tenchu Stealth Assassins-201126-190639.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190639.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10886:
    filename: "Tenchu Stealth Assassins-201126-190710.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190710.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10887:
    filename: "Tenchu Stealth Assassins-201126-190728.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190728.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10888:
    filename: "Tenchu Stealth Assassins-201126-190740.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190740.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10889:
    filename: "Tenchu Stealth Assassins-201126-190755.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190755.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10890:
    filename: "Tenchu Stealth Assassins-201126-190808.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190808.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10891:
    filename: "Tenchu Stealth Assassins-201126-190815.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190815.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10892:
    filename: "Tenchu Stealth Assassins-201126-190823.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190823.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10893:
    filename: "Tenchu Stealth Assassins-201126-190833.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190833.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10894:
    filename: "Tenchu Stealth Assassins-201126-190839.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190839.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10895:
    filename: "Tenchu Stealth Assassins-201126-190854.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190854.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10896:
    filename: "Tenchu Stealth Assassins-201126-190909.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190909.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10897:
    filename: "Tenchu Stealth Assassins-201126-190913.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190913.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10898:
    filename: "Tenchu Stealth Assassins-201126-190926.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190926.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10899:
    filename: "Tenchu Stealth Assassins-201126-190944.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190944.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10900:
    filename: "Tenchu Stealth Assassins-201126-190959.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-190959.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10901:
    filename: "Tenchu Stealth Assassins-201126-191004.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191004.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10902:
    filename: "Tenchu Stealth Assassins-201126-191034.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191034.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10903:
    filename: "Tenchu Stealth Assassins-201126-191044.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191044.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10904:
    filename: "Tenchu Stealth Assassins-201126-191105.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191105.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10905:
    filename: "Tenchu Stealth Assassins-201126-191124.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191124.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10906:
    filename: "Tenchu Stealth Assassins-201126-191132.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191132.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10907:
    filename: "Tenchu Stealth Assassins-201126-191140.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191140.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10908:
    filename: "Tenchu Stealth Assassins-201126-191147.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191147.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10909:
    filename: "Tenchu Stealth Assassins-201126-191156.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191156.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10910:
    filename: "Tenchu Stealth Assassins-201126-191202.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191202.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10911:
    filename: "Tenchu Stealth Assassins-201126-191208.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191208.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10912:
    filename: "Tenchu Stealth Assassins-201126-191214.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191214.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10913:
    filename: "Tenchu Stealth Assassins-201126-191222.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191222.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10914:
    filename: "Tenchu Stealth Assassins-201126-191228.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191228.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10915:
    filename: "Tenchu Stealth Assassins-201126-191235.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191235.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10916:
    filename: "Tenchu Stealth Assassins-201126-191241.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191241.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10917:
    filename: "Tenchu Stealth Assassins-201126-191247.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191247.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10918:
    filename: "Tenchu Stealth Assassins-201126-191255.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191255.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10919:
    filename: "Tenchu Stealth Assassins-201126-191316.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191316.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10920:
    filename: "Tenchu Stealth Assassins-201126-191323.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191323.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10921:
    filename: "Tenchu Stealth Assassins-201126-191331.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191331.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10922:
    filename: "Tenchu Stealth Assassins-201126-191336.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191336.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10923:
    filename: "Tenchu Stealth Assassins-201126-191344.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191344.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10924:
    filename: "Tenchu Stealth Assassins-201126-191354.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191354.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10925:
    filename: "Tenchu Stealth Assassins-201126-191440.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191440.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10926:
    filename: "Tenchu Stealth Assassins-201126-191456.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191456.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10927:
    filename: "Tenchu Stealth Assassins-201126-191517.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191517.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10928:
    filename: "Tenchu Stealth Assassins-201126-191547.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191547.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10929:
    filename: "Tenchu Stealth Assassins-201126-191635.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191635.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10930:
    filename: "Tenchu Stealth Assassins-201126-191645.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191645.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10931:
    filename: "Tenchu Stealth Assassins-201126-191729.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191729.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10932:
    filename: "Tenchu Stealth Assassins-201126-191740.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191740.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10933:
    filename: "Tenchu Stealth Assassins-201126-191819.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191819.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10934:
    filename: "Tenchu Stealth Assassins-201126-191839.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191839.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10935:
    filename: "Tenchu Stealth Assassins-201126-191849.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-191849.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10936:
    filename: "Tenchu Stealth Assassins-201126-192031.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-192031.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10937:
    filename: "Tenchu Stealth Assassins-201126-192136.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-192136.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10938:
    filename: "Tenchu Stealth Assassins-201126-192208.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-192208.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10939:
    filename: "Tenchu Stealth Assassins-201126-192343.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-192343.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10940:
    filename: "Tenchu Stealth Assassins-201126-192735.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-192735.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10941:
    filename: "Tenchu Stealth Assassins-201126-192827.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-192827.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10942:
    filename: "Tenchu Stealth Assassins-201126-192902.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-192902.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10943:
    filename: "Tenchu Stealth Assassins-201126-192945.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-192945.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10944:
    filename: "Tenchu Stealth Assassins-201126-192950.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-192950.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10945:
    filename: "Tenchu Stealth Assassins-201126-192957.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-192957.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10946:
    filename: "Tenchu Stealth Assassins-201126-193003.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-193003.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10947:
    filename: "Tenchu Stealth Assassins-201126-193016.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-193016.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10948:
    filename: "Tenchu Stealth Assassins-201126-193030.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-193030.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10949:
    filename: "Tenchu Stealth Assassins-201126-193037.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-193037.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10950:
    filename: "Tenchu Stealth Assassins-201126-193043.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-193043.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10951:
    filename: "Tenchu Stealth Assassins-201126-193052.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-193052.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10952:
    filename: "Tenchu Stealth Assassins-201126-193058.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-193058.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10953:
    filename: "Tenchu Stealth Assassins-201126-193102.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-193102.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10954:
    filename: "Tenchu Stealth Assassins-201126-193107.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-193107.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10955:
    filename: "Tenchu Stealth Assassins-201126-193115.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-193115.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10956:
    filename: "Tenchu Stealth Assassins-201126-193148.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-193148.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10957:
    filename: "Tenchu Stealth Assassins-201126-193156.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-193156.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10958:
    filename: "Tenchu Stealth Assassins-201126-193202.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-193202.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10959:
    filename: "Tenchu Stealth Assassins-201126-193214.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-193214.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10960:
    filename: "Tenchu Stealth Assassins-201126-193219.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-193219.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10961:
    filename: "Tenchu Stealth Assassins-201126-193224.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-193224.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10962:
    filename: "Tenchu Stealth Assassins-201126-193231.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-193231.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
  10963:
    filename: "Tenchu Stealth Assassins-201126-193236.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tenchu Stealth Assassins PS1 2020 - Screenshots/Tenchu Stealth Assassins-201126-193236.png"
    path: "Tenchu Stealth Assassins PS1 2020 - Screenshots"
playlists:
tags:
  - "TonyTonyX042"
categories:
  - "PS1"
updates:
  - "Tenchu Stealth Assassins PS1 2020 - Screenshots"
---
{% include 'article.html' %}
