---
title: "FIFA Street"
slug: "fifa-street"
post_date: "22/02/2005"
files:
playlists:
  1008:
    title: "FIFA Street XB 2020"
    publishedAt: "21/01/2021"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLO7Vzb0CPqO28FsN7QLgY7" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "XB"
updates:
  - "FIFA Street XB 2020"
---
{% include 'article.html' %}
