---
title: "Super Star Wars Return of the Jedi"
slug: "super-star-wars-return-of-the-jedi"
post_date: "22/06/1994"
files:
playlists:
  1328:
    title: "Super Star Wars Return of the Jedi GB 2021"
    publishedAt: "01/12/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyIYwUJfNfsA6A_nT6_66kx-" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "GB"
updates:
  - "Super Star Wars Return of the Jedi GB 2021"
---
{% include 'article.html' %}
