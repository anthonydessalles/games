---
title: "Pro Evolution Soccer 2008"
slug: "pro-evolution-soccer-2008"
post_date: "26/10/2007"
files:
playlists:
  2472:
    title: "Pro Evolution Soccer 2008 XB360 2024"
    publishedAt: "29/01/2024"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyKyxwe3JNs6bMHHFWJtbnvr" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "XB360"
updates:
  - "Pro Evolution Soccer 2008 XB360 2024"
---
{% include 'article.html' %}