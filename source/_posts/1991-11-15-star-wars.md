---
title: "Star Wars"
slug: "star-wars"
post_date: "15/11/1991"
files:
  9847:
    filename: "Star Wars-201113-174115.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars GB 2020 - Screenshots/Star Wars-201113-174115.png"
    path: "Star Wars GB 2020 - Screenshots"
  9848:
    filename: "Star Wars-201113-174124.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars GB 2020 - Screenshots/Star Wars-201113-174124.png"
    path: "Star Wars GB 2020 - Screenshots"
  9849:
    filename: "Star Wars-201113-174129.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars GB 2020 - Screenshots/Star Wars-201113-174129.png"
    path: "Star Wars GB 2020 - Screenshots"
  9850:
    filename: "Star Wars-201113-174138.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars GB 2020 - Screenshots/Star Wars-201113-174138.png"
    path: "Star Wars GB 2020 - Screenshots"
  9851:
    filename: "Star Wars-201113-174200.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars GB 2020 - Screenshots/Star Wars-201113-174200.png"
    path: "Star Wars GB 2020 - Screenshots"
  9852:
    filename: "Star Wars-201113-174230.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars GB 2020 - Screenshots/Star Wars-201113-174230.png"
    path: "Star Wars GB 2020 - Screenshots"
  9853:
    filename: "Star Wars-201113-174300.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars GB 2020 - Screenshots/Star Wars-201113-174300.png"
    path: "Star Wars GB 2020 - Screenshots"
  9854:
    filename: "Star Wars-201113-174308.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars GB 2020 - Screenshots/Star Wars-201113-174308.png"
    path: "Star Wars GB 2020 - Screenshots"
  9855:
    filename: "Star Wars-201113-174318.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars GB 2020 - Screenshots/Star Wars-201113-174318.png"
    path: "Star Wars GB 2020 - Screenshots"
  9856:
    filename: "Star Wars-201113-174329.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars GB 2020 - Screenshots/Star Wars-201113-174329.png"
    path: "Star Wars GB 2020 - Screenshots"
  9857:
    filename: "Star Wars-201113-174334.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars GB 2020 - Screenshots/Star Wars-201113-174334.png"
    path: "Star Wars GB 2020 - Screenshots"
  9858:
    filename: "Star Wars-201113-174347.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars GB 2020 - Screenshots/Star Wars-201113-174347.png"
    path: "Star Wars GB 2020 - Screenshots"
  9859:
    filename: "Star Wars-201113-174418.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars GB 2020 - Screenshots/Star Wars-201113-174418.png"
    path: "Star Wars GB 2020 - Screenshots"
  9860:
    filename: "Star Wars-201113-174442.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars GB 2020 - Screenshots/Star Wars-201113-174442.png"
    path: "Star Wars GB 2020 - Screenshots"
  9861:
    filename: "Star Wars-201113-174459.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars GB 2020 - Screenshots/Star Wars-201113-174459.png"
    path: "Star Wars GB 2020 - Screenshots"
  9862:
    filename: "Star Wars-201113-174506.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars GB 2020 - Screenshots/Star Wars-201113-174506.png"
    path: "Star Wars GB 2020 - Screenshots"
  9863:
    filename: "Star Wars-201113-174514.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars GB 2020 - Screenshots/Star Wars-201113-174514.png"
    path: "Star Wars GB 2020 - Screenshots"
  9864:
    filename: "Star Wars-201113-174528.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars GB 2020 - Screenshots/Star Wars-201113-174528.png"
    path: "Star Wars GB 2020 - Screenshots"
  9865:
    filename: "Star Wars-201113-174604.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars GB 2020 - Screenshots/Star Wars-201113-174604.png"
    path: "Star Wars GB 2020 - Screenshots"
  9866:
    filename: "Star Wars-201113-174615.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars GB 2020 - Screenshots/Star Wars-201113-174615.png"
    path: "Star Wars GB 2020 - Screenshots"
  9867:
    filename: "Star Wars-201113-174646.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars GB 2020 - Screenshots/Star Wars-201113-174646.png"
    path: "Star Wars GB 2020 - Screenshots"
  9868:
    filename: "Star Wars-201113-174741.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars GB 2020 - Screenshots/Star Wars-201113-174741.png"
    path: "Star Wars GB 2020 - Screenshots"
playlists:
  776:
    title: "Star Wars GB 2020"
    publishedAt: "08/12/2020"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyIrAP1E90EZjtA4HChZFfwU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "GB"
updates:
  - "Star Wars GB 2020 - Screenshots"
  - "Star Wars GB 2020"
---
{% include 'article.html' %}
