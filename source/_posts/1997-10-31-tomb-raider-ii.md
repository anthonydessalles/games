---
title: "Tomb Raider II"
slug: "tomb-raider-ii"
post_date: "31/10/1997"
files:
  11555:
    filename: "Tomb Raider 2-201127-170302.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170302.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11556:
    filename: "Tomb Raider 2-201127-170314.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170314.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11557:
    filename: "Tomb Raider 2-201127-170321.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170321.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11558:
    filename: "Tomb Raider 2-201127-170332.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170332.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11559:
    filename: "Tomb Raider 2-201127-170345.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170345.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11560:
    filename: "Tomb Raider 2-201127-170350.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170350.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11561:
    filename: "Tomb Raider 2-201127-170358.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170358.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11562:
    filename: "Tomb Raider 2-201127-170404.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170404.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11563:
    filename: "Tomb Raider 2-201127-170409.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170409.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11564:
    filename: "Tomb Raider 2-201127-170415.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170415.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11565:
    filename: "Tomb Raider 2-201127-170422.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170422.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11566:
    filename: "Tomb Raider 2-201127-170428.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170428.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11567:
    filename: "Tomb Raider 2-201127-170433.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170433.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11568:
    filename: "Tomb Raider 2-201127-170441.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170441.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11569:
    filename: "Tomb Raider 2-201127-170447.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170447.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11570:
    filename: "Tomb Raider 2-201127-170452.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170452.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11571:
    filename: "Tomb Raider 2-201127-170459.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170459.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11572:
    filename: "Tomb Raider 2-201127-170507.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170507.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11573:
    filename: "Tomb Raider 2-201127-170512.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170512.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11574:
    filename: "Tomb Raider 2-201127-170519.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170519.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11575:
    filename: "Tomb Raider 2-201127-170524.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170524.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11576:
    filename: "Tomb Raider 2-201127-170532.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170532.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11577:
    filename: "Tomb Raider 2-201127-170537.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170537.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11578:
    filename: "Tomb Raider 2-201127-170544.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170544.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11579:
    filename: "Tomb Raider 2-201127-170550.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170550.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11580:
    filename: "Tomb Raider 2-201127-170555.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170555.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11581:
    filename: "Tomb Raider 2-201127-170602.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170602.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11582:
    filename: "Tomb Raider 2-201127-170608.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170608.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11583:
    filename: "Tomb Raider 2-201127-170616.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170616.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11584:
    filename: "Tomb Raider 2-201127-170626.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170626.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11585:
    filename: "Tomb Raider 2-201127-170630.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170630.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11586:
    filename: "Tomb Raider 2-201127-170643.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170643.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11587:
    filename: "Tomb Raider 2-201127-170657.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170657.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11588:
    filename: "Tomb Raider 2-201127-170704.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170704.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11589:
    filename: "Tomb Raider 2-201127-170709.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170709.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11590:
    filename: "Tomb Raider 2-201127-170721.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170721.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11591:
    filename: "Tomb Raider 2-201127-170726.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170726.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11592:
    filename: "Tomb Raider 2-201127-170732.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170732.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11593:
    filename: "Tomb Raider 2-201127-170746.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170746.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11594:
    filename: "Tomb Raider 2-201127-170800.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170800.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11595:
    filename: "Tomb Raider 2-201127-170814.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170814.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11596:
    filename: "Tomb Raider 2-201127-170821.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170821.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11597:
    filename: "Tomb Raider 2-201127-170831.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170831.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11598:
    filename: "Tomb Raider 2-201127-170835.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170835.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11599:
    filename: "Tomb Raider 2-201127-170840.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170840.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11600:
    filename: "Tomb Raider 2-201127-170845.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170845.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11601:
    filename: "Tomb Raider 2-201127-170853.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170853.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11602:
    filename: "Tomb Raider 2-201127-170902.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170902.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11603:
    filename: "Tomb Raider 2-201127-170922.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170922.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11604:
    filename: "Tomb Raider 2-201127-170930.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170930.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11605:
    filename: "Tomb Raider 2-201127-170940.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170940.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11606:
    filename: "Tomb Raider 2-201127-170946.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170946.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11607:
    filename: "Tomb Raider 2-201127-170954.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-170954.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11608:
    filename: "Tomb Raider 2-201127-171000.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-171000.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11609:
    filename: "Tomb Raider 2-201127-171007.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-171007.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11610:
    filename: "Tomb Raider 2-201127-171015.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-171015.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11611:
    filename: "Tomb Raider 2-201127-171022.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-171022.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11612:
    filename: "Tomb Raider 2-201127-171032.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-171032.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11613:
    filename: "Tomb Raider 2-201127-171043.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-171043.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11614:
    filename: "Tomb Raider 2-201127-171052.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-171052.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11615:
    filename: "Tomb Raider 2-201127-171115.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-171115.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11616:
    filename: "Tomb Raider 2-201127-171125.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-171125.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11617:
    filename: "Tomb Raider 2-201127-171132.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-171132.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11618:
    filename: "Tomb Raider 2-201127-171147.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-171147.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11619:
    filename: "Tomb Raider 2-201127-171217.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-171217.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11620:
    filename: "Tomb Raider 2-201127-171227.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-171227.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11621:
    filename: "Tomb Raider 2-201127-171241.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-171241.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11622:
    filename: "Tomb Raider 2-201127-171252.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-171252.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11623:
    filename: "Tomb Raider 2-201127-171305.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-171305.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11624:
    filename: "Tomb Raider 2-201127-171314.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-171314.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11625:
    filename: "Tomb Raider 2-201127-171427.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-171427.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11626:
    filename: "Tomb Raider 2-201127-171446.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-171446.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
  11627:
    filename: "Tomb Raider 2-201127-171458.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider 2 PS1 2020 - Screenshots/Tomb Raider 2-201127-171458.png"
    path: "Tomb Raider 2 PS1 2020 - Screenshots"
playlists:
tags:
  - "TonyTonyX042"
categories:
  - "PS1"
updates:
  - "Tomb Raider 2 PS1 2020 - Screenshots"
---
{% include 'article.html' %}
