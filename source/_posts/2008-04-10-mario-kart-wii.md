---
title: "Mario Kart Wii"
slug: "mario-kart-wii"
post_date: "10/04/2008"
files:
  3192:
    filename: "2020_9_1_20_52_20.bmp"
    date: "01/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Mario Kart Wii 2020 - Screenshots/2020_9_1_20_52_20.bmp"
    path: "Mario Kart Wii 2020 - Screenshots"
  3193:
    filename: "2020_9_1_20_52_24.bmp"
    date: "01/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Mario Kart Wii 2020 - Screenshots/2020_9_1_20_52_24.bmp"
    path: "Mario Kart Wii 2020 - Screenshots"
  3194:
    filename: "2020_9_1_20_52_27.bmp"
    date: "01/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Mario Kart Wii 2020 - Screenshots/2020_9_1_20_52_27.bmp"
    path: "Mario Kart Wii 2020 - Screenshots"
  3195:
    filename: "2020_9_1_20_52_37.bmp"
    date: "01/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Mario Kart Wii 2020 - Screenshots/2020_9_1_20_52_37.bmp"
    path: "Mario Kart Wii 2020 - Screenshots"
  3196:
    filename: "2020_9_1_20_52_40.bmp"
    date: "01/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Mario Kart Wii 2020 - Screenshots/2020_9_1_20_52_40.bmp"
    path: "Mario Kart Wii 2020 - Screenshots"
  3197:
    filename: "2020_9_1_20_52_44.bmp"
    date: "01/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Mario Kart Wii 2020 - Screenshots/2020_9_1_20_52_44.bmp"
    path: "Mario Kart Wii 2020 - Screenshots"
  3198:
    filename: "2020_9_1_20_52_47.bmp"
    date: "01/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Mario Kart Wii 2020 - Screenshots/2020_9_1_20_52_47.bmp"
    path: "Mario Kart Wii 2020 - Screenshots"
  3199:
    filename: "2020_9_1_20_52_50.bmp"
    date: "01/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Mario Kart Wii 2020 - Screenshots/2020_9_1_20_52_50.bmp"
    path: "Mario Kart Wii 2020 - Screenshots"
  3200:
    filename: "2020_9_1_20_52_58.bmp"
    date: "01/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Mario Kart Wii 2020 - Screenshots/2020_9_1_20_52_58.bmp"
    path: "Mario Kart Wii 2020 - Screenshots"
  3201:
    filename: "2020_9_1_20_53_24.bmp"
    date: "01/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Mario Kart Wii 2020 - Screenshots/2020_9_1_20_53_24.bmp"
    path: "Mario Kart Wii 2020 - Screenshots"
  3202:
    filename: "2020_9_1_20_53_32.bmp"
    date: "01/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Mario Kart Wii 2020 - Screenshots/2020_9_1_20_53_32.bmp"
    path: "Mario Kart Wii 2020 - Screenshots"
  3203:
    filename: "2020_9_1_20_53_43.bmp"
    date: "01/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Mario Kart Wii 2020 - Screenshots/2020_9_1_20_53_43.bmp"
    path: "Mario Kart Wii 2020 - Screenshots"
  3204:
    filename: "2020_9_1_20_53_47.bmp"
    date: "01/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Mario Kart Wii 2020 - Screenshots/2020_9_1_20_53_47.bmp"
    path: "Mario Kart Wii 2020 - Screenshots"
  3205:
    filename: "2020_9_1_20_53_52.bmp"
    date: "01/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Mario Kart Wii 2020 - Screenshots/2020_9_1_20_53_52.bmp"
    path: "Mario Kart Wii 2020 - Screenshots"
  3206:
    filename: "2020_9_1_20_53_59.bmp"
    date: "01/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Mario Kart Wii 2020 - Screenshots/2020_9_1_20_53_59.bmp"
    path: "Mario Kart Wii 2020 - Screenshots"
  3207:
    filename: "2020_9_1_20_54_13.bmp"
    date: "01/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Mario Kart Wii 2020 - Screenshots/2020_9_1_20_54_13.bmp"
    path: "Mario Kart Wii 2020 - Screenshots"
  3208:
    filename: "2020_9_1_20_54_15.bmp"
    date: "01/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Mario Kart Wii 2020 - Screenshots/2020_9_1_20_54_15.bmp"
    path: "Mario Kart Wii 2020 - Screenshots"
  3209:
    filename: "2020_9_1_20_54_16.bmp"
    date: "01/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Mario Kart Wii 2020 - Screenshots/2020_9_1_20_54_16.bmp"
    path: "Mario Kart Wii 2020 - Screenshots"
  3210:
    filename: "2020_9_1_20_54_9.bmp"
    date: "01/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Mario Kart Wii 2020 - Screenshots/2020_9_1_20_54_9.bmp"
    path: "Mario Kart Wii 2020 - Screenshots"
playlists:
  2049:
    title: "Mario Kart Wii 2023"
    publishedAt: "27/06/2023"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJ7KJuzBg6O9tcRBl0vIgI4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "Wii"
updates:
  - "Mario Kart Wii 2020 - Screenshots"
  - "Mario Kart Wii 2023"
---
{% include 'article.html' %}
