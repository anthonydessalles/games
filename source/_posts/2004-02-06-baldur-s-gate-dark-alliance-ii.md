---
title: "Baldur's Gate Dark Alliance II"
slug: "baldur-s-gate-dark-alliance-ii"
post_date: "06/02/2004"
files:
playlists:
  917:
    title: "Baldur's Gate Dark Alliance II PS2 2021"
    publishedAt: "26/01/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyL_LXNaFUDsrmifjsd1Ozk_" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS2"
updates:
  - "Baldur's Gate Dark Alliance II PS2 2021"
---
{% include 'article.html' %}
