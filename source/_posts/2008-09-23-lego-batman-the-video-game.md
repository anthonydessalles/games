---
title: "Lego Batman The Video Game"
french_title: "Lego Batman Le Jeu Vidéo"
slug: "lego-batman-the-video-game"
post_date: "23/09/2008"
files:
  3169:
    filename: "lego-batman-le-jeu-video-xb360-20141101-mission-la-soulte-du-sphinx.png"
    date: "01/11/2014"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Lego Batman Le Jeu Vidéo XB360 2014 - Screenshots/lego-batman-le-jeu-video-xb360-20141101-mission-la-soulte-du-sphinx.png"
    path: "Lego Batman Le Jeu Vidéo XB360 2014 - Screenshots"
  3170:
    filename: "lego-batman-le-jeu-video-xb360-20141101-mission-le-pingouin-fou-de-puissance.png"
    date: "01/11/2014"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Lego Batman Le Jeu Vidéo XB360 2014 - Screenshots/lego-batman-le-jeu-video-xb360-20141101-mission-le-pingouin-fou-de-puissance.png"
    path: "Lego Batman Le Jeu Vidéo XB360 2014 - Screenshots"
  3171:
    filename: "lego-batman-le-jeu-video-xb360-20141101-mission-le-retour-du-joker.png"
    date: "01/11/2014"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Lego Batman Le Jeu Vidéo XB360 2014 - Screenshots/lego-batman-le-jeu-video-xb360-20141101-mission-le-retour-du-joker.png"
    path: "Lego Batman Le Jeu Vidéo XB360 2014 - Screenshots"
playlists:
  2067:
    title: "Lego Batman Le Jeu Vidéo XB360 2023"
    publishedAt: "27/06/2023"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJ6xtI87Q_1Z8c6tjEqKf6c" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "XB360"
updates:
  - "Lego Batman Le Jeu Vidéo XB360 2014 - Screenshots"
  - "Lego Batman Le Jeu Vidéo XB360 2023"
---
{% include 'article.html' %}
