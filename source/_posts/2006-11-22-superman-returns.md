---
title: "Superman Returns"
slug: "superman-returns"
post_date: "22/11/2006"
files:
playlists:
  973:
    title: "Superman Returns XB 2020"
    publishedAt: "25/01/2021"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyIDfL28WQnXgfEDppMnQXRu" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "XB"
updates:
  - "Superman Returns XB 2020"
---
{% include 'article.html' %}
