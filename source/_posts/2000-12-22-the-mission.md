---
title: "The Mission"
slug: "the-mission"
post_date: "22/12/2000"
files:
  11469:
    filename: "The Mission-201126-195229.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195229.png"
    path: "The Mission PS1 2020 - Screenshots"
  11470:
    filename: "The Mission-201126-195238.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195238.png"
    path: "The Mission PS1 2020 - Screenshots"
  11471:
    filename: "The Mission-201126-195246.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195246.png"
    path: "The Mission PS1 2020 - Screenshots"
  11472:
    filename: "The Mission-201126-195258.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195258.png"
    path: "The Mission PS1 2020 - Screenshots"
  11473:
    filename: "The Mission-201126-195306.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195306.png"
    path: "The Mission PS1 2020 - Screenshots"
  11474:
    filename: "The Mission-201126-195315.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195315.png"
    path: "The Mission PS1 2020 - Screenshots"
  11475:
    filename: "The Mission-201126-195407.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195407.png"
    path: "The Mission PS1 2020 - Screenshots"
  11476:
    filename: "The Mission-201126-195416.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195416.png"
    path: "The Mission PS1 2020 - Screenshots"
  11477:
    filename: "The Mission-201126-195427.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195427.png"
    path: "The Mission PS1 2020 - Screenshots"
  11478:
    filename: "The Mission-201126-195437.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195437.png"
    path: "The Mission PS1 2020 - Screenshots"
  11479:
    filename: "The Mission-201126-195447.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195447.png"
    path: "The Mission PS1 2020 - Screenshots"
  11480:
    filename: "The Mission-201126-195457.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195457.png"
    path: "The Mission PS1 2020 - Screenshots"
  11481:
    filename: "The Mission-201126-195505.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195505.png"
    path: "The Mission PS1 2020 - Screenshots"
  11482:
    filename: "The Mission-201126-195514.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195514.png"
    path: "The Mission PS1 2020 - Screenshots"
  11483:
    filename: "The Mission-201126-195524.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195524.png"
    path: "The Mission PS1 2020 - Screenshots"
  11484:
    filename: "The Mission-201126-195534.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195534.png"
    path: "The Mission PS1 2020 - Screenshots"
  11485:
    filename: "The Mission-201126-195554.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195554.png"
    path: "The Mission PS1 2020 - Screenshots"
  11486:
    filename: "The Mission-201126-195602.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195602.png"
    path: "The Mission PS1 2020 - Screenshots"
  11487:
    filename: "The Mission-201126-195609.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195609.png"
    path: "The Mission PS1 2020 - Screenshots"
  11488:
    filename: "The Mission-201126-195617.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195617.png"
    path: "The Mission PS1 2020 - Screenshots"
  11489:
    filename: "The Mission-201126-195646.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195646.png"
    path: "The Mission PS1 2020 - Screenshots"
  11490:
    filename: "The Mission-201126-195653.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195653.png"
    path: "The Mission PS1 2020 - Screenshots"
  11491:
    filename: "The Mission-201126-195706.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195706.png"
    path: "The Mission PS1 2020 - Screenshots"
  11492:
    filename: "The Mission-201126-195713.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195713.png"
    path: "The Mission PS1 2020 - Screenshots"
  11493:
    filename: "The Mission-201126-195719.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195719.png"
    path: "The Mission PS1 2020 - Screenshots"
  11494:
    filename: "The Mission-201126-195730.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195730.png"
    path: "The Mission PS1 2020 - Screenshots"
  11495:
    filename: "The Mission-201126-195740.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195740.png"
    path: "The Mission PS1 2020 - Screenshots"
  11496:
    filename: "The Mission-201126-195810.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195810.png"
    path: "The Mission PS1 2020 - Screenshots"
  11497:
    filename: "The Mission-201126-195821.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195821.png"
    path: "The Mission PS1 2020 - Screenshots"
  11498:
    filename: "The Mission-201126-195844.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195844.png"
    path: "The Mission PS1 2020 - Screenshots"
  11499:
    filename: "The Mission-201126-195900.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195900.png"
    path: "The Mission PS1 2020 - Screenshots"
  11500:
    filename: "The Mission-201126-195919.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195919.png"
    path: "The Mission PS1 2020 - Screenshots"
  11501:
    filename: "The Mission-201126-195934.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195934.png"
    path: "The Mission PS1 2020 - Screenshots"
  11502:
    filename: "The Mission-201126-195941.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195941.png"
    path: "The Mission PS1 2020 - Screenshots"
  11503:
    filename: "The Mission-201126-195948.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195948.png"
    path: "The Mission PS1 2020 - Screenshots"
  11504:
    filename: "The Mission-201126-195955.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-195955.png"
    path: "The Mission PS1 2020 - Screenshots"
  11505:
    filename: "The Mission-201126-200044.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-200044.png"
    path: "The Mission PS1 2020 - Screenshots"
  11506:
    filename: "The Mission-201126-200102.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-200102.png"
    path: "The Mission PS1 2020 - Screenshots"
  11507:
    filename: "The Mission-201126-200114.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-200114.png"
    path: "The Mission PS1 2020 - Screenshots"
  11508:
    filename: "The Mission-201126-200139.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-200139.png"
    path: "The Mission PS1 2020 - Screenshots"
  11509:
    filename: "The Mission-201126-200201.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-200201.png"
    path: "The Mission PS1 2020 - Screenshots"
  11510:
    filename: "The Mission-201126-200219.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-200219.png"
    path: "The Mission PS1 2020 - Screenshots"
  11511:
    filename: "The Mission-201126-200319.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-200319.png"
    path: "The Mission PS1 2020 - Screenshots"
  11512:
    filename: "The Mission-201126-200432.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-200432.png"
    path: "The Mission PS1 2020 - Screenshots"
  11513:
    filename: "The Mission-201126-200443.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-200443.png"
    path: "The Mission PS1 2020 - Screenshots"
  11514:
    filename: "The Mission-201126-200505.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-200505.png"
    path: "The Mission PS1 2020 - Screenshots"
  11515:
    filename: "The Mission-201126-200528.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-200528.png"
    path: "The Mission PS1 2020 - Screenshots"
  11516:
    filename: "The Mission-201126-200827.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-200827.png"
    path: "The Mission PS1 2020 - Screenshots"
  11517:
    filename: "The Mission-201126-200929.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-200929.png"
    path: "The Mission PS1 2020 - Screenshots"
  11518:
    filename: "The Mission-201126-201201.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-201201.png"
    path: "The Mission PS1 2020 - Screenshots"
  11519:
    filename: "The Mission-201126-201211.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-201211.png"
    path: "The Mission PS1 2020 - Screenshots"
  11520:
    filename: "The Mission-201126-201233.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-201233.png"
    path: "The Mission PS1 2020 - Screenshots"
  11521:
    filename: "The Mission-201126-201248.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-201248.png"
    path: "The Mission PS1 2020 - Screenshots"
  11522:
    filename: "The Mission-201126-201303.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-201303.png"
    path: "The Mission PS1 2020 - Screenshots"
  11523:
    filename: "The Mission-201126-201317.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-201317.png"
    path: "The Mission PS1 2020 - Screenshots"
  11524:
    filename: "The Mission-201126-201329.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-201329.png"
    path: "The Mission PS1 2020 - Screenshots"
  11525:
    filename: "The Mission-201126-201337.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-201337.png"
    path: "The Mission PS1 2020 - Screenshots"
  11526:
    filename: "The Mission-201126-201414.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Mission PS1 2020 - Screenshots/The Mission-201126-201414.png"
    path: "The Mission PS1 2020 - Screenshots"
playlists:
tags:
  - "TonyTonyX042"
categories:
  - "PS1"
updates:
  - "The Mission PS1 2020 - Screenshots"
---
{% include 'article.html' %}
