---
title: "The Lord of the Rings The Battle for Middle-earth II The Rise of the Witch-king"
french_title: "Le Seigneur des Anneaux La Bataille pour la Terre du Milieu II L'Avènement du Roi-Sorcier"
slug: "the-lord-of-the-rings-the-battle-for-middle-earth-ii-the-rise-of-the-witch-king"
post_date: "28/11/2006"
files:
playlists:
  1337:
    title: "The Lord of the Rings The Battle for Middle-earth II The Rise of the Witch-king PC 2021"
    publishedAt: "23/11/2021"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLA737CmxYQkwfaYxVIiY0s" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PC"
updates:
  - "The Lord of the Rings The Battle for Middle-earth II The Rise of the Witch-king PC 2021"
---
{% include 'article.html' %}
