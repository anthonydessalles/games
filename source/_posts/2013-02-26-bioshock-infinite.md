---
title: "Bioshock Infinite"
slug: "bioshock-infinite"
post_date: "26/02/2013"
files:
playlists:
  1325:
    title: "BioShock Infinite PS3 2021"
    publishedAt: "29/12/2021"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyL4UeVdVAGtkYsWroeqDYMr" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS3"
updates:
  - "BioShock Infinite PS3 2021"
---
{% include 'article.html' %}
