---
title: "Hyrule Warriors Definitive Edition"
slug: "hyrule-warriors-definitive-edition"
post_date: "22/03/2018"
files:
  27405:
    filename: "2023050708505500_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050708505500_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27406:
    filename: "2023050708511600_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050708511600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27407:
    filename: "2023050708513900_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050708513900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27408:
    filename: "2023050709130300_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709130300_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27409:
    filename: "2023050709132000_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709132000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27410:
    filename: "2023050709132500_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709132500_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27411:
    filename: "2023050709133500_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709133500_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27412:
    filename: "2023050709133800_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709133800_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27413:
    filename: "2023050709134500_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709134500_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27414:
    filename: "2023050709134800_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709134800_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27415:
    filename: "2023050709140300_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709140300_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27416:
    filename: "2023050709140800_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709140800_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27417:
    filename: "2023050709141000_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709141000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27418:
    filename: "2023050709142700_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709142700_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27419:
    filename: "2023050709143200_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709143200_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27420:
    filename: "2023050709145400_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709145400_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27421:
    filename: "2023050709145800_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709145800_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27422:
    filename: "2023050709150200_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709150200_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27423:
    filename: "2023050709150400_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709150400_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27424:
    filename: "2023050709150800_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709150800_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27425:
    filename: "2023050709152400_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709152400_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27426:
    filename: "2023050709154900_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709154900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27427:
    filename: "2023050709155000_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709155000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27428:
    filename: "2023050709155600_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709155600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27429:
    filename: "2023050709160200_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709160200_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27430:
    filename: "2023050709162500_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709162500_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27431:
    filename: "2023050709164900_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709164900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27432:
    filename: "2023050709170000_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709170000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27433:
    filename: "2023050709171900_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709171900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27434:
    filename: "2023050709180800_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709180800_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27435:
    filename: "2023050709182100_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709182100_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27436:
    filename: "2023050709182900_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709182900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27437:
    filename: "2023050709183600_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709183600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27438:
    filename: "2023050709184000_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709184000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27439:
    filename: "2023050709184600_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709184600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27440:
    filename: "2023050709191100_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709191100_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27441:
    filename: "2023050709191500_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709191500_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27442:
    filename: "2023050709191800_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709191800_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27443:
    filename: "2023050709193000_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709193000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27444:
    filename: "2023050709201400_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709201400_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27445:
    filename: "2023050709201900_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709201900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27446:
    filename: "2023050709202700_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709202700_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27447:
    filename: "2023050709203500_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709203500_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27448:
    filename: "2023050709204400_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709204400_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27449:
    filename: "2023050709204800_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709204800_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27450:
    filename: "2023050709205500_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709205500_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27451:
    filename: "2023050709210000_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709210000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27452:
    filename: "2023050709210600_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709210600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27453:
    filename: "2023050709211000_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709211000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27454:
    filename: "2023050709211500_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709211500_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27455:
    filename: "2023050709212000_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709212000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27456:
    filename: "2023050709212600_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709212600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27457:
    filename: "2023050709213700_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709213700_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27458:
    filename: "2023050709221500_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709221500_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27459:
    filename: "2023050709222000_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709222000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27460:
    filename: "2023050709223000_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709223000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27461:
    filename: "2023050709224800_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709224800_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27462:
    filename: "2023050709230000_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709230000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27463:
    filename: "2023050709231000_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709231000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27464:
    filename: "2023050709231900_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709231900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27465:
    filename: "2023050709232900_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709232900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27466:
    filename: "2023050709233600_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709233600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27467:
    filename: "2023050709241700_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709241700_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27468:
    filename: "2023050709242500_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709242500_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27469:
    filename: "2023050709242800_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709242800_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27470:
    filename: "2023050709250100_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709250100_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27471:
    filename: "2023050709251000_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709251000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27472:
    filename: "2023050709252900_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709252900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27473:
    filename: "2023050709255300_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709255300_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27474:
    filename: "2023050709255400_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709255400_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27475:
    filename: "2023050709271600_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709271600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27476:
    filename: "2023050709272000_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709272000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27477:
    filename: "2023050709272200_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709272200_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27478:
    filename: "2023050709272900_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709272900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27479:
    filename: "2023050709275100_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709275100_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27480:
    filename: "2023050709293200_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709293200_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27481:
    filename: "2023050709301100_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709301100_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27482:
    filename: "2023050709302500_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709302500_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27483:
    filename: "2023050709303300_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709303300_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27484:
    filename: "2023050709310500_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709310500_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27485:
    filename: "2023050709333600_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709333600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27486:
    filename: "2023050709334900_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709334900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27487:
    filename: "2023050709335300_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709335300_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27488:
    filename: "2023050709335800_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709335800_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27489:
    filename: "2023050709341100_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709341100_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27490:
    filename: "2023050709341400_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709341400_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27491:
    filename: "2023050709350500_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709350500_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27492:
    filename: "2023050709354400_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709354400_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27493:
    filename: "2023050709354700_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709354700_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27494:
    filename: "2023050709355800_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709355800_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27495:
    filename: "2023050709361800_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709361800_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27496:
    filename: "2023050709363100_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709363100_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27497:
    filename: "2023050709363700_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709363700_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27498:
    filename: "2023050709364600_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709364600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27499:
    filename: "2023050709380700_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709380700_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27500:
    filename: "2023050709382100_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709382100_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27501:
    filename: "2023050709383500_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709383500_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27502:
    filename: "2023050709384100_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709384100_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27503:
    filename: "2023050709390000_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709390000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27504:
    filename: "2023050709393400_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050709393400_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27505:
    filename: "2023050710163200_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710163200_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27506:
    filename: "2023050710170500_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710170500_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27507:
    filename: "2023050710173300_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710173300_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27508:
    filename: "2023050710190400_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710190400_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27509:
    filename: "2023050710194000_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710194000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27510:
    filename: "2023050710195700_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710195700_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27511:
    filename: "2023050710195900_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710195900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27512:
    filename: "2023050710200200_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710200200_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27513:
    filename: "2023050710200400_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710200400_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27514:
    filename: "2023050710200900_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710200900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27515:
    filename: "2023050710201900_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710201900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27516:
    filename: "2023050710202600_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710202600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27517:
    filename: "2023050710204300_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710204300_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27518:
    filename: "2023050710205000_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710205000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27519:
    filename: "2023050710205500_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710205500_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27520:
    filename: "2023050710210100_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710210100_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27521:
    filename: "2023050710210900_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710210900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27522:
    filename: "2023050710212000_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710212000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27523:
    filename: "2023050710213000_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710213000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27524:
    filename: "2023050710213900_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710213900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27525:
    filename: "2023050710214800_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710214800_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27526:
    filename: "2023050710215700_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710215700_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27527:
    filename: "2023050710220400_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710220400_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27528:
    filename: "2023050710221600_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710221600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27529:
    filename: "2023050710241800_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710241800_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27530:
    filename: "2023050710243000_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710243000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27531:
    filename: "2023050710243600_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710243600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27532:
    filename: "2023050710244600_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710244600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27533:
    filename: "2023050710244900_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710244900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27534:
    filename: "2023050710250000_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710250000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27535:
    filename: "2023050710250600_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710250600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27536:
    filename: "2023050710251200_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710251200_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27537:
    filename: "2023050710251600_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710251600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27538:
    filename: "2023050710252000_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710252000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27539:
    filename: "2023050710252700_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710252700_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27540:
    filename: "2023050710272000_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710272000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27541:
    filename: "2023050710272800_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710272800_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27542:
    filename: "2023050710275300_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050710275300_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27543:
    filename: "2023050809542000_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050809542000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27544:
    filename: "2023050809542900_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050809542900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27545:
    filename: "2023050809543200_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050809543200_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27546:
    filename: "2023050809543600_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050809543600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27547:
    filename: "2023050809554700_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050809554700_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27548:
    filename: "2023050809561300_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050809561300_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27549:
    filename: "2023050809562600_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050809562600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27550:
    filename: "2023050809571200_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050809571200_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27551:
    filename: "2023050809573300_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050809573300_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27552:
    filename: "2023050809581300_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050809581300_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27553:
    filename: "2023050810040200_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023050810040200_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27554:
    filename: "2023052314594700_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052314594700_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27555:
    filename: "2023052315000200_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315000200_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27556:
    filename: "2023052315002100_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315002100_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27557:
    filename: "2023052315015200_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315015200_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27558:
    filename: "2023052315033700_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315033700_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27559:
    filename: "2023052315051300_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315051300_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27560:
    filename: "2023052315052000_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315052000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27561:
    filename: "2023052315060500_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315060500_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27562:
    filename: "2023052315063500_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315063500_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27563:
    filename: "2023052315080400_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315080400_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27564:
    filename: "2023052315083600_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315083600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27565:
    filename: "2023052315090600_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315090600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27566:
    filename: "2023052315091600_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315091600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27567:
    filename: "2023052315093600_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315093600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27568:
    filename: "2023052315105900_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315105900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27569:
    filename: "2023052315114100_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315114100_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27570:
    filename: "2023052315124800_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315124800_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27571:
    filename: "2023052315141600_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315141600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27572:
    filename: "2023052315144500_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315144500_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27573:
    filename: "2023052315145500_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315145500_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27574:
    filename: "2023052315161800_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315161800_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27575:
    filename: "2023052315170400_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315170400_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27576:
    filename: "2023052315175300_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315175300_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27577:
    filename: "2023052315182600_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315182600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27578:
    filename: "2023052315183600_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315183600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27579:
    filename: "2023052315184000_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315184000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27580:
    filename: "2023052315185200_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315185200_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27581:
    filename: "2023052315193900_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315193900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27582:
    filename: "2023052315195200_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315195200_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  27583:
    filename: "2023052315205900_c.jpg"
    date: "23/05/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023052315205900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28089:
    filename: "2023061414304900_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061414304900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28090:
    filename: "2023061414305300_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061414305300_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28091:
    filename: "2023061414340100_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061414340100_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28092:
    filename: "2023061414345300_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061414345300_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28093:
    filename: "2023061414351600_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061414351600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28094:
    filename: "2023061414365100_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061414365100_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28095:
    filename: "2023061414370200_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061414370200_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28096:
    filename: "2023061414401500_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061414401500_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28097:
    filename: "2023061414442700_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061414442700_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28098:
    filename: "2023061414443900_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061414443900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28099:
    filename: "2023061414481900_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061414481900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28100:
    filename: "2023061414494800_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061414494800_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28101:
    filename: "2023061414500100_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061414500100_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28102:
    filename: "2023061414505100_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061414505100_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28103:
    filename: "2023061414505900_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061414505900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28104:
    filename: "2023061414511400_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061414511400_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28105:
    filename: "2023061414512200_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061414512200_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28106:
    filename: "2023061414515500_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061414515500_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28107:
    filename: "2023061414530200_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061414530200_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28108:
    filename: "2023061414533100_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061414533100_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28109:
    filename: "2023061414540800_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061414540800_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28110:
    filename: "2023061414561300_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061414561300_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28111:
    filename: "2023061414561600_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061414561600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28112:
    filename: "2023061414565900_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061414565900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28113:
    filename: "2023061414593600_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061414593600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28114:
    filename: "2023061415010100_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061415010100_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28115:
    filename: "2023061415014000_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061415014000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28116:
    filename: "2023061415020100_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061415020100_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28117:
    filename: "2023061415022500_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061415022500_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28118:
    filename: "2023061415093800_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061415093800_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28119:
    filename: "2023061415100900_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061415100900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28120:
    filename: "2023061415103100_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061415103100_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28121:
    filename: "2023061415103700_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061415103700_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28122:
    filename: "2023061415114600_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061415114600_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28123:
    filename: "2023061415115800_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061415115800_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28124:
    filename: "2023061415123400_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061415123400_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28125:
    filename: "2023061415125900_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061415125900_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28126:
    filename: "2023061415135400_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061415135400_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28127:
    filename: "2023061415142300_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061415142300_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28128:
    filename: "2023061415150800_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061415150800_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28129:
    filename: "2023061415153700_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061415153700_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28130:
    filename: "2023061415155000_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061415155000_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28131:
    filename: "2023061415160100_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061415160100_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
  28132:
    filename: "2023061415163500_c.jpg"
    date: "14/06/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Hyrule Warriors Definitive Edition Switch 2023 - Screenshots/2023061415163500_c.jpg"
    path: "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
playlists:
tags:
  - "TonyTonyX042"
  - "SnappedWheel884"
categories:
  - "Switch"
updates:
  - "Hyrule Warriors Definitive Edition Switch 2023 - Screenshots"
---
{% include 'article.html' %}