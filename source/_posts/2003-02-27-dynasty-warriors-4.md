---
title: "Dynasty Warriors 4"
slug: "dynasty-warriors-4"
post_date: "27/02/2003"
files:
playlists:
  1012:
    title: "Dynasty Warriors 4 XB 2020"
    publishedAt: "20/01/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyKxNnUTtO37C1RIQVu7SS8X" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "XB"
updates:
  - "Dynasty Warriors 4 XB 2020"
---
{% include 'article.html' %}
