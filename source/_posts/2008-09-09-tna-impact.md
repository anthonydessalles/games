---
title: "TNA Impact"
slug: "tna-impact"
post_date: "09/09/2008"
files:
playlists:
  33:
    title: "TNA Impact XB360 2020"
    publishedAt: "12/09/2020"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyINCduTHgz0Eqsmg4eCmFHf" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "XB360"
updates:
  - "TNA Impact XB360 2020"
---
{% include 'article.html' %}
