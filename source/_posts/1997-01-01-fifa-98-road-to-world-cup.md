---
title: "FIFA 98 Road to World Cup"
slug: "fifa-98-road-to-world-cup"
post_date: "01/01/1997"
files:
playlists:
  1672:
    title: "FIFA 98 Road to World Cup N64 2022"
    publishedAt: "17/05/2022"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyK45A5zH0-63S7_H_HSjfrc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "N64"
updates:
  - "FIFA 98 Road to World Cup N64 2022"
---
{% include 'article.html' %}
