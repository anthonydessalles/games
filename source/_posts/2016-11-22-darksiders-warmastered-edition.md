---
title: "Darksiders Warmastered Edition"
slug: "darksiders-warmastered-edition"
post_date: "22/11/2016"
files:
  37016:
    filename: "Darksiders Warmastered Edition_20240131130134.jpg"
    date: "31/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Darksiders Warmastered Edition PS4 2024 - Screenshots/Darksiders Warmastered Edition_20240131130134.jpg"
    path: "Darksiders Warmastered Edition PS4 2024 - Screenshots"
  37017:
    filename: "Darksiders Warmastered Edition_20240131130148.jpg"
    date: "31/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Darksiders Warmastered Edition PS4 2024 - Screenshots/Darksiders Warmastered Edition_20240131130148.jpg"
    path: "Darksiders Warmastered Edition PS4 2024 - Screenshots"
  37018:
    filename: "Darksiders Warmastered Edition_20240131130204.jpg"
    date: "31/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Darksiders Warmastered Edition PS4 2024 - Screenshots/Darksiders Warmastered Edition_20240131130204.jpg"
    path: "Darksiders Warmastered Edition PS4 2024 - Screenshots"
  37019:
    filename: "Darksiders Warmastered Edition_20240131130214.jpg"
    date: "31/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Darksiders Warmastered Edition PS4 2024 - Screenshots/Darksiders Warmastered Edition_20240131130214.jpg"
    path: "Darksiders Warmastered Edition PS4 2024 - Screenshots"
  37020:
    filename: "Darksiders Warmastered Edition_20240131130229.jpg"
    date: "31/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Darksiders Warmastered Edition PS4 2024 - Screenshots/Darksiders Warmastered Edition_20240131130229.jpg"
    path: "Darksiders Warmastered Edition PS4 2024 - Screenshots"
  37021:
    filename: "Darksiders Warmastered Edition_20240131130252.jpg"
    date: "31/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Darksiders Warmastered Edition PS4 2024 - Screenshots/Darksiders Warmastered Edition_20240131130252.jpg"
    path: "Darksiders Warmastered Edition PS4 2024 - Screenshots"
  37022:
    filename: "Darksiders Warmastered Edition_20240131130425.jpg"
    date: "31/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Darksiders Warmastered Edition PS4 2024 - Screenshots/Darksiders Warmastered Edition_20240131130425.jpg"
    path: "Darksiders Warmastered Edition PS4 2024 - Screenshots"
  37023:
    filename: "Darksiders Warmastered Edition_20240131130438.jpg"
    date: "31/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Darksiders Warmastered Edition PS4 2024 - Screenshots/Darksiders Warmastered Edition_20240131130438.jpg"
    path: "Darksiders Warmastered Edition PS4 2024 - Screenshots"
  37024:
    filename: "Darksiders Warmastered Edition_20240131130448.jpg"
    date: "31/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Darksiders Warmastered Edition PS4 2024 - Screenshots/Darksiders Warmastered Edition_20240131130448.jpg"
    path: "Darksiders Warmastered Edition PS4 2024 - Screenshots"
  37025:
    filename: "Darksiders Warmastered Edition_20240131130458.jpg"
    date: "31/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Darksiders Warmastered Edition PS4 2024 - Screenshots/Darksiders Warmastered Edition_20240131130458.jpg"
    path: "Darksiders Warmastered Edition PS4 2024 - Screenshots"
  37026:
    filename: "Darksiders Warmastered Edition_20240131130521.jpg"
    date: "31/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Darksiders Warmastered Edition PS4 2024 - Screenshots/Darksiders Warmastered Edition_20240131130521.jpg"
    path: "Darksiders Warmastered Edition PS4 2024 - Screenshots"
  37027:
    filename: "Darksiders Warmastered Edition_20240131130546.jpg"
    date: "31/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Darksiders Warmastered Edition PS4 2024 - Screenshots/Darksiders Warmastered Edition_20240131130546.jpg"
    path: "Darksiders Warmastered Edition PS4 2024 - Screenshots"
  37028:
    filename: "Darksiders Warmastered Edition_20240131130601.jpg"
    date: "31/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Darksiders Warmastered Edition PS4 2024 - Screenshots/Darksiders Warmastered Edition_20240131130601.jpg"
    path: "Darksiders Warmastered Edition PS4 2024 - Screenshots"
  37029:
    filename: "Darksiders Warmastered Edition_20240131130630.jpg"
    date: "31/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Darksiders Warmastered Edition PS4 2024 - Screenshots/Darksiders Warmastered Edition_20240131130630.jpg"
    path: "Darksiders Warmastered Edition PS4 2024 - Screenshots"
  37030:
    filename: "Darksiders Warmastered Edition_20240131130640.jpg"
    date: "31/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Darksiders Warmastered Edition PS4 2024 - Screenshots/Darksiders Warmastered Edition_20240131130640.jpg"
    path: "Darksiders Warmastered Edition PS4 2024 - Screenshots"
playlists:
  2296:
    title: "Darksiders Warmastered Edition Steam 2023"
    publishedAt: "20/11/2023"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyKleVEJbY-gBYyH_0sUw6zY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS4"
  - "Steam"
updates:
  - "Darksiders Warmastered Edition PS4 2024 - Screenshots"
  - "Darksiders Warmastered Edition Steam 2023"
---
{% include 'article.html' %}