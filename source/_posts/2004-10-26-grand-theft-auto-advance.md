---
title: "Grand Theft Auto Advance"
slug: "grand-theft-auto-advance"
post_date: "26/10/2004"
files:
playlists:
  1811:
    title: "Grand Theft Auto Advance GBA 2023"
    publishedAt: "12/01/2023"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyI1m_GyQYiYt2f0KUa-RyTD" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "GBA"
updates:
  - "Grand Theft Auto Advance GBA 2023"
---
{% include 'article.html' %}
