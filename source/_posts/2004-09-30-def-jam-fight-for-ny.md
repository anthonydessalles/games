---
title: "Def Jam Fight for NY"
slug: "def-jam-fight-for-ny"
post_date: "30/09/2004"
files:
playlists:
  1016:
    title: "Def Jam Fight for NY GC 2020"
    publishedAt: "20/01/2021"
    itemsCount: "7"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyIkQmGLQBYvAR7pB8AyrnTv" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "GC"
updates:
  - "Def Jam Fight for NY GC 2020"
---
{% include 'article.html' %}
