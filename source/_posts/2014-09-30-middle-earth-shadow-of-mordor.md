---
title: "Middle-earth Shadow of Mordor"
slug: "middle-earth-shadow-of-mordor"
post_date: "30/09/2014"
files:
  37031:
    filename: "Middle-earth™_ Shadow of Mordor™_20240201102518.jpg"
    date: "01/02/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Middle-earth Shadow of Mordor PS4 2024 - Screenshots/Middle-earth™_ Shadow of Mordor™_20240201102518.jpg"
    path: "Middle-earth Shadow of Mordor PS4 2024 - Screenshots"
  37032:
    filename: "Middle-earth™_ Shadow of Mordor™_20240201102555.jpg"
    date: "01/02/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Middle-earth Shadow of Mordor PS4 2024 - Screenshots/Middle-earth™_ Shadow of Mordor™_20240201102555.jpg"
    path: "Middle-earth Shadow of Mordor PS4 2024 - Screenshots"
  37033:
    filename: "Middle-earth™_ Shadow of Mordor™_20240201102618.jpg"
    date: "01/02/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Middle-earth Shadow of Mordor PS4 2024 - Screenshots/Middle-earth™_ Shadow of Mordor™_20240201102618.jpg"
    path: "Middle-earth Shadow of Mordor PS4 2024 - Screenshots"
  37034:
    filename: "Middle-earth™_ Shadow of Mordor™_20240201102630.jpg"
    date: "01/02/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Middle-earth Shadow of Mordor PS4 2024 - Screenshots/Middle-earth™_ Shadow of Mordor™_20240201102630.jpg"
    path: "Middle-earth Shadow of Mordor PS4 2024 - Screenshots"
  37035:
    filename: "Middle-earth™_ Shadow of Mordor™_20240201102643.jpg"
    date: "01/02/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Middle-earth Shadow of Mordor PS4 2024 - Screenshots/Middle-earth™_ Shadow of Mordor™_20240201102643.jpg"
    path: "Middle-earth Shadow of Mordor PS4 2024 - Screenshots"
playlists:
  2486:
    title: "Middle-earth Shadow of Mordor PS4 2024"
    publishedAt: "01/02/2024"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyL2J_NB_V6Ho25Q57Xx7YA6" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS4"
updates:
  - "Middle-earth Shadow of Mordor PS4 2024 - Screenshots"
  - "Middle-earth Shadow of Mordor PS4 2024"
---
{% include 'article.html' %}