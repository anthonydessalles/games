---
title: "Showdown Legends of Wrestling"
slug: "showdown-legends-of-wrestling"
post_date: "22/06/2004"
files:
playlists:
  984:
    title: "Showdown Legends of Wrestling XB 2020"
    publishedAt: "22/01/2021"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJn4k7VgkS26XdWKXaZmHNO" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "XB"
updates:
  - "Showdown Legends of Wrestling XB 2020"
---
{% include 'article.html' %}
