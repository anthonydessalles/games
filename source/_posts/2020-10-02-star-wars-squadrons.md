---
title: "Star Wars Squadrons"
slug: "star-wars-squadrons"
post_date: "02/10/2020"
files:
  18814:
    filename: "Star_Wars_Squadrons_PS4_202101_1348571279499132929-ErcWbl-W4AEN19Z.jpg"
    date: "11/01/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Squadrons PS4 202101 - Screenshots/Star_Wars_Squadrons_PS4_202101_1348571279499132929-ErcWbl-W4AEN19Z.jpg"
    path: "Star Wars Squadrons PS4 202101 - Screenshots"
  18812:
    filename: "Star_Wars_Squadrons_PS4_202101_1348571279499132929-ErcWbLlXEAMG3Z7.jpg"
    date: "11/01/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Squadrons PS4 202101 - Screenshots/Star_Wars_Squadrons_PS4_202101_1348571279499132929-ErcWbLlXEAMG3Z7.jpg"
    path: "Star Wars Squadrons PS4 202101 - Screenshots"
  18813:
    filename: "Star_Wars_Squadrons_PS4_202101_1348571279499132929-ErcWbZeXIAAMJFZ.jpg"
    date: "11/01/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Squadrons PS4 202101 - Screenshots/Star_Wars_Squadrons_PS4_202101_1348571279499132929-ErcWbZeXIAAMJFZ.jpg"
    path: "Star Wars Squadrons PS4 202101 - Screenshots"
playlists:
  1032:
    title: "Star Wars Squadrons PS4 2021"
    publishedAt: "13/01/2021"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLLTCIBNmO4-tShG3gOQVug" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS4"
updates:
  - "Star Wars Squadrons PS4 202101 - Screenshots"
  - "Star Wars Squadrons PS4 2021"
---
{% include 'article.html' %}
