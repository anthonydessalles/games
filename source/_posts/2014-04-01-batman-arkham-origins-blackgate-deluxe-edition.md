---
title: "Batman Arkham Origins Blackgate Deluxe Edition"
slug: "batman-arkham-origins-blackgate-deluxe-edition"
post_date: "01/04/2014"
files:
playlists:
  843:
    title: "Batman Arkham Origins Blackgate Deluxe Edition Steam 2020"
    publishedAt: "13/11/2020"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJiviixrBHtP273vcRuT9tQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "Steam"
updates:
  - "Batman Arkham Origins Blackgate Deluxe Edition Steam 2020"
---
{% include 'article.html' %}