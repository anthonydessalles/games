---
title: "Batman Arkham Asylum"
slug: "batman-arkham-asylum"
post_date: "28/08/2009"
files:
  48781:
    filename: "202407270100 (01).png"
    date: "26/07/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman Arkham Asylum XB360 2024 - Screenshots/202407270100 (01).png"
    path: "Batman Arkham Asylum XB360 2024 - Screenshots"
  48782:
    filename: "202407270100 (02).png"
    date: "26/07/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman Arkham Asylum XB360 2024 - Screenshots/202407270100 (02).png"
    path: "Batman Arkham Asylum XB360 2024 - Screenshots"
  48783:
    filename: "202407270100 (03).png"
    date: "26/07/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman Arkham Asylum XB360 2024 - Screenshots/202407270100 (03).png"
    path: "Batman Arkham Asylum XB360 2024 - Screenshots"
playlists:
  2269:
    title: "Batman Arkham Asylum PC 2023"
    publishedAt: "16/11/2023"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLIvVVdyio6R-Vl9N13JwG5" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
  105:
    title: "Batman Arkham Asylum XB360 2014"
    publishedAt: "05/08/2020"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJFWecMKXMfwp1Ol_OvXJ4x" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "XB360"
  - "PC"
updates:
  - "Batman Arkham Asylum XB360 2024 - Screenshots"
  - "Batman Arkham Asylum PC 2023"
  - "Batman Arkham Asylum XB360 2014"
---
{% include 'article.html' %}