---
title: "Tomb Raider"
slug: "tomb-raider"
post_date: "25/10/1996"
files:
  11809:
    filename: "Tomb Raider-201127-164321.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164321.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11810:
    filename: "Tomb Raider-201127-164333.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164333.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11811:
    filename: "Tomb Raider-201127-164421.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164421.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11812:
    filename: "Tomb Raider-201127-164428.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164428.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11813:
    filename: "Tomb Raider-201127-164436.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164436.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11814:
    filename: "Tomb Raider-201127-164444.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164444.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11815:
    filename: "Tomb Raider-201127-164450.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164450.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11816:
    filename: "Tomb Raider-201127-164502.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164502.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11817:
    filename: "Tomb Raider-201127-164516.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164516.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11818:
    filename: "Tomb Raider-201127-164523.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164523.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11819:
    filename: "Tomb Raider-201127-164534.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164534.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11820:
    filename: "Tomb Raider-201127-164542.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164542.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11821:
    filename: "Tomb Raider-201127-164548.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164548.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11822:
    filename: "Tomb Raider-201127-164559.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164559.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11823:
    filename: "Tomb Raider-201127-164612.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164612.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11824:
    filename: "Tomb Raider-201127-164631.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164631.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11825:
    filename: "Tomb Raider-201127-164638.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164638.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11826:
    filename: "Tomb Raider-201127-164651.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164651.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11827:
    filename: "Tomb Raider-201127-164657.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164657.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11828:
    filename: "Tomb Raider-201127-164707.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164707.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11829:
    filename: "Tomb Raider-201127-164717.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164717.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11830:
    filename: "Tomb Raider-201127-164813.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164813.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11831:
    filename: "Tomb Raider-201127-164821.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164821.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11832:
    filename: "Tomb Raider-201127-164830.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164830.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11833:
    filename: "Tomb Raider-201127-164839.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164839.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11834:
    filename: "Tomb Raider-201127-164850.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164850.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11835:
    filename: "Tomb Raider-201127-164900.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164900.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11836:
    filename: "Tomb Raider-201127-164907.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164907.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11837:
    filename: "Tomb Raider-201127-164914.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164914.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11838:
    filename: "Tomb Raider-201127-164923.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164923.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11839:
    filename: "Tomb Raider-201127-164930.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164930.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11840:
    filename: "Tomb Raider-201127-164936.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164936.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11841:
    filename: "Tomb Raider-201127-164942.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164942.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11842:
    filename: "Tomb Raider-201127-164953.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-164953.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11843:
    filename: "Tomb Raider-201127-165003.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165003.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11844:
    filename: "Tomb Raider-201127-165010.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165010.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11845:
    filename: "Tomb Raider-201127-165016.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165016.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11846:
    filename: "Tomb Raider-201127-165024.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165024.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11847:
    filename: "Tomb Raider-201127-165032.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165032.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11848:
    filename: "Tomb Raider-201127-165039.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165039.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11849:
    filename: "Tomb Raider-201127-165045.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165045.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11850:
    filename: "Tomb Raider-201127-165051.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165051.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11851:
    filename: "Tomb Raider-201127-165055.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165055.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11852:
    filename: "Tomb Raider-201127-165102.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165102.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11853:
    filename: "Tomb Raider-201127-165110.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165110.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11854:
    filename: "Tomb Raider-201127-165118.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165118.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11855:
    filename: "Tomb Raider-201127-165125.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165125.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11856:
    filename: "Tomb Raider-201127-165132.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165132.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11857:
    filename: "Tomb Raider-201127-165142.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165142.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11858:
    filename: "Tomb Raider-201127-165148.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165148.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11859:
    filename: "Tomb Raider-201127-165153.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165153.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11860:
    filename: "Tomb Raider-201127-165159.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165159.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11861:
    filename: "Tomb Raider-201127-165205.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165205.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11862:
    filename: "Tomb Raider-201127-165210.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165210.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11863:
    filename: "Tomb Raider-201127-165215.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165215.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11864:
    filename: "Tomb Raider-201127-165222.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165222.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11865:
    filename: "Tomb Raider-201127-165234.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165234.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11866:
    filename: "Tomb Raider-201127-165242.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165242.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11867:
    filename: "Tomb Raider-201127-165256.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165256.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11868:
    filename: "Tomb Raider-201127-165314.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165314.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11869:
    filename: "Tomb Raider-201127-165327.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165327.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11870:
    filename: "Tomb Raider-201127-165358.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165358.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11871:
    filename: "Tomb Raider-201127-165408.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165408.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11872:
    filename: "Tomb Raider-201127-165430.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165430.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11873:
    filename: "Tomb Raider-201127-165440.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165440.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11874:
    filename: "Tomb Raider-201127-165517.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165517.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11875:
    filename: "Tomb Raider-201127-165548.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165548.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11876:
    filename: "Tomb Raider-201127-165602.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165602.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11877:
    filename: "Tomb Raider-201127-165609.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165609.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11878:
    filename: "Tomb Raider-201127-165620.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165620.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11879:
    filename: "Tomb Raider-201127-165639.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165639.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11880:
    filename: "Tomb Raider-201127-165658.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165658.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11881:
    filename: "Tomb Raider-201127-165723.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165723.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11882:
    filename: "Tomb Raider-201127-165755.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165755.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11883:
    filename: "Tomb Raider-201127-165803.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165803.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11884:
    filename: "Tomb Raider-201127-165816.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165816.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11885:
    filename: "Tomb Raider-201127-165912.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165912.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11886:
    filename: "Tomb Raider-201127-165924.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165924.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11887:
    filename: "Tomb Raider-201127-165940.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-165940.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11888:
    filename: "Tomb Raider-201127-170000.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-170000.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11889:
    filename: "Tomb Raider-201127-170015.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-170015.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11890:
    filename: "Tomb Raider-201127-170025.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-170025.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11891:
    filename: "Tomb Raider-201127-170032.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-170032.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
  11892:
    filename: "Tomb Raider-201127-170040.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider PS1 2020 - Screenshots/Tomb Raider-201127-170040.png"
    path: "Tomb Raider PS1 2020 - Screenshots"
playlists:
tags:
  - "TonyTonyX042"
categories:
  - "PS1"
updates:
  - "Tomb Raider PS1 2020 - Screenshots"
---
{% include 'article.html' %}
