---
title: "Dead Rising"
slug: "dead-rising"
post_date: "07/09/2006"
files:
playlists:
  2295:
    title: "Dead Rising Steam 2023"
    publishedAt: "21/11/2023"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyIhLjBHDW1Fmnsxh9UtoqpB" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "Steam"
updates:
  - "Dead Rising Steam 2023"
---
{% include 'article.html' %}