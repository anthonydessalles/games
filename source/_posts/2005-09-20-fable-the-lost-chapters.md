---
title: "Fable The Lost Chapters"
slug: "fable-the-lost-chapters"
post_date: "20/09/2005"
files:
playlists:
  1011:
    title: "Fable The Lost Chapters XB 2020"
    publishedAt: "20/01/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLILgST-tfsAeAIRg-09r7B" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "XB"
updates:
  - "Fable The Lost Chapters XB 2020"
---
{% include 'article.html' %}
