---
title: "Pokémon Red Version"
french_title: "Pokémon Rouge"
slug: "pokemon-red-version"
post_date: "27/02/1996"
files:
playlists:
  1595:
    title: "Pokémon Red Version GB 2022"
    publishedAt: "14/04/2022"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyKRLJfTkbaExraeztmEXr9i" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "GB"
updates:
  - "Pokémon Red Version GB 2022"
---
{% include 'article.html' %}
