---
title: "Wipeout 3"
slug: "wipeout-3"
post_date: "08/09/1999"
files:
  13924:
    filename: "Wipeout 3-201127-181633.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-181633.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13925:
    filename: "Wipeout 3-201127-181651.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-181651.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13926:
    filename: "Wipeout 3-201127-181708.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-181708.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13927:
    filename: "Wipeout 3-201127-181716.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-181716.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13928:
    filename: "Wipeout 3-201127-181726.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-181726.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13929:
    filename: "Wipeout 3-201127-181732.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-181732.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13930:
    filename: "Wipeout 3-201127-181744.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-181744.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13931:
    filename: "Wipeout 3-201127-181754.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-181754.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13932:
    filename: "Wipeout 3-201127-181805.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-181805.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13933:
    filename: "Wipeout 3-201127-181811.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-181811.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13934:
    filename: "Wipeout 3-201127-181817.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-181817.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13935:
    filename: "Wipeout 3-201127-181824.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-181824.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13936:
    filename: "Wipeout 3-201127-181829.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-181829.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13937:
    filename: "Wipeout 3-201127-181836.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-181836.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13938:
    filename: "Wipeout 3-201127-181842.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-181842.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13939:
    filename: "Wipeout 3-201127-181849.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-181849.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13940:
    filename: "Wipeout 3-201127-181857.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-181857.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13941:
    filename: "Wipeout 3-201127-181902.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-181902.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13942:
    filename: "Wipeout 3-201127-181907.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-181907.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13943:
    filename: "Wipeout 3-201127-181914.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-181914.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13944:
    filename: "Wipeout 3-201127-181918.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-181918.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13945:
    filename: "Wipeout 3-201127-181925.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-181925.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13946:
    filename: "Wipeout 3-201127-181932.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-181932.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13947:
    filename: "Wipeout 3-201127-181938.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-181938.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13948:
    filename: "Wipeout 3-201127-181944.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-181944.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13949:
    filename: "Wipeout 3-201127-181947.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-181947.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13950:
    filename: "Wipeout 3-201127-181957.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-181957.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13951:
    filename: "Wipeout 3-201127-182007.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-182007.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13952:
    filename: "Wipeout 3-201127-182012.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-182012.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13953:
    filename: "Wipeout 3-201127-182019.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-182019.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13954:
    filename: "Wipeout 3-201127-182023.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-182023.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13955:
    filename: "Wipeout 3-201127-182029.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-182029.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13956:
    filename: "Wipeout 3-201127-182034.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-182034.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13957:
    filename: "Wipeout 3-201127-182042.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-182042.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13958:
    filename: "Wipeout 3-201127-182054.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-182054.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13959:
    filename: "Wipeout 3-201127-182100.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-182100.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13960:
    filename: "Wipeout 3-201127-182105.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-182105.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13961:
    filename: "Wipeout 3-201127-182113.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-182113.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13962:
    filename: "Wipeout 3-201127-182121.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-182121.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13963:
    filename: "Wipeout 3-201127-182138.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-182138.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13964:
    filename: "Wipeout 3-201127-182147.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-182147.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13965:
    filename: "Wipeout 3-201127-182159.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-182159.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13966:
    filename: "Wipeout 3-201127-182216.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-182216.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13967:
    filename: "Wipeout 3-201127-182231.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-182231.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13968:
    filename: "Wipeout 3-201127-182237.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-182237.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13969:
    filename: "Wipeout 3-201127-182301.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-182301.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
  13970:
    filename: "Wipeout 3-201127-182310.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wipeout 3 PS1 2020 - Screenshots/Wipeout 3-201127-182310.png"
    path: "Wipeout 3 PS1 2020 - Screenshots"
playlists:
tags:
  - "TonyTonyX042"
categories:
  - "PS1"
updates:
  - "Wipeout 3 PS1 2020 - Screenshots"
---
{% include 'article.html' %}
