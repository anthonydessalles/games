---
title: "James Bond 007 Nightfire"
french_title: "007 Nightfire"
slug: "james-bond-007-nightfire"
post_date: "28/11/2002"
files:
playlists:
  1023:
    title: "007 Nightfire GC 2020"
    publishedAt: "20/01/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLYrzrQ1yfvM2ds753Dc0L2" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "GC"
updates:
  - "007 Nightfire GC 2020"
---
{% include 'article.html' %}
