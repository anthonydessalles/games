---
title: "Asterix at the Olympic Games"
french_title: "Astérix aux Jeux Olympiques"
slug: "asterix-at-the-olympic-games"
post_date: "09/11/2007"
files:
playlists:
  918:
    title: "Astérix aux Jeux Olympiques PS2 2021"
    publishedAt: "26/01/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyIkkU8rY1-v7EpMiwtB-XpL" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS2"
updates:
  - "Astérix aux Jeux Olympiques PS2 2021"
---
{% include 'article.html' %}
