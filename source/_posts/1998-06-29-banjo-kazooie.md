---
title: "Banjo-Kazooie"
slug: "banjo-kazooie"
post_date: "29/06/1998"
files:
playlists:
  1547:
    title: "Banjo-Kazooie N64 2022"
    publishedAt: "22/03/2022"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyKRxGKu5sk4lWrwAGJo-mik" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "N64"
updates:
  - "Banjo-Kazooie N64 2022"
---
{% include 'article.html' %}
