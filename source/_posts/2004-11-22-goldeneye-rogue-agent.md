---
title: "GoldenEye Rogue Agent"
french_title: "GoldenEye Au Service du Mal"
slug: "goldeneye-rogue-agent"
post_date: "22/11/2004"
files:
playlists:
  1005:
    title: "GoldenEye Au Service du Mal XB 2020"
    publishedAt: "21/01/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyL6Y-A7nUJRluBJFkfUeXY2" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "XB"
updates:
  - "GoldenEye Au Service du Mal XB 2020"
---
{% include 'article.html' %}
