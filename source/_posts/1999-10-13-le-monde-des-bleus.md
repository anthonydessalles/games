---
title: "Le Monde des Bleus"
slug: "le-monde-des-bleus"
post_date: "13/10/1999"
files:
  7231:
    filename: "Le Monde des Bleus-201123-194609.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-194609.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7232:
    filename: "Le Monde des Bleus-201123-194616.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-194616.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7233:
    filename: "Le Monde des Bleus-201123-194643.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-194643.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7234:
    filename: "Le Monde des Bleus-201123-194649.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-194649.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7235:
    filename: "Le Monde des Bleus-201123-194702.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-194702.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7236:
    filename: "Le Monde des Bleus-201123-194712.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-194712.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7237:
    filename: "Le Monde des Bleus-201123-194722.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-194722.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7238:
    filename: "Le Monde des Bleus-201123-194729.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-194729.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7239:
    filename: "Le Monde des Bleus-201123-194740.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-194740.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7240:
    filename: "Le Monde des Bleus-201123-194748.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-194748.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7241:
    filename: "Le Monde des Bleus-201123-194754.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-194754.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7242:
    filename: "Le Monde des Bleus-201123-194802.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-194802.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7243:
    filename: "Le Monde des Bleus-201123-194809.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-194809.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7244:
    filename: "Le Monde des Bleus-201123-194816.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-194816.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7245:
    filename: "Le Monde des Bleus-201123-194822.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-194822.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7246:
    filename: "Le Monde des Bleus-201123-194829.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-194829.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7247:
    filename: "Le Monde des Bleus-201123-194836.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-194836.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7248:
    filename: "Le Monde des Bleus-201123-194848.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-194848.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7249:
    filename: "Le Monde des Bleus-201123-194902.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-194902.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7250:
    filename: "Le Monde des Bleus-201123-194911.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-194911.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7251:
    filename: "Le Monde des Bleus-201123-194920.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-194920.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7252:
    filename: "Le Monde des Bleus-201123-194926.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-194926.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7253:
    filename: "Le Monde des Bleus-201123-194933.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-194933.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7254:
    filename: "Le Monde des Bleus-201123-194943.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-194943.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7255:
    filename: "Le Monde des Bleus-201123-194952.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-194952.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7256:
    filename: "Le Monde des Bleus-201123-194958.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-194958.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7257:
    filename: "Le Monde des Bleus-201123-195004.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195004.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7258:
    filename: "Le Monde des Bleus-201123-195014.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195014.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7259:
    filename: "Le Monde des Bleus-201123-195028.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195028.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7260:
    filename: "Le Monde des Bleus-201123-195043.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195043.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7261:
    filename: "Le Monde des Bleus-201123-195101.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195101.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7262:
    filename: "Le Monde des Bleus-201123-195115.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195115.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7263:
    filename: "Le Monde des Bleus-201123-195122.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195122.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7264:
    filename: "Le Monde des Bleus-201123-195126.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195126.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7265:
    filename: "Le Monde des Bleus-201123-195139.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195139.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7266:
    filename: "Le Monde des Bleus-201123-195151.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195151.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7267:
    filename: "Le Monde des Bleus-201123-195201.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195201.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7268:
    filename: "Le Monde des Bleus-201123-195209.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195209.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7269:
    filename: "Le Monde des Bleus-201123-195219.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195219.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7270:
    filename: "Le Monde des Bleus-201123-195230.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195230.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7271:
    filename: "Le Monde des Bleus-201123-195239.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195239.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7272:
    filename: "Le Monde des Bleus-201123-195301.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195301.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7273:
    filename: "Le Monde des Bleus-201123-195317.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195317.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7274:
    filename: "Le Monde des Bleus-201123-195324.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195324.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7275:
    filename: "Le Monde des Bleus-201123-195335.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195335.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7276:
    filename: "Le Monde des Bleus-201123-195343.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195343.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7277:
    filename: "Le Monde des Bleus-201123-195351.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195351.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7278:
    filename: "Le Monde des Bleus-201123-195358.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195358.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7279:
    filename: "Le Monde des Bleus-201123-195409.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195409.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7280:
    filename: "Le Monde des Bleus-201123-195415.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195415.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7281:
    filename: "Le Monde des Bleus-201123-195423.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195423.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7282:
    filename: "Le Monde des Bleus-201123-195430.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195430.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7283:
    filename: "Le Monde des Bleus-201123-195440.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195440.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7284:
    filename: "Le Monde des Bleus-201123-195447.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195447.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7285:
    filename: "Le Monde des Bleus-201123-195459.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195459.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7286:
    filename: "Le Monde des Bleus-201123-195513.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195513.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7287:
    filename: "Le Monde des Bleus-201123-195530.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195530.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7288:
    filename: "Le Monde des Bleus-201123-195540.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195540.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7289:
    filename: "Le Monde des Bleus-201123-195601.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195601.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7290:
    filename: "Le Monde des Bleus-201123-195608.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195608.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7291:
    filename: "Le Monde des Bleus-201123-195616.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195616.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7292:
    filename: "Le Monde des Bleus-201123-195711.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195711.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7293:
    filename: "Le Monde des Bleus-201123-195720.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195720.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7294:
    filename: "Le Monde des Bleus-201123-195729.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195729.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7295:
    filename: "Le Monde des Bleus-201123-195750.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195750.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7296:
    filename: "Le Monde des Bleus-201123-195756.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195756.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7297:
    filename: "Le Monde des Bleus-201123-195804.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195804.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7298:
    filename: "Le Monde des Bleus-201123-195815.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195815.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7299:
    filename: "Le Monde des Bleus-201123-195823.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195823.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7300:
    filename: "Le Monde des Bleus-201123-195840.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195840.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7301:
    filename: "Le Monde des Bleus-201123-195848.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195848.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7302:
    filename: "Le Monde des Bleus-201123-195910.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195910.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7303:
    filename: "Le Monde des Bleus-201123-195925.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195925.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7304:
    filename: "Le Monde des Bleus-201123-195952.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195952.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7305:
    filename: "Le Monde des Bleus-201123-195958.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-195958.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7306:
    filename: "Le Monde des Bleus-201123-200008.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-200008.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7307:
    filename: "Le Monde des Bleus-201123-200016.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-200016.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7308:
    filename: "Le Monde des Bleus-201123-200023.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-200023.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7309:
    filename: "Le Monde des Bleus-201123-200101.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-200101.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7310:
    filename: "Le Monde des Bleus-201123-200109.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-200109.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7311:
    filename: "Le Monde des Bleus-201123-200135.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-200135.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7312:
    filename: "Le Monde des Bleus-201123-200239.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-200239.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7313:
    filename: "Le Monde des Bleus-201123-200312.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-200312.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7314:
    filename: "Le Monde des Bleus-201123-200338.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-200338.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7315:
    filename: "Le Monde des Bleus-201123-200344.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-200344.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7316:
    filename: "Le Monde des Bleus-201123-200352.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-200352.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7317:
    filename: "Le Monde des Bleus-201123-200412.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-200412.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7318:
    filename: "Le Monde des Bleus-201123-200425.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-200425.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7319:
    filename: "Le Monde des Bleus-201123-200437.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-200437.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7320:
    filename: "Le Monde des Bleus-201123-200510.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-200510.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7321:
    filename: "Le Monde des Bleus-201123-200532.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-200532.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7322:
    filename: "Le Monde des Bleus-201123-200540.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-200540.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7323:
    filename: "Le Monde des Bleus-201123-200613.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-200613.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7324:
    filename: "Le Monde des Bleus-201123-200727.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-200727.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7325:
    filename: "Le Monde des Bleus-201123-200847.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-200847.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7326:
    filename: "Le Monde des Bleus-201123-200854.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-200854.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7327:
    filename: "Le Monde des Bleus-201123-200907.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-200907.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7328:
    filename: "Le Monde des Bleus-201123-200928.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-200928.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7329:
    filename: "Le Monde des Bleus-201123-200935.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-200935.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7330:
    filename: "Le Monde des Bleus-201123-201151.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-201151.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7331:
    filename: "Le Monde des Bleus-201123-201226.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-201226.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7332:
    filename: "Le Monde des Bleus-201123-201235.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-201235.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7333:
    filename: "Le Monde des Bleus-201123-201245.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-201245.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7334:
    filename: "Le Monde des Bleus-201123-201252.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-201252.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7335:
    filename: "Le Monde des Bleus-201123-201302.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-201302.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7336:
    filename: "Le Monde des Bleus-201123-201400.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-201400.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7337:
    filename: "Le Monde des Bleus-201123-201406.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-201406.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7338:
    filename: "Le Monde des Bleus-201123-201420.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-201420.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7339:
    filename: "Le Monde des Bleus-201123-201427.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-201427.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7340:
    filename: "Le Monde des Bleus-201123-201437.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-201437.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7341:
    filename: "Le Monde des Bleus-201123-201537.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-201537.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7342:
    filename: "Le Monde des Bleus-201123-201544.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-201544.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7343:
    filename: "Le Monde des Bleus-201123-201552.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-201552.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7344:
    filename: "Le Monde des Bleus-201123-201601.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-201601.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7345:
    filename: "Le Monde des Bleus-201123-201611.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-201611.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7346:
    filename: "Le Monde des Bleus-201123-201620.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-201620.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7347:
    filename: "Le Monde des Bleus-201123-201626.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-201626.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7348:
    filename: "Le Monde des Bleus-201123-201632.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-201632.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7349:
    filename: "Le Monde des Bleus-201123-201638.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-201638.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7350:
    filename: "Le Monde des Bleus-201123-201651.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-201651.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7351:
    filename: "Le Monde des Bleus-201123-201658.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-201658.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7352:
    filename: "Le Monde des Bleus-201123-201710.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-201710.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7353:
    filename: "Le Monde des Bleus-201123-201720.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-201720.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
  7354:
    filename: "Le Monde des Bleus-201123-201730.png"
    date: "23/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Monde des Bleus PS1 2020 - Screenshots/Le Monde des Bleus-201123-201730.png"
    path: "Le Monde des Bleus PS1 2020 - Screenshots"
playlists:
tags:
  - "TonyTonyX042"
categories:
  - "PS1"
updates:
  - "Le Monde des Bleus PS1 2020 - Screenshots"
---
{% include 'article.html' %}
