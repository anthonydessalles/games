---
title: "The Sims Bustin Out"
french_title: "Les Sims Permis de Sortir"
slug: "the-sims-bustin-out"
post_date: "02/12/2003"
files:
playlists:
  997:
    title: "Les Sims Permis de Sortir XB 2020"
    publishedAt: "21/01/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLrNN7x4B0cymkOTQ7uyE97" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "XB"
updates:
  - "Les Sims Permis de Sortir XB 2020"
---
{% include 'article.html' %}
