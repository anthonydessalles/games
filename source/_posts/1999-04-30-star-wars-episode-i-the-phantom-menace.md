---
title: "Star Wars Episode I The Phantom Menace"
slug: "star-wars-episode-i-the-phantom-menace"
post_date: "30/04/1999"
files:
  9716:
    filename: "Star Wars Episode I The Phantom Menace-201125-213148.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-213148.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9717:
    filename: "Star Wars Episode I The Phantom Menace-201125-213158.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-213158.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9718:
    filename: "Star Wars Episode I The Phantom Menace-201125-213208.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-213208.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9719:
    filename: "Star Wars Episode I The Phantom Menace-201125-213223.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-213223.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9720:
    filename: "Star Wars Episode I The Phantom Menace-201125-213317.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-213317.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9721:
    filename: "Star Wars Episode I The Phantom Menace-201125-213333.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-213333.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9722:
    filename: "Star Wars Episode I The Phantom Menace-201125-213352.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-213352.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9723:
    filename: "Star Wars Episode I The Phantom Menace-201125-213420.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-213420.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9724:
    filename: "Star Wars Episode I The Phantom Menace-201125-213428.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-213428.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9725:
    filename: "Star Wars Episode I The Phantom Menace-201125-213443.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-213443.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9726:
    filename: "Star Wars Episode I The Phantom Menace-201125-213452.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-213452.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9727:
    filename: "Star Wars Episode I The Phantom Menace-201125-213503.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-213503.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9728:
    filename: "Star Wars Episode I The Phantom Menace-201125-213515.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-213515.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9729:
    filename: "Star Wars Episode I The Phantom Menace-201125-213531.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-213531.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9730:
    filename: "Star Wars Episode I The Phantom Menace-201125-225313.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-225313.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9731:
    filename: "Star Wars Episode I The Phantom Menace-201125-225325.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-225325.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9732:
    filename: "Star Wars Episode I The Phantom Menace-201125-225338.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-225338.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9733:
    filename: "Star Wars Episode I The Phantom Menace-201125-225349.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-225349.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9734:
    filename: "Star Wars Episode I The Phantom Menace-201125-225357.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-225357.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9735:
    filename: "Star Wars Episode I The Phantom Menace-201125-225457.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-225457.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9736:
    filename: "Star Wars Episode I The Phantom Menace-201125-225518.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-225518.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9737:
    filename: "Star Wars Episode I The Phantom Menace-201125-225628.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-225628.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9738:
    filename: "Star Wars Episode I The Phantom Menace-201125-225636.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-225636.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9739:
    filename: "Star Wars Episode I The Phantom Menace-201125-225645.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-225645.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9740:
    filename: "Star Wars Episode I The Phantom Menace-201125-225658.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-225658.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9741:
    filename: "Star Wars Episode I The Phantom Menace-201125-225705.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-225705.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9742:
    filename: "Star Wars Episode I The Phantom Menace-201125-225718.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-225718.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9743:
    filename: "Star Wars Episode I The Phantom Menace-201125-225732.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-225732.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9744:
    filename: "Star Wars Episode I The Phantom Menace-201125-225737.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-225737.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9745:
    filename: "Star Wars Episode I The Phantom Menace-201125-225745.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-225745.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9746:
    filename: "Star Wars Episode I The Phantom Menace-201125-225752.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-225752.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9747:
    filename: "Star Wars Episode I The Phantom Menace-201125-225806.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-225806.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9748:
    filename: "Star Wars Episode I The Phantom Menace-201125-225814.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-225814.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9749:
    filename: "Star Wars Episode I The Phantom Menace-201125-225822.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-225822.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9750:
    filename: "Star Wars Episode I The Phantom Menace-201125-225900.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-225900.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9751:
    filename: "Star Wars Episode I The Phantom Menace-201125-225913.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-225913.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9752:
    filename: "Star Wars Episode I The Phantom Menace-201125-225919.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-225919.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9753:
    filename: "Star Wars Episode I The Phantom Menace-201125-225939.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-225939.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9754:
    filename: "Star Wars Episode I The Phantom Menace-201125-225952.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-225952.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9755:
    filename: "Star Wars Episode I The Phantom Menace-201125-230009.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-230009.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9756:
    filename: "Star Wars Episode I The Phantom Menace-201125-230022.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-230022.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9757:
    filename: "Star Wars Episode I The Phantom Menace-201125-230030.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-230030.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9758:
    filename: "Star Wars Episode I The Phantom Menace-201125-230038.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-230038.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9759:
    filename: "Star Wars Episode I The Phantom Menace-201125-230049.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-230049.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9760:
    filename: "Star Wars Episode I The Phantom Menace-201125-230058.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-230058.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9761:
    filename: "Star Wars Episode I The Phantom Menace-201125-230106.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-230106.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9762:
    filename: "Star Wars Episode I The Phantom Menace-201125-230118.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-230118.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  9763:
    filename: "Star Wars Episode I The Phantom Menace-201125-230133.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots/Star Wars Episode I The Phantom Menace-201125-230133.png"
    path: "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
playlists:
  2628:
    title: "Star Wars Episode I The Phantom Menace PS1 2024"
    publishedAt: "21/06/2024"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyKV0UpI0ykWZupOmwgPDE9a" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS1"
updates:
  - "Star Wars Episode I The Phantom Menace PS1 2020 - Screenshots"
  - "Star Wars Episode I The Phantom Menace PS1 2024"
---
{% include 'article.html' %}