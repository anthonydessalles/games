---
title: "Baldur's Gate Dark Alliance"
slug: "baldur-s-gate-dark-alliance"
post_date: "23/04/2003"
files:
playlists:
  1022:
    title: "Baldur's Gate Dark Alliance GC 2020"
    publishedAt: "20/01/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLlim8IOLZf-ZEfgx0w2W8H" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "GC"
updates:
  - "Baldur's Gate Dark Alliance GC 2020"
---
{% include 'article.html' %}
