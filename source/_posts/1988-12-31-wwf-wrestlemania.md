---
title: "WWF WrestleMania"
slug: "wwf-wrestlemania"
post_date: "31/12/1988"
files:
playlists:
  1856:
    title: "WWF WrestleMania NES 2023"
    publishedAt: "31/03/2023"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJzLzoMfLtUuVV9sHNPNdHC" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "NES"
updates:
  - "WWF WrestleMania NES 2023"
---
{% include 'article.html' %}