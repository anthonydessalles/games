---
title: "X-Men Next Dimension"
slug: "x-men-next-dimension"
post_date: "29/11/2002"
files:
playlists:
  922:
    title: "X-Men Next Dimension GC 2020"
    publishedAt: "25/01/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLAACo2FnMF6IaZhDa2_7lF" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "GC"
updates:
  - "X-Men Next Dimension GC 2020"
---
{% include 'article.html' %}
