---
title: "SoulCalibur VI"
slug: "soulcalibur-vi"
post_date: "18/10/2018"
files:
  33093:
    filename: "20230920_01_F6i2nZ4XcAAZIWP.jpg"
    date: "20/09/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur VI PS4 2023 - Screenshots/20230920_01_F6i2nZ4XcAAZIWP.jpg"
    path: "SoulCalibur VI PS4 2023 - Screenshots"
  33094:
    filename: "20230920_02_F6i2nI4XAAAWA_2.jpg"
    date: "20/09/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur VI PS4 2023 - Screenshots/20230920_02_F6i2nI4XAAAWA_2.jpg"
    path: "SoulCalibur VI PS4 2023 - Screenshots"
  33095:
    filename: "20230920_03_F6i2mzjW8AASH5c.jpg"
    date: "20/09/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur VI PS4 2023 - Screenshots/20230920_03_F6i2mzjW8AASH5c.jpg"
    path: "SoulCalibur VI PS4 2023 - Screenshots"
playlists:
  2101:
    title: "SoulCalibur VI PS4 2023"
    publishedAt: "21/09/2023"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJXvYvENEkZ24ebeccYEzrz" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS4"
updates:
  - "SoulCalibur VI PS4 2023 - Screenshots"
  - "SoulCalibur VI PS4 2023"
---
{% include 'article.html' %}