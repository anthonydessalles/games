---
title: "Astérix & Obélix"
slug: "asterix-obelix"
post_date: "28/09/1995"
files:
  4857:
    filename: " Astérix & Obélix-201113-170239.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix & Obélix GB 2020 - Screenshots/ Astérix & Obélix-201113-170239.png"
    path: "Astérix & Obélix GB 2020 - Screenshots"
  4858:
    filename: " Astérix & Obélix-201113-170252.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix & Obélix GB 2020 - Screenshots/ Astérix & Obélix-201113-170252.png"
    path: "Astérix & Obélix GB 2020 - Screenshots"
  4859:
    filename: " Astérix & Obélix-201113-170304.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix & Obélix GB 2020 - Screenshots/ Astérix & Obélix-201113-170304.png"
    path: "Astérix & Obélix GB 2020 - Screenshots"
  4860:
    filename: " Astérix & Obélix-201113-170326.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix & Obélix GB 2020 - Screenshots/ Astérix & Obélix-201113-170326.png"
    path: "Astérix & Obélix GB 2020 - Screenshots"
  4861:
    filename: " Astérix & Obélix-201113-170345.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix & Obélix GB 2020 - Screenshots/ Astérix & Obélix-201113-170345.png"
    path: "Astérix & Obélix GB 2020 - Screenshots"
  4862:
    filename: " Astérix & Obélix-201113-170400.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix & Obélix GB 2020 - Screenshots/ Astérix & Obélix-201113-170400.png"
    path: "Astérix & Obélix GB 2020 - Screenshots"
  4863:
    filename: " Astérix & Obélix-201113-170409.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix & Obélix GB 2020 - Screenshots/ Astérix & Obélix-201113-170409.png"
    path: "Astérix & Obélix GB 2020 - Screenshots"
  4864:
    filename: " Astérix & Obélix-201113-170424.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix & Obélix GB 2020 - Screenshots/ Astérix & Obélix-201113-170424.png"
    path: "Astérix & Obélix GB 2020 - Screenshots"
  4865:
    filename: " Astérix & Obélix-201113-170445.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix & Obélix GB 2020 - Screenshots/ Astérix & Obélix-201113-170445.png"
    path: "Astérix & Obélix GB 2020 - Screenshots"
  4866:
    filename: " Astérix & Obélix-201113-170646.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix & Obélix GB 2020 - Screenshots/ Astérix & Obélix-201113-170646.png"
    path: "Astérix & Obélix GB 2020 - Screenshots"
  4867:
    filename: " Astérix & Obélix-201113-170701.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix & Obélix GB 2020 - Screenshots/ Astérix & Obélix-201113-170701.png"
    path: "Astérix & Obélix GB 2020 - Screenshots"
  4868:
    filename: " Astérix & Obélix-201113-170710.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix & Obélix GB 2020 - Screenshots/ Astérix & Obélix-201113-170710.png"
    path: "Astérix & Obélix GB 2020 - Screenshots"
  4869:
    filename: " Astérix & Obélix-201113-170752.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix & Obélix GB 2020 - Screenshots/ Astérix & Obélix-201113-170752.png"
    path: "Astérix & Obélix GB 2020 - Screenshots"
  4870:
    filename: " Astérix & Obélix-201113-170814.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix & Obélix GB 2020 - Screenshots/ Astérix & Obélix-201113-170814.png"
    path: "Astérix & Obélix GB 2020 - Screenshots"
  4871:
    filename: " Astérix & Obélix-201113-170822.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix & Obélix GB 2020 - Screenshots/ Astérix & Obélix-201113-170822.png"
    path: "Astérix & Obélix GB 2020 - Screenshots"
  4872:
    filename: " Astérix & Obélix-201113-170830.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix & Obélix GB 2020 - Screenshots/ Astérix & Obélix-201113-170830.png"
    path: "Astérix & Obélix GB 2020 - Screenshots"
  4873:
    filename: " Astérix & Obélix-201113-170837.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix & Obélix GB 2020 - Screenshots/ Astérix & Obélix-201113-170837.png"
    path: "Astérix & Obélix GB 2020 - Screenshots"
  4874:
    filename: " Astérix & Obélix-201113-170846.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix & Obélix GB 2020 - Screenshots/ Astérix & Obélix-201113-170846.png"
    path: "Astérix & Obélix GB 2020 - Screenshots"
  4875:
    filename: " Astérix & Obélix-201113-170851.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix & Obélix GB 2020 - Screenshots/ Astérix & Obélix-201113-170851.png"
    path: "Astérix & Obélix GB 2020 - Screenshots"
  4876:
    filename: " Astérix & Obélix-201113-170905.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix & Obélix GB 2020 - Screenshots/ Astérix & Obélix-201113-170905.png"
    path: "Astérix & Obélix GB 2020 - Screenshots"
playlists:
  811:
    title: "Astérix & Obélix GB 2020"
    publishedAt: "06/12/2020"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLdxyUGRUCu2hz0-EToAOC9" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "GB"
updates:
  - "Astérix & Obélix GB 2020 - Screenshots"
  - "Astérix & Obélix GB 2020"
---
{% include 'article.html' %}
