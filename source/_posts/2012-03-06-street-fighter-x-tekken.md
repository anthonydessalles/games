---
title: "Street Fighter X Tekken"
slug: "street-fighter-x-tekken"
post_date: "06/03/2012"
files:
playlists:
  2098:
    title: "Street Fighter X Tekken PC 2023"
    publishedAt: "12/07/2023"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyIZzdvj0D8yOjsn0pfyR2Ws" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PC"
updates:
  - "Street Fighter X Tekken PC 2023"
---
{% include 'article.html' %}