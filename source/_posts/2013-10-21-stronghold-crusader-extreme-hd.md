---
title: "Stronghold Crusader Extreme HD"
slug: "stronghold-crusader-extreme-hd"
post_date: "21/10/2013"
files:
playlists:
  842:
    title: "Stronghold Crusader Extreme HD Steam 2020"
    publishedAt: "17/11/2020"
    itemsCount: "12"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLK4fGg23eFoaDsklbb8mva" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "Steam"
updates:
  - "Stronghold Crusader Extreme HD Steam 2020"
---
{% include 'article.html' %}
