---
title: "Wii Sports"
slug: "wii-sports"
post_date: "19/11/2006"
files:
playlists:
  967:
    title: "Wii Sports Wii 2020"
    publishedAt: "25/01/2021"
    itemsCount: "3"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLYkX1OgagfQhdZC4jKaXU9" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "Wii"
updates:
  - "Wii Sports Wii 2020"
---
{% include 'article.html' %}
