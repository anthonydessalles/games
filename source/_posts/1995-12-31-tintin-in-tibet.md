---
title: "Tintin in Tibet"
slug: "tintin-in-tibet"
post_date: "31/12/1995"
files:
  11527:
    filename: "Tintin in Tibet-201113-183649.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-183649.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
  11528:
    filename: "Tintin in Tibet-201113-183720.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-183720.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
  11529:
    filename: "Tintin in Tibet-201113-183727.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-183727.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
  11530:
    filename: "Tintin in Tibet-201113-183739.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-183739.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
  11531:
    filename: "Tintin in Tibet-201113-183749.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-183749.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
  11532:
    filename: "Tintin in Tibet-201113-183759.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-183759.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
  11533:
    filename: "Tintin in Tibet-201113-183805.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-183805.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
  11534:
    filename: "Tintin in Tibet-201113-183813.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-183813.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
  11535:
    filename: "Tintin in Tibet-201113-183831.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-183831.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
  11536:
    filename: "Tintin in Tibet-201113-183838.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-183838.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
  11537:
    filename: "Tintin in Tibet-201113-183846.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-183846.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
  11538:
    filename: "Tintin in Tibet-201113-183907.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-183907.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
  11539:
    filename: "Tintin in Tibet-201113-183922.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-183922.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
  11540:
    filename: "Tintin in Tibet-201113-184050.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-184050.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
  11541:
    filename: "Tintin in Tibet-201113-184058.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-184058.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
  11542:
    filename: "Tintin in Tibet-201113-184110.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-184110.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
  11543:
    filename: "Tintin in Tibet-201113-184119.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-184119.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
  11544:
    filename: "Tintin in Tibet-201113-184131.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-184131.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
  11545:
    filename: "Tintin in Tibet-201113-184141.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-184141.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
  11546:
    filename: "Tintin in Tibet-201113-184156.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-184156.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
  11547:
    filename: "Tintin in Tibet-201113-184212.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-184212.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
  11548:
    filename: "Tintin in Tibet-201113-184219.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-184219.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
  11549:
    filename: "Tintin in Tibet-201113-184230.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-184230.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
  11550:
    filename: "Tintin in Tibet-201113-184239.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-184239.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
  11551:
    filename: "Tintin in Tibet-201113-184245.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-184245.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
  11552:
    filename: "Tintin in Tibet-201113-184252.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-184252.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
  11553:
    filename: "Tintin in Tibet-201113-184300.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-184300.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
  11554:
    filename: "Tintin in Tibet-201113-184303.png"
    date: "13/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tintin in Tibet GB 2020 - Screenshots/Tintin in Tibet-201113-184303.png"
    path: "Tintin in Tibet GB 2020 - Screenshots"
playlists:
  745:
    title: "Tintin in Tibet GB 2020"
    publishedAt: "06/12/2020"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLu5c1OaR4S5CDcy4Obc0yK" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "GB"
updates:
  - "Tintin in Tibet GB 2020 - Screenshots"
  - "Tintin in Tibet GB 2020"
---
{% include 'article.html' %}
