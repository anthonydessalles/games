---
title: "New Play Control Mario Power Tennis"
french_title: "Nouvelle Façon de Jouer Mario Power Tennis"
slug: "new-play-control-mario-power-tennis"
post_date: "15/01/2009"
files:
playlists:
  1245:
    title: "Mario Power Tennis Wii 2021"
    publishedAt: "20/09/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLmnLo8XmTiVpKi1DKcmZvV" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "Wii"
updates:
  - "Mario Power Tennis Wii 2021"
---
{% include 'article.html' %}
