---
title: "The Getaway Black Monday"
slug: "the-getaway-black-monday"
post_date: "12/11/2004"
files:
playlists:
  1656:
    title: "The Getaway Black Monday PS2 2022"
    publishedAt: "17/05/2022"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyIqzJQlDj4MBSF0GwilU2pM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS2"
updates:
  - "The Getaway Black Monday PS2 2022"
---
{% include 'article.html' %}
