---
title: "Prince of Persia The Sands of Time"
french_title: "Prince of Persia Les Sables du Temps"
slug: "prince-of-persia-the-sands-of-time"
post_date: "28/10/2003"
files:
playlists:
  1242:
    title: "Prince of Persia Les Sables du Temps PS2 2021"
    publishedAt: "20/09/2021"
    itemsCount: "4"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyIgGhiNEf9JV7EkHxb0wl4W" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS2"
updates:
  - "Prince of Persia Les Sables du Temps PS2 2021"
---
{% include 'article.html' %}
