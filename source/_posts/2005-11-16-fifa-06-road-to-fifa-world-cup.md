---
title: "FIFA 06 Road to FIFA World Cup"
french_title: "FIFA 06 En Route pour la Coupe du Monde de la FIFA"
slug: "fifa-06-road-to-fifa-world-cup"
post_date: "16/11/2005"
files:
playlists:
  2078:
    title: "FIFA 06 En Route pour la Coupe du Monde de la FIFA XB360 2023"
    publishedAt: "22/06/2023"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJs2R9YuhoyCHDYnS6JAc9p" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "XB360"
updates:
  - "FIFA 06 En Route pour la Coupe du Monde de la FIFA XB360 2023"
---
{% include 'article.html' %}
