---
title: "Duke Nukem Forever"
slug: "duke-nukem-forever"
post_date: "10/06/2011"
files:
playlists:
  2406:
    title: "Duke Nukem Forever XB360 2014"
    publishedAt: "20/12/2023"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyIvosbk0NhZRnkXKS3mY5Cf" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "XB360"
updates:
  - "Duke Nukem Forever XB360 2014"
---
{% include 'article.html' %}