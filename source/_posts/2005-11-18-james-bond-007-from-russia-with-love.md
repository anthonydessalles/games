---
title: "James Bond 007 From Russia with Love"
french_title: "007 Bons Baisers de Russie"
slug: "james-bond-007-from-russia-with-love"
post_date: "18/11/2005"
files:
playlists:
  921:
    title: "007 Bons Baisers de Russie PS2 2021"
    publishedAt: "26/01/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyKO9KbqnQb-YfmjrDCugchR" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS2"
updates:
  - "007 Bons Baisers de Russie PS2 2021"
---
{% include 'article.html' %}
