---
title: "Spider-Man Battle for New York"
slug: "spider-man-battle-for-new-york"
post_date: "14/11/2006"
files:
  9155:
    filename: "Spider-Man Battle for New York-201114-175005.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175005.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9156:
    filename: "Spider-Man Battle for New York-201114-175014.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175014.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9157:
    filename: "Spider-Man Battle for New York-201114-175020.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175020.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9158:
    filename: "Spider-Man Battle for New York-201114-175026.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175026.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9159:
    filename: "Spider-Man Battle for New York-201114-175032.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175032.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9160:
    filename: "Spider-Man Battle for New York-201114-175037.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175037.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9161:
    filename: "Spider-Man Battle for New York-201114-175044.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175044.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9162:
    filename: "Spider-Man Battle for New York-201114-175051.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175051.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9163:
    filename: "Spider-Man Battle for New York-201114-175100.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175100.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9164:
    filename: "Spider-Man Battle for New York-201114-175112.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175112.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9165:
    filename: "Spider-Man Battle for New York-201114-175121.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175121.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9166:
    filename: "Spider-Man Battle for New York-201114-175131.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175131.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9167:
    filename: "Spider-Man Battle for New York-201114-175138.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175138.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9168:
    filename: "Spider-Man Battle for New York-201114-175147.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175147.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9169:
    filename: "Spider-Man Battle for New York-201114-175154.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175154.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9170:
    filename: "Spider-Man Battle for New York-201114-175201.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175201.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9171:
    filename: "Spider-Man Battle for New York-201114-175208.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175208.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9172:
    filename: "Spider-Man Battle for New York-201114-175215.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175215.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9173:
    filename: "Spider-Man Battle for New York-201114-175222.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175222.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9174:
    filename: "Spider-Man Battle for New York-201114-175230.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175230.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9175:
    filename: "Spider-Man Battle for New York-201114-175237.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175237.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9176:
    filename: "Spider-Man Battle for New York-201114-175248.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175248.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9177:
    filename: "Spider-Man Battle for New York-201114-175255.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175255.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9178:
    filename: "Spider-Man Battle for New York-201114-175301.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175301.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9179:
    filename: "Spider-Man Battle for New York-201114-175309.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175309.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9180:
    filename: "Spider-Man Battle for New York-201114-175316.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175316.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9181:
    filename: "Spider-Man Battle for New York-201114-175322.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175322.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9182:
    filename: "Spider-Man Battle for New York-201114-175329.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175329.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9183:
    filename: "Spider-Man Battle for New York-201114-175336.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175336.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9184:
    filename: "Spider-Man Battle for New York-201114-175344.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175344.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9185:
    filename: "Spider-Man Battle for New York-201114-175353.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175353.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9186:
    filename: "Spider-Man Battle for New York-201114-175411.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175411.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9187:
    filename: "Spider-Man Battle for New York-201114-175423.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175423.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9188:
    filename: "Spider-Man Battle for New York-201114-175447.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175447.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9189:
    filename: "Spider-Man Battle for New York-201114-175458.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175458.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9190:
    filename: "Spider-Man Battle for New York-201114-175510.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175510.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9191:
    filename: "Spider-Man Battle for New York-201114-175531.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175531.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9192:
    filename: "Spider-Man Battle for New York-201114-175546.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175546.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9193:
    filename: "Spider-Man Battle for New York-201114-175556.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175556.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
  9194:
    filename: "Spider-Man Battle for New York-201114-175602.png"
    date: "14/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Spider-Man Battle for New York GBA 2020 - Screenshots/Spider-Man Battle for New York-201114-175602.png"
    path: "Spider-Man Battle for New York GBA 2020 - Screenshots"
playlists:
  769:
    title: "Spider-Man Battle for New York GBA 2020"
    publishedAt: "08/12/2020"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJTqjXw5nDng2QCte9U2X2q" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "GBA"
updates:
  - "Spider-Man Battle for New York GBA 2020 - Screenshots"
  - "Spider-Man Battle for New York GBA 2020"
---
{% include 'article.html' %}
