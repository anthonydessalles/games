---
title: "The Lord of the Rings The Battle for Middle-earth"
french_title: "Le Seigneur des Anneaux La Bataille pour la Terre du Milieu"
slug: "the-lord-of-the-rings-the-battle-for-middle-earth"
post_date: "06/12/2004"
files:
playlists:
  409:
    title: "The Lord of the Rings The Battle for Middle-earth PC 2020"
    publishedAt: "23/05/2020"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyL2_1QNjCLXrFMKDueiFvx7" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PC"
updates:
  - "The Lord of the Rings The Battle for Middle-earth PC 2020"
---
{% include 'article.html' %}
