---
title: "Samurai Warriors 2"
slug: "samurai-warriors-2"
post_date: "24/02/2006"
files:
playlists:
  908:
    title: "Samurai Warriors 2 PS2 2021"
    publishedAt: "26/01/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyIVa5ex84_RlVzQf1EYJ8rR" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS2"
updates:
  - "Samurai Warriors 2 PS2 2021"
---
{% include 'article.html' %}
