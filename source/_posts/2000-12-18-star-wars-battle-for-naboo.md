---
title: "Star Wars Battle for Naboo"
slug: "star-wars-battle-for-naboo"
post_date: "18/12/2000"
files:
playlists:
  2099:
    title: "Star Wars Battle for Naboo PC 2023"
    publishedAt: "12/07/2023"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyIYIS9YkKT11mct_QUuXBHl" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PC"
updates:
  - "Star Wars Battle for Naboo PC 2023"
---
{% include 'article.html' %}