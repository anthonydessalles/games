---
title: "Batman Forever The Arcade Game"
slug: "batman-forever-the-arcade-game"
post_date: "21/11/1996"
files:
playlists:
  81:
    title: "Batman Forever The Arcade Game PS1 2020"
    publishedAt: "12/09/2020"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJj4PSiCB6sETL8tmFzjqiY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS1"
updates:
  - "Batman Forever The Arcade Game PS1 2020"
---
{% include 'article.html' %}
