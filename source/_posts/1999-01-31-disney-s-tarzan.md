---
title: "Disney's Tarzan"
french_title: "Tarzan"
slug: "disney-s-tarzan"
post_date: "31/01/1999"
files:
  5690:
    filename: "Disney's Tarzan-201122-174439.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-174439.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5691:
    filename: "Disney's Tarzan-201122-174452.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-174452.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5692:
    filename: "Disney's Tarzan-201122-174512.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-174512.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5693:
    filename: "Disney's Tarzan-201122-174537.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-174537.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5694:
    filename: "Disney's Tarzan-201122-174547.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-174547.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5695:
    filename: "Disney's Tarzan-201122-174554.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-174554.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5696:
    filename: "Disney's Tarzan-201122-174559.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-174559.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5697:
    filename: "Disney's Tarzan-201122-174606.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-174606.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5698:
    filename: "Disney's Tarzan-201122-174615.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-174615.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5699:
    filename: "Disney's Tarzan-201122-174632.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-174632.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5700:
    filename: "Disney's Tarzan-201122-174643.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-174643.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5701:
    filename: "Disney's Tarzan-201122-174656.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-174656.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5702:
    filename: "Disney's Tarzan-201122-174706.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-174706.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5703:
    filename: "Disney's Tarzan-201122-174743.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-174743.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5704:
    filename: "Disney's Tarzan-201122-174749.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-174749.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5705:
    filename: "Disney's Tarzan-201122-174758.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-174758.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5706:
    filename: "Disney's Tarzan-201122-174814.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-174814.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5707:
    filename: "Disney's Tarzan-201122-174825.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-174825.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5708:
    filename: "Disney's Tarzan-201122-174838.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-174838.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5709:
    filename: "Disney's Tarzan-201122-174846.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-174846.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5710:
    filename: "Disney's Tarzan-201122-174900.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-174900.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5711:
    filename: "Disney's Tarzan-201122-174923.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-174923.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5712:
    filename: "Disney's Tarzan-201122-174929.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-174929.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5713:
    filename: "Disney's Tarzan-201122-174941.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-174941.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5714:
    filename: "Disney's Tarzan-201122-175002.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-175002.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5715:
    filename: "Disney's Tarzan-201122-175020.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-175020.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5716:
    filename: "Disney's Tarzan-201122-175030.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-175030.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5717:
    filename: "Disney's Tarzan-201122-175044.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-175044.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5718:
    filename: "Disney's Tarzan-201122-175057.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-175057.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5719:
    filename: "Disney's Tarzan-201122-175117.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-175117.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5720:
    filename: "Disney's Tarzan-201122-175132.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-175132.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5721:
    filename: "Disney's Tarzan-201122-175142.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-175142.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5722:
    filename: "Disney's Tarzan-201122-175148.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-175148.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5723:
    filename: "Disney's Tarzan-201122-175204.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-175204.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5724:
    filename: "Disney's Tarzan-201122-175224.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-175224.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5725:
    filename: "Disney's Tarzan-201122-175239.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-175239.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5726:
    filename: "Disney's Tarzan-201122-175250.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-175250.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5727:
    filename: "Disney's Tarzan-201122-175314.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-175314.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5728:
    filename: "Disney's Tarzan-201122-175322.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-175322.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5729:
    filename: "Disney's Tarzan-201122-175424.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-175424.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5730:
    filename: "Disney's Tarzan-201122-175501.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-175501.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5731:
    filename: "Disney's Tarzan-201122-175516.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-175516.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5732:
    filename: "Disney's Tarzan-201122-175536.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-175536.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5733:
    filename: "Disney's Tarzan-201122-175603.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-175603.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5734:
    filename: "Disney's Tarzan-201122-175621.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-175621.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5735:
    filename: "Disney's Tarzan-201122-175714.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-175714.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5736:
    filename: "Disney's Tarzan-201122-175807.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-175807.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5737:
    filename: "Disney's Tarzan-201122-175826.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-175826.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5738:
    filename: "Disney's Tarzan-201122-175840.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-175840.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5739:
    filename: "Disney's Tarzan-201122-175849.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-175849.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5740:
    filename: "Disney's Tarzan-201122-175903.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-175903.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5741:
    filename: "Disney's Tarzan-201122-180022.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-180022.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5742:
    filename: "Disney's Tarzan-201122-180057.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-180057.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5743:
    filename: "Disney's Tarzan-201122-180107.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-180107.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5744:
    filename: "Disney's Tarzan-201122-180124.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-180124.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5745:
    filename: "Disney's Tarzan-201122-180219.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-180219.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5746:
    filename: "Disney's Tarzan-201122-180350.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-180350.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5747:
    filename: "Disney's Tarzan-201122-180510.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-180510.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5748:
    filename: "Disney's Tarzan-201122-180533.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-180533.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5749:
    filename: "Disney's Tarzan-201122-180551.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-180551.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5750:
    filename: "Disney's Tarzan-201122-180602.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-180602.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5751:
    filename: "Disney's Tarzan-201122-180616.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-180616.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5752:
    filename: "Disney's Tarzan-201122-180626.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-180626.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5753:
    filename: "Disney's Tarzan-201122-180634.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-180634.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5754:
    filename: "Disney's Tarzan-201122-180646.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-180646.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5755:
    filename: "Disney's Tarzan-201122-180706.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-180706.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5756:
    filename: "Disney's Tarzan-201122-180721.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-180721.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5757:
    filename: "Disney's Tarzan-201122-180735.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-180735.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5758:
    filename: "Disney's Tarzan-201122-180800.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-180800.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5759:
    filename: "Disney's Tarzan-201122-180821.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-180821.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5760:
    filename: "Disney's Tarzan-201122-180833.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-180833.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5761:
    filename: "Disney's Tarzan-201122-180847.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-180847.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5762:
    filename: "Disney's Tarzan-201122-180856.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-180856.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5763:
    filename: "Disney's Tarzan-201122-180918.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-180918.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5764:
    filename: "Disney's Tarzan-201122-180927.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-180927.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
  5765:
    filename: "Disney's Tarzan-201122-181005.png"
    date: "22/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Disney's Tarzan PS1 2020 - Screenshots/Disney's Tarzan-201122-181005.png"
    path: "Disney's Tarzan PS1 2020 - Screenshots"
playlists:
tags:
  - "TonyTonyX042"
categories:
  - "PS1"
updates:
  - "Disney's Tarzan PS1 2020 - Screenshots"
---
{% include 'article.html' %}
