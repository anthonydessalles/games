---
title: "Tony Hawk's American Wasteland"
slug: "tony-hawk-s-american-wasteland"
post_date: "18/10/2005"
files:
playlists:
  1655:
    title: "Tony Hawk's American Wasteland PS2 2022"
    publishedAt: "17/05/2022"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyIV3A80ht5_khWTPFVZFmt5" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS2"
updates:
  - "Tony Hawk's American Wasteland PS2 2022"
---
{% include 'article.html' %}
