---
title: "Syphon Filter"
slug: "syphon-filter"
post_date: "17/02/1999"
files:
  10678:
    filename: "Syphon Filter-201126-171915.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-171915.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10679:
    filename: "Syphon Filter-201126-171922.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-171922.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10680:
    filename: "Syphon Filter-201126-171928.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-171928.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10681:
    filename: "Syphon Filter-201126-171936.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-171936.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10682:
    filename: "Syphon Filter-201126-171944.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-171944.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10683:
    filename: "Syphon Filter-201126-171951.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-171951.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10684:
    filename: "Syphon Filter-201126-171959.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-171959.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10685:
    filename: "Syphon Filter-201126-172012.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172012.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10686:
    filename: "Syphon Filter-201126-172019.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172019.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10687:
    filename: "Syphon Filter-201126-172025.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172025.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10688:
    filename: "Syphon Filter-201126-172033.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172033.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10689:
    filename: "Syphon Filter-201126-172039.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172039.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10690:
    filename: "Syphon Filter-201126-172047.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172047.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10691:
    filename: "Syphon Filter-201126-172056.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172056.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10692:
    filename: "Syphon Filter-201126-172105.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172105.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10693:
    filename: "Syphon Filter-201126-172113.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172113.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10694:
    filename: "Syphon Filter-201126-172119.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172119.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10695:
    filename: "Syphon Filter-201126-172130.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172130.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10696:
    filename: "Syphon Filter-201126-172139.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172139.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10697:
    filename: "Syphon Filter-201126-172146.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172146.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10698:
    filename: "Syphon Filter-201126-172153.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172153.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10699:
    filename: "Syphon Filter-201126-172203.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172203.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10700:
    filename: "Syphon Filter-201126-172211.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172211.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10701:
    filename: "Syphon Filter-201126-172218.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172218.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10702:
    filename: "Syphon Filter-201126-172230.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172230.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10703:
    filename: "Syphon Filter-201126-172238.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172238.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10704:
    filename: "Syphon Filter-201126-172248.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172248.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10705:
    filename: "Syphon Filter-201126-172301.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172301.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10706:
    filename: "Syphon Filter-201126-172307.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172307.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10707:
    filename: "Syphon Filter-201126-172313.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172313.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10708:
    filename: "Syphon Filter-201126-172323.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172323.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10709:
    filename: "Syphon Filter-201126-172339.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172339.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10710:
    filename: "Syphon Filter-201126-172406.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172406.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10711:
    filename: "Syphon Filter-201126-172412.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172412.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10712:
    filename: "Syphon Filter-201126-172425.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172425.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10713:
    filename: "Syphon Filter-201126-172436.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172436.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10714:
    filename: "Syphon Filter-201126-172445.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172445.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10715:
    filename: "Syphon Filter-201126-172454.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172454.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10716:
    filename: "Syphon Filter-201126-172504.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172504.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10717:
    filename: "Syphon Filter-201126-172516.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172516.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10718:
    filename: "Syphon Filter-201126-172527.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172527.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10719:
    filename: "Syphon Filter-201126-172537.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172537.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10720:
    filename: "Syphon Filter-201126-172546.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172546.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10721:
    filename: "Syphon Filter-201126-172555.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172555.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10722:
    filename: "Syphon Filter-201126-172604.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172604.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10723:
    filename: "Syphon Filter-201126-172614.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172614.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10724:
    filename: "Syphon Filter-201126-172624.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172624.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10725:
    filename: "Syphon Filter-201126-172637.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172637.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10726:
    filename: "Syphon Filter-201126-172645.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172645.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10727:
    filename: "Syphon Filter-201126-172656.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172656.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10728:
    filename: "Syphon Filter-201126-172726.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172726.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10729:
    filename: "Syphon Filter-201126-172749.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172749.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10730:
    filename: "Syphon Filter-201126-172803.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172803.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10731:
    filename: "Syphon Filter-201126-172827.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172827.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10732:
    filename: "Syphon Filter-201126-172836.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172836.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10733:
    filename: "Syphon Filter-201126-172843.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172843.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10734:
    filename: "Syphon Filter-201126-172857.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172857.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10735:
    filename: "Syphon Filter-201126-172908.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172908.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10736:
    filename: "Syphon Filter-201126-172920.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172920.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10737:
    filename: "Syphon Filter-201126-172936.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172936.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10738:
    filename: "Syphon Filter-201126-172950.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-172950.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10739:
    filename: "Syphon Filter-201126-173005.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-173005.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10740:
    filename: "Syphon Filter-201126-173015.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-173015.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10741:
    filename: "Syphon Filter-201126-173030.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-173030.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10742:
    filename: "Syphon Filter-201126-173042.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-173042.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10743:
    filename: "Syphon Filter-201126-173140.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-173140.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10744:
    filename: "Syphon Filter-201126-173157.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-173157.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10745:
    filename: "Syphon Filter-201126-173214.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-173214.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
  10746:
    filename: "Syphon Filter-201126-173230.png"
    date: "26/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Syphon Filter PS1 2020 - Screenshots/Syphon Filter-201126-173230.png"
    path: "Syphon Filter PS1 2020 - Screenshots"
playlists:
tags:
  - "TonyTonyX042"
categories:
  - "PS1"
updates:
  - "Syphon Filter PS1 2020 - Screenshots"
---
{% include 'article.html' %}
