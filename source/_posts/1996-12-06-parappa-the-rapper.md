---
title: "Parappa the Rapper"
slug: "parappa-the-rapper"
post_date: "06/12/1996"
files:
  8323:
    filename: "Parappa the Rapper-201125-113749.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-113749.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8324:
    filename: "Parappa the Rapper-201125-113759.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-113759.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8325:
    filename: "Parappa the Rapper-201125-113813.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-113813.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8326:
    filename: "Parappa the Rapper-201125-113821.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-113821.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8327:
    filename: "Parappa the Rapper-201125-113833.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-113833.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8328:
    filename: "Parappa the Rapper-201125-113843.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-113843.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8329:
    filename: "Parappa the Rapper-201125-113850.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-113850.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8330:
    filename: "Parappa the Rapper-201125-113857.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-113857.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8331:
    filename: "Parappa the Rapper-201125-113908.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-113908.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8332:
    filename: "Parappa the Rapper-201125-113916.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-113916.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8333:
    filename: "Parappa the Rapper-201125-113922.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-113922.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8334:
    filename: "Parappa the Rapper-201125-113928.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-113928.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8335:
    filename: "Parappa the Rapper-201125-113933.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-113933.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8336:
    filename: "Parappa the Rapper-201125-113940.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-113940.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8337:
    filename: "Parappa the Rapper-201125-113949.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-113949.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8338:
    filename: "Parappa the Rapper-201125-113957.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-113957.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8339:
    filename: "Parappa the Rapper-201125-114004.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-114004.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8340:
    filename: "Parappa the Rapper-201125-114011.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-114011.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8341:
    filename: "Parappa the Rapper-201125-114018.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-114018.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8342:
    filename: "Parappa the Rapper-201125-114028.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-114028.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8343:
    filename: "Parappa the Rapper-201125-114036.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-114036.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8344:
    filename: "Parappa the Rapper-201125-114043.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-114043.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8345:
    filename: "Parappa the Rapper-201125-114053.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-114053.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8346:
    filename: "Parappa the Rapper-201125-114108.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-114108.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8347:
    filename: "Parappa the Rapper-201125-114114.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-114114.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8348:
    filename: "Parappa the Rapper-201125-114120.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-114120.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8349:
    filename: "Parappa the Rapper-201125-114126.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-114126.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8350:
    filename: "Parappa the Rapper-201125-114130.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-114130.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8351:
    filename: "Parappa the Rapper-201125-114547.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-114547.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8352:
    filename: "Parappa the Rapper-201125-114832.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-114832.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8353:
    filename: "Parappa the Rapper-201125-114841.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-114841.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8354:
    filename: "Parappa the Rapper-201125-114848.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-114848.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8355:
    filename: "Parappa the Rapper-201125-114855.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-114855.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8356:
    filename: "Parappa the Rapper-201125-114909.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-114909.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8357:
    filename: "Parappa the Rapper-201125-114916.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-114916.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8358:
    filename: "Parappa the Rapper-201125-114923.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-114923.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8359:
    filename: "Parappa the Rapper-201125-114942.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-114942.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8360:
    filename: "Parappa the Rapper-201125-114947.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-114947.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8361:
    filename: "Parappa the Rapper-201125-114953.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-114953.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8362:
    filename: "Parappa the Rapper-201125-114958.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-114958.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8363:
    filename: "Parappa the Rapper-201125-115004.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115004.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8364:
    filename: "Parappa the Rapper-201125-115013.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115013.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8365:
    filename: "Parappa the Rapper-201125-115020.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115020.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8366:
    filename: "Parappa the Rapper-201125-115027.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115027.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8367:
    filename: "Parappa the Rapper-201125-115037.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115037.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8368:
    filename: "Parappa the Rapper-201125-115042.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115042.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8369:
    filename: "Parappa the Rapper-201125-115049.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115049.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8370:
    filename: "Parappa the Rapper-201125-115054.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115054.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8371:
    filename: "Parappa the Rapper-201125-115100.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115100.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8372:
    filename: "Parappa the Rapper-201125-115106.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115106.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8373:
    filename: "Parappa the Rapper-201125-115111.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115111.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8374:
    filename: "Parappa the Rapper-201125-115118.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115118.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8375:
    filename: "Parappa the Rapper-201125-115126.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115126.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8376:
    filename: "Parappa the Rapper-201125-115132.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115132.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8377:
    filename: "Parappa the Rapper-201125-115137.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115137.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8378:
    filename: "Parappa the Rapper-201125-115143.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115143.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8379:
    filename: "Parappa the Rapper-201125-115148.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115148.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8380:
    filename: "Parappa the Rapper-201125-115153.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115153.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8381:
    filename: "Parappa the Rapper-201125-115159.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115159.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8382:
    filename: "Parappa the Rapper-201125-115206.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115206.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8383:
    filename: "Parappa the Rapper-201125-115213.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115213.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8384:
    filename: "Parappa the Rapper-201125-115219.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115219.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8385:
    filename: "Parappa the Rapper-201125-115225.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115225.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8386:
    filename: "Parappa the Rapper-201125-115233.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115233.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8387:
    filename: "Parappa the Rapper-201125-115240.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115240.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8388:
    filename: "Parappa the Rapper-201125-115251.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115251.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8389:
    filename: "Parappa the Rapper-201125-115257.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115257.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8390:
    filename: "Parappa the Rapper-201125-115302.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115302.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8391:
    filename: "Parappa the Rapper-201125-115309.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115309.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8392:
    filename: "Parappa the Rapper-201125-115319.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115319.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8393:
    filename: "Parappa the Rapper-201125-115327.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115327.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8394:
    filename: "Parappa the Rapper-201125-115334.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115334.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8395:
    filename: "Parappa the Rapper-201125-115339.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115339.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8396:
    filename: "Parappa the Rapper-201125-115345.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115345.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8397:
    filename: "Parappa the Rapper-201125-115352.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115352.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8398:
    filename: "Parappa the Rapper-201125-115403.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115403.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8399:
    filename: "Parappa the Rapper-201125-115410.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115410.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8400:
    filename: "Parappa the Rapper-201125-115416.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115416.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8401:
    filename: "Parappa the Rapper-201125-115422.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115422.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8402:
    filename: "Parappa the Rapper-201125-115429.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115429.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8403:
    filename: "Parappa the Rapper-201125-115434.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115434.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8404:
    filename: "Parappa the Rapper-201125-115442.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115442.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8405:
    filename: "Parappa the Rapper-201125-115450.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115450.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8406:
    filename: "Parappa the Rapper-201125-115501.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115501.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8407:
    filename: "Parappa the Rapper-201125-115508.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115508.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8408:
    filename: "Parappa the Rapper-201125-115514.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115514.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8409:
    filename: "Parappa the Rapper-201125-115527.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115527.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8410:
    filename: "Parappa the Rapper-201125-115534.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115534.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8411:
    filename: "Parappa the Rapper-201125-115544.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115544.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8412:
    filename: "Parappa the Rapper-201125-115549.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115549.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8413:
    filename: "Parappa the Rapper-201125-115556.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115556.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8414:
    filename: "Parappa the Rapper-201125-115604.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115604.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8415:
    filename: "Parappa the Rapper-201125-115612.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115612.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8416:
    filename: "Parappa the Rapper-201125-115623.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115623.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8417:
    filename: "Parappa the Rapper-201125-115632.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115632.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8418:
    filename: "Parappa the Rapper-201125-115645.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115645.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8419:
    filename: "Parappa the Rapper-201125-115650.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115650.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8420:
    filename: "Parappa the Rapper-201125-115655.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115655.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8421:
    filename: "Parappa the Rapper-201125-115700.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115700.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8422:
    filename: "Parappa the Rapper-201125-115706.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115706.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8423:
    filename: "Parappa the Rapper-201125-115717.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115717.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8424:
    filename: "Parappa the Rapper-201125-115725.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115725.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8425:
    filename: "Parappa the Rapper-201125-115737.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115737.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8426:
    filename: "Parappa the Rapper-201125-115751.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115751.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8427:
    filename: "Parappa the Rapper-201125-115758.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115758.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8428:
    filename: "Parappa the Rapper-201125-115804.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115804.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8429:
    filename: "Parappa the Rapper-201125-115811.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115811.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8430:
    filename: "Parappa the Rapper-201125-115820.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115820.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8431:
    filename: "Parappa the Rapper-201125-115826.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115826.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8432:
    filename: "Parappa the Rapper-201125-115831.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115831.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8433:
    filename: "Parappa the Rapper-201125-115839.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115839.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8434:
    filename: "Parappa the Rapper-201125-115845.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115845.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8435:
    filename: "Parappa the Rapper-201125-115851.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115851.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8436:
    filename: "Parappa the Rapper-201125-115900.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115900.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8437:
    filename: "Parappa the Rapper-201125-115905.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115905.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8438:
    filename: "Parappa the Rapper-201125-115919.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115919.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8439:
    filename: "Parappa the Rapper-201125-115926.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115926.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8440:
    filename: "Parappa the Rapper-201125-115936.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115936.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8441:
    filename: "Parappa the Rapper-201125-115944.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115944.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8442:
    filename: "Parappa the Rapper-201125-115952.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115952.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8443:
    filename: "Parappa the Rapper-201125-115958.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-115958.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8444:
    filename: "Parappa the Rapper-201125-120005.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120005.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8445:
    filename: "Parappa the Rapper-201125-120012.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120012.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8446:
    filename: "Parappa the Rapper-201125-120018.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120018.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8447:
    filename: "Parappa the Rapper-201125-120024.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120024.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8448:
    filename: "Parappa the Rapper-201125-120033.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120033.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8449:
    filename: "Parappa the Rapper-201125-120038.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120038.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8450:
    filename: "Parappa the Rapper-201125-120047.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120047.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8451:
    filename: "Parappa the Rapper-201125-120053.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120053.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8452:
    filename: "Parappa the Rapper-201125-120100.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120100.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8453:
    filename: "Parappa the Rapper-201125-120109.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120109.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8454:
    filename: "Parappa the Rapper-201125-120124.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120124.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8455:
    filename: "Parappa the Rapper-201125-120129.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120129.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8456:
    filename: "Parappa the Rapper-201125-120134.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120134.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8457:
    filename: "Parappa the Rapper-201125-120139.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120139.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8458:
    filename: "Parappa the Rapper-201125-120145.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120145.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8459:
    filename: "Parappa the Rapper-201125-120156.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120156.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8460:
    filename: "Parappa the Rapper-201125-120202.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120202.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8461:
    filename: "Parappa the Rapper-201125-120209.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120209.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8462:
    filename: "Parappa the Rapper-201125-120217.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120217.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8463:
    filename: "Parappa the Rapper-201125-120224.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120224.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8464:
    filename: "Parappa the Rapper-201125-120230.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120230.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8465:
    filename: "Parappa the Rapper-201125-120236.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120236.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8466:
    filename: "Parappa the Rapper-201125-120243.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120243.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8467:
    filename: "Parappa the Rapper-201125-120250.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120250.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8468:
    filename: "Parappa the Rapper-201125-120255.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120255.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8469:
    filename: "Parappa the Rapper-201125-120300.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120300.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8470:
    filename: "Parappa the Rapper-201125-120306.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120306.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8471:
    filename: "Parappa the Rapper-201125-120313.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120313.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8472:
    filename: "Parappa the Rapper-201125-120319.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120319.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8473:
    filename: "Parappa the Rapper-201125-120327.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120327.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8474:
    filename: "Parappa the Rapper-201125-120336.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120336.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8475:
    filename: "Parappa the Rapper-201125-120340.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120340.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8476:
    filename: "Parappa the Rapper-201125-120351.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120351.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8477:
    filename: "Parappa the Rapper-201125-120358.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120358.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8478:
    filename: "Parappa the Rapper-201125-120405.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120405.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8479:
    filename: "Parappa the Rapper-201125-120412.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120412.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8480:
    filename: "Parappa the Rapper-201125-120419.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120419.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8481:
    filename: "Parappa the Rapper-201125-120427.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120427.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8482:
    filename: "Parappa the Rapper-201125-120432.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120432.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8483:
    filename: "Parappa the Rapper-201125-120438.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120438.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8484:
    filename: "Parappa the Rapper-201125-120447.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120447.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8485:
    filename: "Parappa the Rapper-201125-120453.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120453.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8486:
    filename: "Parappa the Rapper-201125-120459.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120459.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8487:
    filename: "Parappa the Rapper-201125-120504.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120504.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8488:
    filename: "Parappa the Rapper-201125-120510.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120510.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8489:
    filename: "Parappa the Rapper-201125-120515.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120515.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8490:
    filename: "Parappa the Rapper-201125-120527.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120527.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8491:
    filename: "Parappa the Rapper-201125-120532.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120532.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8492:
    filename: "Parappa the Rapper-201125-120537.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120537.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8493:
    filename: "Parappa the Rapper-201125-120543.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120543.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
  8494:
    filename: "Parappa the Rapper-201125-120548.png"
    date: "25/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Parappa the Rapper PS1 2020 - Screenshots/Parappa the Rapper-201125-120548.png"
    path: "Parappa the Rapper PS1 2020 - Screenshots"
playlists:
tags:
  - "TonyTonyX042"
categories:
  - "PS1"
updates:
  - "Parappa the Rapper PS1 2020 - Screenshots"
---
{% include 'article.html' %}
