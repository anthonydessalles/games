---
title: "Fighting Force 2"
slug: "fighting-force-2"
post_date: "30/11/1999"
files:
playlists:
  82:
    title: "Fighting Force 2 PS1 2020"
    publishedAt: "12/09/2020"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJ8NeOceDAE2nzaY_GlNm-d" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS1"
updates:
  - "Fighting Force 2 PS1 2020"
---
{% include 'article.html' %}
