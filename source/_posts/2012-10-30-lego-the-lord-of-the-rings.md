---
title: "Lego The Lord of the Rings"
french_title: "Lego Le Seigneur des Anneaux"
slug: "lego-the-lord-of-the-rings"
post_date: "30/10/2012"
files:
playlists:
  2066:
    title: "Lego Le Seigneur des Anneaux XB360 2023"
    publishedAt: "27/06/2023"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyKnBckSvo1SeDG5xpAGVdyP" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "XB360"
updates:
  - "Lego Le Seigneur des Anneaux XB360 2023"
---
{% include 'article.html' %}
