---
title: "Marvel vs Capcom 2 New Age of Heroes"
slug: "marvel-vs-capcom-2-new-age-of-heroes"
post_date: "24/02/2000"
files:
playlists:
  994:
    title: "Marvel vs Capcom 2 New Age of Heroes XB 2020"
    publishedAt: "21/01/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyI0ZWtmcJzHhgmZNKJslvVB" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "XB"
updates:
  - "Marvel vs Capcom 2 New Age of Heroes XB 2020"
---
{% include 'article.html' %}
