---
title: "Le Monde des Bleus 2003"
slug: "le-monde-des-bleus-2003"
post_date: "04/10/2002"
files:
playlists:
  1000:
    title: "Le Monde des Bleus 2003 PS2 2020"
    publishedAt: "21/01/2021"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLsZ2zX19KzSpVYWf_V4yV1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS2"
updates:
  - "Le Monde des Bleus 2003 PS2 2020"
---
{% include 'article.html' %}
