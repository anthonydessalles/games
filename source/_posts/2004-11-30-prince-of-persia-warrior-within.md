---
title: "Prince of Persia Warrior Within"
french_title: "Prince of Persia L'Âme du Guerrier"
slug: "prince-of-persia-warrior-within"
post_date: "30/11/2004"
files:
  20303:
    filename: "Prince of Persia Revelations-211105-113215.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-113215.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20304:
    filename: "Prince of Persia Revelations-211105-113226.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-113226.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20305:
    filename: "Prince of Persia Revelations-211105-113238.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-113238.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20306:
    filename: "Prince of Persia Revelations-211105-113247.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-113247.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20307:
    filename: "Prince of Persia Revelations-211105-113257.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-113257.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20308:
    filename: "Prince of Persia Revelations-211105-113355.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-113355.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20309:
    filename: "Prince of Persia Revelations-211105-113443.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-113443.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20310:
    filename: "Prince of Persia Revelations-211105-113457.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-113457.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20311:
    filename: "Prince of Persia Revelations-211105-113507.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-113507.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20312:
    filename: "Prince of Persia Revelations-211105-113544.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-113544.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20313:
    filename: "Prince of Persia Revelations-211105-113552.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-113552.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20314:
    filename: "Prince of Persia Revelations-211105-113615.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-113615.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20315:
    filename: "Prince of Persia Revelations-211105-113627.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-113627.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20316:
    filename: "Prince of Persia Revelations-211105-113651.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-113651.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20317:
    filename: "Prince of Persia Revelations-211105-113710.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-113710.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20318:
    filename: "Prince of Persia Revelations-211105-113719.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-113719.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20319:
    filename: "Prince of Persia Revelations-211105-113735.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-113735.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20320:
    filename: "Prince of Persia Revelations-211105-113745.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-113745.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20321:
    filename: "Prince of Persia Revelations-211105-113755.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-113755.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20322:
    filename: "Prince of Persia Revelations-211105-113805.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-113805.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20323:
    filename: "Prince of Persia Revelations-211105-114423.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-114423.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20324:
    filename: "Prince of Persia Revelations-211105-114444.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-114444.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20325:
    filename: "Prince of Persia Revelations-211105-114455.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-114455.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20326:
    filename: "Prince of Persia Revelations-211105-114504.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-114504.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20327:
    filename: "Prince of Persia Revelations-211105-114514.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-114514.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20328:
    filename: "Prince of Persia Revelations-211105-114523.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-114523.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20329:
    filename: "Prince of Persia Revelations-211105-114537.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-114537.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20330:
    filename: "Prince of Persia Revelations-211105-114547.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-114547.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20331:
    filename: "Prince of Persia Revelations-211105-114617.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-114617.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20332:
    filename: "Prince of Persia Revelations-211105-114632.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-114632.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20333:
    filename: "Prince of Persia Revelations-211105-114646.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-114646.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20334:
    filename: "Prince of Persia Revelations-211105-114655.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-114655.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20335:
    filename: "Prince of Persia Revelations-211105-114707.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-114707.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
  20336:
    filename: "Prince of Persia Revelations-211105-114719.png"
    date: "05/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Prince of Persia Revelations PSP 2021 - Screenshots/Prince of Persia Revelations-211105-114719.png"
    path: "Prince of Persia Revelations PSP 2021 - Screenshots"
playlists:
  1241:
    title: "Prince of Persia L'Âme du Guerrier PS2 2021"
    publishedAt: "21/09/2021"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLFfoNzOTGE-_tL7_XCzg3V" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PSP"
  - "PS2"
updates:
  - "Prince of Persia Revelations PSP 2021 - Screenshots"
  - "Prince of Persia L'Âme du Guerrier PS2 2021"
---
{% include 'article.html' %}