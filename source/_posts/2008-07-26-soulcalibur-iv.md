---
title: "SoulCalibur IV"
slug: "soulcalibur-iv"
post_date: "26/07/2008"
files:
  68216:
    filename: "Screenshot 2025-02-07 134330.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur IV XB360 2025 - Screenshots/Screenshot 2025-02-07 134330.png"
    path: "SoulCalibur IV XB360 2025 - Screenshots"
  68217:
    filename: "Screenshot 2025-02-07 134422.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur IV XB360 2025 - Screenshots/Screenshot 2025-02-07 134422.png"
    path: "SoulCalibur IV XB360 2025 - Screenshots"
  68218:
    filename: "Screenshot 2025-02-07 134430.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur IV XB360 2025 - Screenshots/Screenshot 2025-02-07 134430.png"
    path: "SoulCalibur IV XB360 2025 - Screenshots"
  68219:
    filename: "Screenshot 2025-02-07 134442.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur IV XB360 2025 - Screenshots/Screenshot 2025-02-07 134442.png"
    path: "SoulCalibur IV XB360 2025 - Screenshots"
  68220:
    filename: "Screenshot 2025-02-07 134454.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur IV XB360 2025 - Screenshots/Screenshot 2025-02-07 134454.png"
    path: "SoulCalibur IV XB360 2025 - Screenshots"
  68221:
    filename: "Screenshot 2025-02-07 134514.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur IV XB360 2025 - Screenshots/Screenshot 2025-02-07 134514.png"
    path: "SoulCalibur IV XB360 2025 - Screenshots"
  68222:
    filename: "Screenshot 2025-02-07 134526.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur IV XB360 2025 - Screenshots/Screenshot 2025-02-07 134526.png"
    path: "SoulCalibur IV XB360 2025 - Screenshots"
  68223:
    filename: "Screenshot 2025-02-07 134557.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur IV XB360 2025 - Screenshots/Screenshot 2025-02-07 134557.png"
    path: "SoulCalibur IV XB360 2025 - Screenshots"
  68224:
    filename: "Screenshot 2025-02-07 134632.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur IV XB360 2025 - Screenshots/Screenshot 2025-02-07 134632.png"
    path: "SoulCalibur IV XB360 2025 - Screenshots"
  68225:
    filename: "Screenshot 2025-02-07 134741.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur IV XB360 2025 - Screenshots/Screenshot 2025-02-07 134741.png"
    path: "SoulCalibur IV XB360 2025 - Screenshots"
  68226:
    filename: "Screenshot 2025-02-07 134749.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur IV XB360 2025 - Screenshots/Screenshot 2025-02-07 134749.png"
    path: "SoulCalibur IV XB360 2025 - Screenshots"
  68227:
    filename: "Screenshot 2025-02-07 134759.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur IV XB360 2025 - Screenshots/Screenshot 2025-02-07 134759.png"
    path: "SoulCalibur IV XB360 2025 - Screenshots"
  68228:
    filename: "Screenshot 2025-02-07 134831.png"
    date: "07/02/2025"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur IV XB360 2025 - Screenshots/Screenshot 2025-02-07 134831.png"
    path: "SoulCalibur IV XB360 2025 - Screenshots"
playlists:
  2041:
    title: "SoulCalibur IV PS3 2023"
    publishedAt: "27/06/2023"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyKnq6XTOYzRCz6deKwa4rlp" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "XB360"
  - "PS3"
updates:
  - "SoulCalibur IV XB360 2025 - Screenshots"
  - "SoulCalibur IV PS3 2023"
---
{% include 'article.html' %}