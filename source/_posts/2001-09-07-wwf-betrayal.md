---
title: "WWF Betrayal"
slug: "wwf-betrayal"
post_date: "07/09/2001"
files:
playlists:
  1828:
    title: "WWF Betrayal GBC 2023"
    publishedAt: "24/01/2023"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyI--Ff6gj6OzkQ-EiByRDvP" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "GBC"
updates:
  - "WWF Betrayal GBC 2023"
---
{% include 'article.html' %}
