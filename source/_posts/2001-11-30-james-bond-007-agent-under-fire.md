---
title: "James Bond 007 Agent Under Fire"
french_title: "007 Espion pour Cible"
slug: "james-bond-007-agent-under-fire"
post_date: "30/11/2001"
files:
playlists:
  920:
    title: "007 Espion pour Cible PS2 2021"
    publishedAt: "26/01/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLVYPzsrXkWmBpZzBgA_9JD" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS2"
updates:
  - "007 Espion pour Cible PS2 2021"
---
{% include 'article.html' %}
