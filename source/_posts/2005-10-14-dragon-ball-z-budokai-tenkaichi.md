---
title: "Dragon Ball Z Budokai Tenkaichi"
slug: "dragon-ball-z-budokai-tenkaichi"
post_date: "14/10/2005"
files:
playlists:
  913:
    title: "Dragon Ball Z Budokai Tenkaichi PS2 2021"
    publishedAt: "26/01/2021"
    itemsCount: "4"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLhhhNncgZc4yApYDsUHgrX" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS2"
updates:
  - "Dragon Ball Z Budokai Tenkaichi PS2 2021"
---
{% include 'article.html' %}
