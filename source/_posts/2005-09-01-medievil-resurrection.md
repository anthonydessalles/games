---
title: "MediEvil Resurrection"
slug: "medievil-resurrection"
post_date: "01/09/2005"
files:
  26108:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00000.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00000.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26109:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00001.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00001.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26110:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00002.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00002.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26111:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00003.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00003.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26112:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00004.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00004.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26113:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00005.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00005.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26114:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00006.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00006.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26115:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00007.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00007.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26116:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00008.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00008.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26117:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00009.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00009.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26118:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00010.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00010.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26119:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00011.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00011.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26120:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00012.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00012.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26121:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00013.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00013.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26122:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00014.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00014.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26123:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00015.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00015.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26124:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00016.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00016.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26125:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00017.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00017.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26126:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00018.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00018.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26127:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00020.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00020.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26128:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00021.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00021.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26129:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00022.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00022.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26130:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00024.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00024.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26131:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00025.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00025.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26132:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00028.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00028.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26133:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00029.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00029.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26134:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00030.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00030.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26135:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00032.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00032.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26136:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00033.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00033.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26137:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00034.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00034.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26138:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00036.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00036.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26139:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00038.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00038.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26140:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00039.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00039.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26141:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00040.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00040.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26142:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00041.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00041.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26143:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00042.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00042.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26144:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00043.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00043.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26145:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00044.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00044.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26146:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00045.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00045.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26147:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00047.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00047.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26148:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00048.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00048.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26149:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00049.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00049.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26150:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00050.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00050.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26151:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00051.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00051.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26152:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00052.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00052.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26153:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00053.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00053.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26154:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00054.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00054.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26155:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00055.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00055.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26156:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00056.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00056.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26157:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00057.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00057.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26158:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00058.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00058.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26159:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00059.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00059.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26160:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00060.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00060.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26161:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00061.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00061.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26162:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00062.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00062.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26163:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00063.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00063.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26164:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00064.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00064.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26165:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00065.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00065.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26166:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00066.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00066.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26167:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00067.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00067.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26168:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00068.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00068.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
  26169:
    filename: "MediEvil Resurrection PSP 20230411 UCUS98620_00069.jpg"
    date: "11/04/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/MediEvil Resurrection PSP 2023 - Screenshots/MediEvil Resurrection PSP 20230411 UCUS98620_00069.jpg"
    path: "MediEvil Resurrection PSP 2023 - Screenshots"
playlists:
tags:
  - "TonyTonyX042"
categories:
  - "PSP"
updates:
  - "MediEvil Resurrection PSP 2023 - Screenshots"
---
{% include 'article.html' %}
