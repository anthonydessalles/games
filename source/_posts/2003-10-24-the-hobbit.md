---
title: "The Hobbit"
french_title: "Bilbo le Hobbit"
slug: "the-hobbit"
post_date: "24/10/2003"
files:
playlists:
  44:
    title: "Bilbo le Hobbit PS2 2020"
    publishedAt: "12/09/2020"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJWh1K5gls8FOz0BnSfx7me" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  1026:
    title: "The Hobbit GBA 2021"
    publishedAt: "15/01/2021"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJNU_G8YCNF3XQcifzsXYCq" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS2"
  - "GBA"
updates:
  - "Bilbo le Hobbit PS2 2020"
  - "The Hobbit GBA 2021"
---
{% include 'article.html' %}
