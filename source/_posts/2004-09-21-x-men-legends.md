---
title: "X-Men Legends"
slug: "x-men-legends"
post_date: "21/09/2004"
files:
playlists:
  925:
    title: "X-Men Legends PS2 2020"
    publishedAt: "25/01/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJuKUdHg4zv2GstkRc8puzG" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS2"
updates:
  - "X-Men Legends PS2 2020"
---
{% include 'article.html' %}
