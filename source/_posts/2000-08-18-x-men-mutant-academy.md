---
title: "X-Men Mutant Academy"
slug: "x-men-mutant-academy"
post_date: "18/08/2000"
files:
  14189:
    filename: "X-Men Mutant Academy-201115-155829.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy GBC 2020 - Screenshots/X-Men Mutant Academy-201115-155829.png"
    path: "X-Men Mutant Academy GBC 2020 - Screenshots"
  14190:
    filename: "X-Men Mutant Academy-201115-155838.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy GBC 2020 - Screenshots/X-Men Mutant Academy-201115-155838.png"
    path: "X-Men Mutant Academy GBC 2020 - Screenshots"
  14191:
    filename: "X-Men Mutant Academy-201115-155843.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy GBC 2020 - Screenshots/X-Men Mutant Academy-201115-155843.png"
    path: "X-Men Mutant Academy GBC 2020 - Screenshots"
  14192:
    filename: "X-Men Mutant Academy-201115-155850.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy GBC 2020 - Screenshots/X-Men Mutant Academy-201115-155850.png"
    path: "X-Men Mutant Academy GBC 2020 - Screenshots"
  14193:
    filename: "X-Men Mutant Academy-201115-155858.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy GBC 2020 - Screenshots/X-Men Mutant Academy-201115-155858.png"
    path: "X-Men Mutant Academy GBC 2020 - Screenshots"
  14194:
    filename: "X-Men Mutant Academy-201115-155907.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy GBC 2020 - Screenshots/X-Men Mutant Academy-201115-155907.png"
    path: "X-Men Mutant Academy GBC 2020 - Screenshots"
  14195:
    filename: "X-Men Mutant Academy-201115-155914.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy GBC 2020 - Screenshots/X-Men Mutant Academy-201115-155914.png"
    path: "X-Men Mutant Academy GBC 2020 - Screenshots"
  14196:
    filename: "X-Men Mutant Academy-201115-155922.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy GBC 2020 - Screenshots/X-Men Mutant Academy-201115-155922.png"
    path: "X-Men Mutant Academy GBC 2020 - Screenshots"
  14197:
    filename: "X-Men Mutant Academy-201115-155928.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy GBC 2020 - Screenshots/X-Men Mutant Academy-201115-155928.png"
    path: "X-Men Mutant Academy GBC 2020 - Screenshots"
  14198:
    filename: "X-Men Mutant Academy-201115-155934.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy GBC 2020 - Screenshots/X-Men Mutant Academy-201115-155934.png"
    path: "X-Men Mutant Academy GBC 2020 - Screenshots"
  14199:
    filename: "X-Men Mutant Academy-201115-155944.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy GBC 2020 - Screenshots/X-Men Mutant Academy-201115-155944.png"
    path: "X-Men Mutant Academy GBC 2020 - Screenshots"
  14200:
    filename: "X-Men Mutant Academy-201115-155955.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy GBC 2020 - Screenshots/X-Men Mutant Academy-201115-155955.png"
    path: "X-Men Mutant Academy GBC 2020 - Screenshots"
  14201:
    filename: "X-Men Mutant Academy-201115-160044.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy GBC 2020 - Screenshots/X-Men Mutant Academy-201115-160044.png"
    path: "X-Men Mutant Academy GBC 2020 - Screenshots"
  14202:
    filename: "X-Men Mutant Academy-201115-160053.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy GBC 2020 - Screenshots/X-Men Mutant Academy-201115-160053.png"
    path: "X-Men Mutant Academy GBC 2020 - Screenshots"
  14203:
    filename: "X-Men Mutant Academy-201115-160119.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy GBC 2020 - Screenshots/X-Men Mutant Academy-201115-160119.png"
    path: "X-Men Mutant Academy GBC 2020 - Screenshots"
  14204:
    filename: "X-Men Mutant Academy-201115-160156.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy GBC 2020 - Screenshots/X-Men Mutant Academy-201115-160156.png"
    path: "X-Men Mutant Academy GBC 2020 - Screenshots"
  14205:
    filename: "X-Men Mutant Academy-201115-160209.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy GBC 2020 - Screenshots/X-Men Mutant Academy-201115-160209.png"
    path: "X-Men Mutant Academy GBC 2020 - Screenshots"
  14206:
    filename: "X-Men Mutant Academy-201115-160243.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy GBC 2020 - Screenshots/X-Men Mutant Academy-201115-160243.png"
    path: "X-Men Mutant Academy GBC 2020 - Screenshots"
  14207:
    filename: "X-Men Mutant Academy-201115-160252.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy GBC 2020 - Screenshots/X-Men Mutant Academy-201115-160252.png"
    path: "X-Men Mutant Academy GBC 2020 - Screenshots"
  14208:
    filename: "X-Men Mutant Academy-201115-160308.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy GBC 2020 - Screenshots/X-Men Mutant Academy-201115-160308.png"
    path: "X-Men Mutant Academy GBC 2020 - Screenshots"
  14209:
    filename: "X-Men Mutant Academy-201115-160316.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy GBC 2020 - Screenshots/X-Men Mutant Academy-201115-160316.png"
    path: "X-Men Mutant Academy GBC 2020 - Screenshots"
  14210:
    filename: "X-Men Mutant Academy-201115-160337.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy GBC 2020 - Screenshots/X-Men Mutant Academy-201115-160337.png"
    path: "X-Men Mutant Academy GBC 2020 - Screenshots"
  14211:
    filename: "X-Men Mutant Academy-201115-160348.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy GBC 2020 - Screenshots/X-Men Mutant Academy-201115-160348.png"
    path: "X-Men Mutant Academy GBC 2020 - Screenshots"
  14212:
    filename: "X-Men Mutant Academy-201115-160355.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy GBC 2020 - Screenshots/X-Men Mutant Academy-201115-160355.png"
    path: "X-Men Mutant Academy GBC 2020 - Screenshots"
  14213:
    filename: "X-Men Mutant Academy-201115-160401.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy GBC 2020 - Screenshots/X-Men Mutant Academy-201115-160401.png"
    path: "X-Men Mutant Academy GBC 2020 - Screenshots"
  14214:
    filename: "X-Men Mutant Academy-201127-185302.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185302.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14215:
    filename: "X-Men Mutant Academy-201127-185327.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185327.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14216:
    filename: "X-Men Mutant Academy-201127-185346.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185346.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14217:
    filename: "X-Men Mutant Academy-201127-185356.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185356.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14218:
    filename: "X-Men Mutant Academy-201127-185404.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185404.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14219:
    filename: "X-Men Mutant Academy-201127-185413.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185413.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14220:
    filename: "X-Men Mutant Academy-201127-185421.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185421.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14221:
    filename: "X-Men Mutant Academy-201127-185431.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185431.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14222:
    filename: "X-Men Mutant Academy-201127-185440.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185440.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14223:
    filename: "X-Men Mutant Academy-201127-185449.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185449.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14224:
    filename: "X-Men Mutant Academy-201127-185457.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185457.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14225:
    filename: "X-Men Mutant Academy-201127-185510.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185510.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14226:
    filename: "X-Men Mutant Academy-201127-185519.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185519.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14227:
    filename: "X-Men Mutant Academy-201127-185529.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185529.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14228:
    filename: "X-Men Mutant Academy-201127-185553.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185553.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14229:
    filename: "X-Men Mutant Academy-201127-185611.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185611.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14230:
    filename: "X-Men Mutant Academy-201127-185620.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185620.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14231:
    filename: "X-Men Mutant Academy-201127-185631.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185631.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14232:
    filename: "X-Men Mutant Academy-201127-185638.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185638.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14233:
    filename: "X-Men Mutant Academy-201127-185649.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185649.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14234:
    filename: "X-Men Mutant Academy-201127-185700.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185700.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14235:
    filename: "X-Men Mutant Academy-201127-185713.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185713.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14236:
    filename: "X-Men Mutant Academy-201127-185719.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185719.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14237:
    filename: "X-Men Mutant Academy-201127-185726.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185726.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14238:
    filename: "X-Men Mutant Academy-201127-185733.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185733.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14239:
    filename: "X-Men Mutant Academy-201127-185741.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185741.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14240:
    filename: "X-Men Mutant Academy-201127-185806.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185806.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14241:
    filename: "X-Men Mutant Academy-201127-185836.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185836.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14242:
    filename: "X-Men Mutant Academy-201127-185902.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185902.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14243:
    filename: "X-Men Mutant Academy-201127-185915.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185915.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14244:
    filename: "X-Men Mutant Academy-201127-185926.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185926.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14245:
    filename: "X-Men Mutant Academy-201127-185936.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185936.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14246:
    filename: "X-Men Mutant Academy-201127-185942.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185942.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14247:
    filename: "X-Men Mutant Academy-201127-185951.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185951.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14248:
    filename: "X-Men Mutant Academy-201127-185958.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-185958.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
  14249:
    filename: "X-Men Mutant Academy-201127-190008.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy PS1 2020 - Screenshots/X-Men Mutant Academy-201127-190008.png"
    path: "X-Men Mutant Academy PS1 2020 - Screenshots"
playlists:
  765:
    title: "X-Men Mutant Academy GBC 2020"
    publishedAt: "08/12/2020"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyKeqe09Wzoq2QrhROIP1ksn" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  2899:
    title: "X-Men Mutant Academy PS1 2025"
    publishedAt: "27/02/2025"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJ4K4OaA5jL1qQi4bULY4hw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "GBC"
  - "PS1"
updates:
  - "X-Men Mutant Academy GBC 2020 - Screenshots"
  - "X-Men Mutant Academy PS1 2020 - Screenshots"
  - "X-Men Mutant Academy GBC 2020"
  - "X-Men Mutant Academy PS1 2025"
---
{% include 'article.html' %}