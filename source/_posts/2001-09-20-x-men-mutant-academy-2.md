---
title: "X-Men Mutant Academy 2"
slug: "x-men-mutant-academy-2"
post_date: "20/09/2001"
files:
  14141:
    filename: "X-Men Mutant Academy 2-201127-190151.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190151.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14142:
    filename: "X-Men Mutant Academy 2-201127-190212.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190212.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14143:
    filename: "X-Men Mutant Academy 2-201127-190220.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190220.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14144:
    filename: "X-Men Mutant Academy 2-201127-190233.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190233.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14145:
    filename: "X-Men Mutant Academy 2-201127-190242.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190242.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14146:
    filename: "X-Men Mutant Academy 2-201127-190257.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190257.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14147:
    filename: "X-Men Mutant Academy 2-201127-190311.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190311.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14148:
    filename: "X-Men Mutant Academy 2-201127-190318.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190318.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14149:
    filename: "X-Men Mutant Academy 2-201127-190330.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190330.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14150:
    filename: "X-Men Mutant Academy 2-201127-190348.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190348.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14151:
    filename: "X-Men Mutant Academy 2-201127-190354.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190354.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14152:
    filename: "X-Men Mutant Academy 2-201127-190405.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190405.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14153:
    filename: "X-Men Mutant Academy 2-201127-190412.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190412.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14154:
    filename: "X-Men Mutant Academy 2-201127-190421.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190421.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14155:
    filename: "X-Men Mutant Academy 2-201127-190427.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190427.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14156:
    filename: "X-Men Mutant Academy 2-201127-190435.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190435.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14157:
    filename: "X-Men Mutant Academy 2-201127-190443.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190443.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14158:
    filename: "X-Men Mutant Academy 2-201127-190451.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190451.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14159:
    filename: "X-Men Mutant Academy 2-201127-190500.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190500.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14160:
    filename: "X-Men Mutant Academy 2-201127-190507.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190507.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14161:
    filename: "X-Men Mutant Academy 2-201127-190518.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190518.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14162:
    filename: "X-Men Mutant Academy 2-201127-190524.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190524.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14163:
    filename: "X-Men Mutant Academy 2-201127-190530.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190530.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14164:
    filename: "X-Men Mutant Academy 2-201127-190541.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190541.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14165:
    filename: "X-Men Mutant Academy 2-201127-190554.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190554.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14166:
    filename: "X-Men Mutant Academy 2-201127-190621.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190621.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14167:
    filename: "X-Men Mutant Academy 2-201127-190629.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190629.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14168:
    filename: "X-Men Mutant Academy 2-201127-190639.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190639.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14169:
    filename: "X-Men Mutant Academy 2-201127-190646.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190646.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14170:
    filename: "X-Men Mutant Academy 2-201127-190702.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190702.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14171:
    filename: "X-Men Mutant Academy 2-201127-190711.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190711.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14172:
    filename: "X-Men Mutant Academy 2-201127-190722.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190722.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14173:
    filename: "X-Men Mutant Academy 2-201127-190730.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190730.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14174:
    filename: "X-Men Mutant Academy 2-201127-190740.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190740.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14175:
    filename: "X-Men Mutant Academy 2-201127-190749.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190749.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14176:
    filename: "X-Men Mutant Academy 2-201127-190811.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190811.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14177:
    filename: "X-Men Mutant Academy 2-201127-190822.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190822.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14178:
    filename: "X-Men Mutant Academy 2-201127-190840.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190840.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14179:
    filename: "X-Men Mutant Academy 2-201127-190845.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190845.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14180:
    filename: "X-Men Mutant Academy 2-201127-190850.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190850.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14181:
    filename: "X-Men Mutant Academy 2-201127-190901.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190901.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14182:
    filename: "X-Men Mutant Academy 2-201127-190909.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190909.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14183:
    filename: "X-Men Mutant Academy 2-201127-190943.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190943.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14184:
    filename: "X-Men Mutant Academy 2-201127-190949.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190949.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14185:
    filename: "X-Men Mutant Academy 2-201127-190958.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-190958.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14186:
    filename: "X-Men Mutant Academy 2-201127-191025.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-191025.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14187:
    filename: "X-Men Mutant Academy 2-201127-191036.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-191036.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  14188:
    filename: "X-Men Mutant Academy 2-201127-191048.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Mutant Academy 2 PS1 2020 - Screenshots/X-Men Mutant Academy 2-201127-191048.png"
    path: "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
playlists:
  2898:
    title: "X-Men Mutant Academy 2 PS1 2025"
    publishedAt: "27/02/2025"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyL3X0F1bOAjGYBLz5iXzw4E" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS1"
updates:
  - "X-Men Mutant Academy 2 PS1 2020 - Screenshots"
  - "X-Men Mutant Academy 2 PS1 2025"
---
{% include 'article.html' %}