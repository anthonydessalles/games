---
title: "Batman The Animated Series"
slug: "batman-the-animated-series"
post_date: "01/11/1993"
files:
  5086:
    filename: "Batman The Animated Series-201205-180410.png"
    date: "05/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Animated Series GB 2020 - Screenshots/Batman The Animated Series-201205-180410.png"
    path: "Batman The Animated Series GB 2020 - Screenshots"
  5087:
    filename: "Batman The Animated Series-201205-182009.png"
    date: "05/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Animated Series GB 2020 - Screenshots/Batman The Animated Series-201205-182009.png"
    path: "Batman The Animated Series GB 2020 - Screenshots"
  5088:
    filename: "Batman The Animated Series-201205-182017.png"
    date: "05/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Animated Series GB 2020 - Screenshots/Batman The Animated Series-201205-182017.png"
    path: "Batman The Animated Series GB 2020 - Screenshots"
  5089:
    filename: "Batman The Animated Series-201205-182026.png"
    date: "05/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Animated Series GB 2020 - Screenshots/Batman The Animated Series-201205-182026.png"
    path: "Batman The Animated Series GB 2020 - Screenshots"
  5090:
    filename: "Batman The Animated Series-201205-182037.png"
    date: "05/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Animated Series GB 2020 - Screenshots/Batman The Animated Series-201205-182037.png"
    path: "Batman The Animated Series GB 2020 - Screenshots"
  5091:
    filename: "Batman The Animated Series-201205-182045.png"
    date: "05/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Animated Series GB 2020 - Screenshots/Batman The Animated Series-201205-182045.png"
    path: "Batman The Animated Series GB 2020 - Screenshots"
  5092:
    filename: "Batman The Animated Series-201205-182055.png"
    date: "05/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Animated Series GB 2020 - Screenshots/Batman The Animated Series-201205-182055.png"
    path: "Batman The Animated Series GB 2020 - Screenshots"
  5093:
    filename: "Batman The Animated Series-201205-182103.png"
    date: "05/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Animated Series GB 2020 - Screenshots/Batman The Animated Series-201205-182103.png"
    path: "Batman The Animated Series GB 2020 - Screenshots"
  5094:
    filename: "Batman The Animated Series-201205-182116.png"
    date: "05/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Animated Series GB 2020 - Screenshots/Batman The Animated Series-201205-182116.png"
    path: "Batman The Animated Series GB 2020 - Screenshots"
  5095:
    filename: "Batman The Animated Series-201205-182124.png"
    date: "05/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Animated Series GB 2020 - Screenshots/Batman The Animated Series-201205-182124.png"
    path: "Batman The Animated Series GB 2020 - Screenshots"
  5096:
    filename: "Batman The Animated Series-201205-182136.png"
    date: "05/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Animated Series GB 2020 - Screenshots/Batman The Animated Series-201205-182136.png"
    path: "Batman The Animated Series GB 2020 - Screenshots"
  5097:
    filename: "Batman The Animated Series-201205-182150.png"
    date: "05/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Animated Series GB 2020 - Screenshots/Batman The Animated Series-201205-182150.png"
    path: "Batman The Animated Series GB 2020 - Screenshots"
  5098:
    filename: "Batman The Animated Series-201205-182158.png"
    date: "05/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Animated Series GB 2020 - Screenshots/Batman The Animated Series-201205-182158.png"
    path: "Batman The Animated Series GB 2020 - Screenshots"
  5099:
    filename: "Batman The Animated Series-201205-182210.png"
    date: "05/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Animated Series GB 2020 - Screenshots/Batman The Animated Series-201205-182210.png"
    path: "Batman The Animated Series GB 2020 - Screenshots"
  5100:
    filename: "Batman The Animated Series-201205-182221.png"
    date: "05/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Batman The Animated Series GB 2020 - Screenshots/Batman The Animated Series-201205-182221.png"
    path: "Batman The Animated Series GB 2020 - Screenshots"
playlists:
  812:
    title: "Batman The Animated Series GB 2020"
    publishedAt: "05/12/2020"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLL6tuNlzAwWPoQHvbvYTIH" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "GB"
updates:
  - "Batman The Animated Series GB 2020 - Screenshots"
  - "Batman The Animated Series GB 2020"
---
{% include 'article.html' %}
