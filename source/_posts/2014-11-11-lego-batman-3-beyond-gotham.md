---
title: "Lego Batman 3 Beyond Gotham"
slug: "lego-batman-3-beyond-gotham"
post_date: "11/11/2014"
files:
  33327:
    filename: "20231010-01-F8E5xwyXQAAl0PL.jpg"
    date: "10/10/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Lego Batman 3 Beyond Gotham PS4 2023 - Screenshots/20231010-01-F8E5xwyXQAAl0PL.jpg"
    path: "Lego Batman 3 Beyond Gotham PS4 2023 - Screenshots"
  33328:
    filename: "20231010-02-F8E5x-rXsAA9iCf.jpg"
    date: "10/10/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Lego Batman 3 Beyond Gotham PS4 2023 - Screenshots/20231010-02-F8E5x-rXsAA9iCf.jpg"
    path: "Lego Batman 3 Beyond Gotham PS4 2023 - Screenshots"
  33329:
    filename: "20231010-03-F8E5yLXWYAA8UYE.jpg"
    date: "10/10/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Lego Batman 3 Beyond Gotham PS4 2023 - Screenshots/20231010-03-F8E5yLXWYAA8UYE.jpg"
    path: "Lego Batman 3 Beyond Gotham PS4 2023 - Screenshots"
playlists:
  2146:
    title: "Lego Batman 3 Beyond Gotham PS4 2023"
    publishedAt: "10/10/2023"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyK9BhNuj4b7-dzBbKMwnmi-" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS4"
updates:
  - "Lego Batman 3 Beyond Gotham PS4 2023 - Screenshots"
  - "Lego Batman 3 Beyond Gotham PS4 2023"
---
{% include 'article.html' %}