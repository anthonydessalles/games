---
title: "Le Monde des Bleus 2002"
slug: "le-monde-des-bleus-2002"
post_date: "26/09/2001"
files:
playlists:
  1001:
    title: "Le Monde des Bleus 2002 PS2 2020"
    publishedAt: "21/01/2021"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLC3dVzo8KeyXDgIPf7KwEI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS2"
updates:
  - "Le Monde des Bleus 2002 PS2 2020"
---
{% include 'article.html' %}
