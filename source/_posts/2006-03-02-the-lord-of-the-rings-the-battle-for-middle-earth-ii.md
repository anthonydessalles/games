---
title: "The Lord of the Rings The Battle for Middle-earth II"
french_title: "Le Seigneur des Anneaux La Bataille pour la Terre du Milieu II"
slug: "the-lord-of-the-rings-the-battle-for-middle-earth-ii"
post_date: "02/03/2006"
files:
playlists:
  31:
    title: "Le Seigneur des Anneaux La Bataille pour la Terre du Milieu II XB360 2020"
    publishedAt: "13/09/2020"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJw4GB2OPO0Rsond6shfceX" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  1338:
    title: "The Lord of the Rings The Battle for Middle-earth II PC 2021"
    publishedAt: "23/11/2021"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyIK7gJFqueqQTGvq_v9DI8L" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "XB360"
  - "PC"
updates:
  - "Le Seigneur des Anneaux La Bataille pour la Terre du Milieu II XB360 2020"
  - "The Lord of the Rings The Battle for Middle-earth II PC 2021"
---
{% include 'article.html' %}
