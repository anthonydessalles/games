---
title: "Fantastic Four"
french_title: "Les 4 Fantastiques"
slug: "fantastic-four"
post_date: "27/06/2005"
files:
playlists:
  998:
    title: "Les 4 Fantastiques PS2 2020"
    publishedAt: "21/01/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyIXVrpMM6Hl3kXsWkUEAwvs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS2"
updates:
  - "Les 4 Fantastiques PS2 2020"
---
{% include 'article.html' %}
