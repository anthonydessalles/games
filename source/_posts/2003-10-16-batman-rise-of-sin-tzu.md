---
title: "Batman Rise of Sin Tzu"
slug: "batman-rise-of-sin-tzu"
post_date: "16/10/2003"
files:
playlists:
  1020:
    title: "Batman Rise of Sin Tzu PS2 2020"
    publishedAt: "20/01/2021"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyK9S7cqtjtJFyK-t_K7KqvM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS2"
updates:
  - "Batman Rise of Sin Tzu PS2 2020"
---
{% include 'article.html' %}
