---
title: "Batman & Robin"
slug: "batman-robin"
post_date: "11/11/1998"
files:
playlists:
  80:
    title: "Batman & Robin PS1 2020"
    publishedAt: "12/09/2020"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyI9fZI5wLxwKw4drsgRlZZV" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS1"
updates:
  - "Batman & Robin PS1 2020"
---
{% include 'article.html' %}
