---
title: "Middle-earth Shadow of War"
slug: "middle-earth-shadow-of-war"
post_date: "10/10/2017"
files:
  37036:
    filename: "Middle-earth™_ Shadow of War™_20240201110716.jpg"
    date: "01/02/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Middle-earth Shadow of War PS4 2024 - Screenshots/Middle-earth™_ Shadow of War™_20240201110716.jpg"
    path: "Middle-earth Shadow of War PS4 2024 - Screenshots"
  37037:
    filename: "Middle-earth™_ Shadow of War™_20240201110730.jpg"
    date: "01/02/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Middle-earth Shadow of War PS4 2024 - Screenshots/Middle-earth™_ Shadow of War™_20240201110730.jpg"
    path: "Middle-earth Shadow of War PS4 2024 - Screenshots"
  37038:
    filename: "Middle-earth™_ Shadow of War™_20240201110744.jpg"
    date: "01/02/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Middle-earth Shadow of War PS4 2024 - Screenshots/Middle-earth™_ Shadow of War™_20240201110744.jpg"
    path: "Middle-earth Shadow of War PS4 2024 - Screenshots"
  37039:
    filename: "Middle-earth™_ Shadow of War™_20240201110757.jpg"
    date: "01/02/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Middle-earth Shadow of War PS4 2024 - Screenshots/Middle-earth™_ Shadow of War™_20240201110757.jpg"
    path: "Middle-earth Shadow of War PS4 2024 - Screenshots"
  37040:
    filename: "Middle-earth™_ Shadow of War™_20240201110808.jpg"
    date: "01/02/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Middle-earth Shadow of War PS4 2024 - Screenshots/Middle-earth™_ Shadow of War™_20240201110808.jpg"
    path: "Middle-earth Shadow of War PS4 2024 - Screenshots"
playlists:
  2485:
    title: "Middle-earth Shadow of War PS4 2024"
    publishedAt: "01/02/2024"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyIhRwC__fPtcPt7yEmH-K5e" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS4"
updates:
  - "Middle-earth Shadow of War PS4 2024 - Screenshots"
  - "Middle-earth Shadow of War PS4 2024"
---
{% include 'article.html' %}