---
title: "Metal Gear Solid"
slug: "metal-gear-solid"
post_date: "03/09/1998"
files:
  7800:
    filename: "Metal Gear Solid Disc 1-201124-173715.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-173715.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7801:
    filename: "Metal Gear Solid Disc 1-201124-173723.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-173723.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7802:
    filename: "Metal Gear Solid Disc 1-201124-173739.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-173739.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7803:
    filename: "Metal Gear Solid Disc 1-201124-173747.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-173747.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7804:
    filename: "Metal Gear Solid Disc 1-201124-173805.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-173805.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7805:
    filename: "Metal Gear Solid Disc 1-201124-173823.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-173823.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7806:
    filename: "Metal Gear Solid Disc 1-201124-173831.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-173831.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7807:
    filename: "Metal Gear Solid Disc 1-201124-173837.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-173837.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7808:
    filename: "Metal Gear Solid Disc 1-201124-173843.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-173843.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7809:
    filename: "Metal Gear Solid Disc 1-201124-173851.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-173851.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7810:
    filename: "Metal Gear Solid Disc 1-201124-173901.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-173901.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7811:
    filename: "Metal Gear Solid Disc 1-201124-173909.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-173909.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7812:
    filename: "Metal Gear Solid Disc 1-201124-173915.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-173915.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7813:
    filename: "Metal Gear Solid Disc 1-201124-173924.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-173924.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7814:
    filename: "Metal Gear Solid Disc 1-201124-173947.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-173947.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7815:
    filename: "Metal Gear Solid Disc 1-201124-173955.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-173955.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7816:
    filename: "Metal Gear Solid Disc 1-201124-174002.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174002.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7817:
    filename: "Metal Gear Solid Disc 1-201124-174011.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174011.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7818:
    filename: "Metal Gear Solid Disc 1-201124-174019.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174019.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7819:
    filename: "Metal Gear Solid Disc 1-201124-174025.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174025.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7820:
    filename: "Metal Gear Solid Disc 1-201124-174032.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174032.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7821:
    filename: "Metal Gear Solid Disc 1-201124-174038.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174038.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7822:
    filename: "Metal Gear Solid Disc 1-201124-174044.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174044.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7823:
    filename: "Metal Gear Solid Disc 1-201124-174050.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174050.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7824:
    filename: "Metal Gear Solid Disc 1-201124-174057.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174057.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7825:
    filename: "Metal Gear Solid Disc 1-201124-174105.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174105.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7826:
    filename: "Metal Gear Solid Disc 1-201124-174118.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174118.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7827:
    filename: "Metal Gear Solid Disc 1-201124-174126.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174126.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7828:
    filename: "Metal Gear Solid Disc 1-201124-174146.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174146.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7829:
    filename: "Metal Gear Solid Disc 1-201124-174153.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174153.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7830:
    filename: "Metal Gear Solid Disc 1-201124-174159.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174159.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7831:
    filename: "Metal Gear Solid Disc 1-201124-174206.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174206.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7832:
    filename: "Metal Gear Solid Disc 1-201124-174214.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174214.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7833:
    filename: "Metal Gear Solid Disc 1-201124-174222.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174222.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7834:
    filename: "Metal Gear Solid Disc 1-201124-174229.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174229.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7835:
    filename: "Metal Gear Solid Disc 1-201124-174239.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174239.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7836:
    filename: "Metal Gear Solid Disc 1-201124-174245.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174245.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7837:
    filename: "Metal Gear Solid Disc 1-201124-174253.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174253.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7838:
    filename: "Metal Gear Solid Disc 1-201124-174302.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174302.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7839:
    filename: "Metal Gear Solid Disc 1-201124-174313.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174313.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7840:
    filename: "Metal Gear Solid Disc 1-201124-174319.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174319.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7841:
    filename: "Metal Gear Solid Disc 1-201124-174327.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174327.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7842:
    filename: "Metal Gear Solid Disc 1-201124-174334.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174334.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7843:
    filename: "Metal Gear Solid Disc 1-201124-174339.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174339.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7844:
    filename: "Metal Gear Solid Disc 1-201124-174347.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174347.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7845:
    filename: "Metal Gear Solid Disc 1-201124-174354.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174354.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7846:
    filename: "Metal Gear Solid Disc 1-201124-174401.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174401.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7847:
    filename: "Metal Gear Solid Disc 1-201124-174409.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174409.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7848:
    filename: "Metal Gear Solid Disc 1-201124-174424.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174424.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7849:
    filename: "Metal Gear Solid Disc 1-201124-174444.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174444.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7850:
    filename: "Metal Gear Solid Disc 1-201124-174451.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174451.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7851:
    filename: "Metal Gear Solid Disc 1-201124-174500.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174500.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7852:
    filename: "Metal Gear Solid Disc 1-201124-174516.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174516.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7853:
    filename: "Metal Gear Solid Disc 1-201124-174521.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174521.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7854:
    filename: "Metal Gear Solid Disc 1-201124-174529.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174529.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7855:
    filename: "Metal Gear Solid Disc 1-201124-174538.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174538.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7856:
    filename: "Metal Gear Solid Disc 1-201124-174545.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174545.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7857:
    filename: "Metal Gear Solid Disc 1-201124-174552.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174552.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7858:
    filename: "Metal Gear Solid Disc 1-201124-174601.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174601.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7859:
    filename: "Metal Gear Solid Disc 1-201124-174608.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174608.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7860:
    filename: "Metal Gear Solid Disc 1-201124-174615.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174615.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7861:
    filename: "Metal Gear Solid Disc 1-201124-174628.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174628.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7862:
    filename: "Metal Gear Solid Disc 1-201124-174635.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174635.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7863:
    filename: "Metal Gear Solid Disc 1-201124-174643.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174643.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7864:
    filename: "Metal Gear Solid Disc 1-201124-174650.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174650.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7865:
    filename: "Metal Gear Solid Disc 1-201124-174708.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174708.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7866:
    filename: "Metal Gear Solid Disc 1-201124-174721.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174721.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7867:
    filename: "Metal Gear Solid Disc 1-201124-174731.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174731.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7868:
    filename: "Metal Gear Solid Disc 1-201124-174738.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174738.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7869:
    filename: "Metal Gear Solid Disc 1-201124-174746.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174746.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7870:
    filename: "Metal Gear Solid Disc 1-201124-174751.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174751.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7871:
    filename: "Metal Gear Solid Disc 1-201124-174805.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174805.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7872:
    filename: "Metal Gear Solid Disc 1-201124-174810.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174810.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7873:
    filename: "Metal Gear Solid Disc 1-201124-174814.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174814.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7874:
    filename: "Metal Gear Solid Disc 1-201124-174819.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174819.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7875:
    filename: "Metal Gear Solid Disc 1-201124-174823.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174823.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7876:
    filename: "Metal Gear Solid Disc 1-201124-174829.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174829.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7877:
    filename: "Metal Gear Solid Disc 1-201124-174834.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174834.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7878:
    filename: "Metal Gear Solid Disc 1-201124-174839.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174839.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7879:
    filename: "Metal Gear Solid Disc 1-201124-174845.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174845.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7880:
    filename: "Metal Gear Solid Disc 1-201124-174851.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174851.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7881:
    filename: "Metal Gear Solid Disc 1-201124-174858.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174858.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7882:
    filename: "Metal Gear Solid Disc 1-201124-174905.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174905.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7883:
    filename: "Metal Gear Solid Disc 1-201124-174912.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174912.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7884:
    filename: "Metal Gear Solid Disc 1-201124-174918.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174918.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7885:
    filename: "Metal Gear Solid Disc 1-201124-174925.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174925.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7886:
    filename: "Metal Gear Solid Disc 1-201124-174930.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174930.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7887:
    filename: "Metal Gear Solid Disc 1-201124-174938.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174938.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7888:
    filename: "Metal Gear Solid Disc 1-201124-174946.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-174946.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7889:
    filename: "Metal Gear Solid Disc 1-201124-175004.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175004.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7890:
    filename: "Metal Gear Solid Disc 1-201124-175024.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175024.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7891:
    filename: "Metal Gear Solid Disc 1-201124-175032.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175032.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7892:
    filename: "Metal Gear Solid Disc 1-201124-175048.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175048.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7893:
    filename: "Metal Gear Solid Disc 1-201124-175058.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175058.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7894:
    filename: "Metal Gear Solid Disc 1-201124-175252.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175252.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7895:
    filename: "Metal Gear Solid Disc 1-201124-175303.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175303.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7896:
    filename: "Metal Gear Solid Disc 1-201124-175317.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175317.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7897:
    filename: "Metal Gear Solid Disc 1-201124-175325.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175325.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7898:
    filename: "Metal Gear Solid Disc 1-201124-175341.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175341.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7899:
    filename: "Metal Gear Solid Disc 1-201124-175348.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175348.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7900:
    filename: "Metal Gear Solid Disc 1-201124-175354.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175354.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7901:
    filename: "Metal Gear Solid Disc 1-201124-175403.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175403.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7902:
    filename: "Metal Gear Solid Disc 1-201124-175410.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175410.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7903:
    filename: "Metal Gear Solid Disc 1-201124-175430.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175430.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7904:
    filename: "Metal Gear Solid Disc 1-201124-175458.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175458.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7905:
    filename: "Metal Gear Solid Disc 1-201124-175515.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175515.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7906:
    filename: "Metal Gear Solid Disc 1-201124-175541.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175541.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7907:
    filename: "Metal Gear Solid Disc 1-201124-175607.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175607.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7908:
    filename: "Metal Gear Solid Disc 1-201124-175628.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175628.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7909:
    filename: "Metal Gear Solid Disc 1-201124-175637.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175637.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7910:
    filename: "Metal Gear Solid Disc 1-201124-175656.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175656.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7911:
    filename: "Metal Gear Solid Disc 1-201124-175724.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175724.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7912:
    filename: "Metal Gear Solid Disc 1-201124-175744.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175744.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7913:
    filename: "Metal Gear Solid Disc 1-201124-175818.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175818.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7914:
    filename: "Metal Gear Solid Disc 1-201124-175836.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175836.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7915:
    filename: "Metal Gear Solid Disc 1-201124-175849.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175849.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7916:
    filename: "Metal Gear Solid Disc 1-201124-175926.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175926.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7917:
    filename: "Metal Gear Solid Disc 1-201124-175937.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-175937.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7918:
    filename: "Metal Gear Solid Disc 1-201124-180031.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180031.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7919:
    filename: "Metal Gear Solid Disc 1-201124-180147.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180147.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7920:
    filename: "Metal Gear Solid Disc 1-201124-180305.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180305.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7921:
    filename: "Metal Gear Solid Disc 1-201124-180341.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180341.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7922:
    filename: "Metal Gear Solid Disc 1-201124-180359.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180359.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7923:
    filename: "Metal Gear Solid Disc 1-201124-180409.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180409.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7924:
    filename: "Metal Gear Solid Disc 1-201124-180417.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180417.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7925:
    filename: "Metal Gear Solid Disc 1-201124-180426.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180426.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7926:
    filename: "Metal Gear Solid Disc 1-201124-180432.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180432.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7927:
    filename: "Metal Gear Solid Disc 1-201124-180438.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180438.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7928:
    filename: "Metal Gear Solid Disc 1-201124-180447.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180447.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7929:
    filename: "Metal Gear Solid Disc 1-201124-180455.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180455.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7930:
    filename: "Metal Gear Solid Disc 1-201124-180504.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180504.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7931:
    filename: "Metal Gear Solid Disc 1-201124-180513.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180513.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7932:
    filename: "Metal Gear Solid Disc 1-201124-180528.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180528.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7933:
    filename: "Metal Gear Solid Disc 1-201124-180542.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180542.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7934:
    filename: "Metal Gear Solid Disc 1-201124-180549.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180549.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7935:
    filename: "Metal Gear Solid Disc 1-201124-180605.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180605.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7936:
    filename: "Metal Gear Solid Disc 1-201124-180616.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180616.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7937:
    filename: "Metal Gear Solid Disc 1-201124-180621.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180621.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7938:
    filename: "Metal Gear Solid Disc 1-201124-180626.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180626.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7939:
    filename: "Metal Gear Solid Disc 1-201124-180630.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180630.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7940:
    filename: "Metal Gear Solid Disc 1-201124-180637.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180637.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7941:
    filename: "Metal Gear Solid Disc 1-201124-180641.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180641.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7942:
    filename: "Metal Gear Solid Disc 1-201124-180646.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180646.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7943:
    filename: "Metal Gear Solid Disc 1-201124-180653.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180653.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7944:
    filename: "Metal Gear Solid Disc 1-201124-180657.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180657.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7945:
    filename: "Metal Gear Solid Disc 1-201124-180702.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180702.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7946:
    filename: "Metal Gear Solid Disc 1-201124-180709.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180709.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7947:
    filename: "Metal Gear Solid Disc 1-201124-180726.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180726.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7948:
    filename: "Metal Gear Solid Disc 1-201124-180732.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180732.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7949:
    filename: "Metal Gear Solid Disc 1-201124-180739.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180739.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7950:
    filename: "Metal Gear Solid Disc 1-201124-180745.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180745.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7951:
    filename: "Metal Gear Solid Disc 1-201124-180751.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180751.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7952:
    filename: "Metal Gear Solid Disc 1-201124-180801.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180801.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7953:
    filename: "Metal Gear Solid Disc 1-201124-180808.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180808.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7954:
    filename: "Metal Gear Solid Disc 1-201124-180814.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180814.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7955:
    filename: "Metal Gear Solid Disc 1-201124-180820.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180820.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7956:
    filename: "Metal Gear Solid Disc 1-201124-180826.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180826.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7957:
    filename: "Metal Gear Solid Disc 1-201124-180832.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180832.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7958:
    filename: "Metal Gear Solid Disc 1-201124-180838.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180838.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7959:
    filename: "Metal Gear Solid Disc 1-201124-180845.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180845.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7960:
    filename: "Metal Gear Solid Disc 1-201124-180854.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180854.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7961:
    filename: "Metal Gear Solid Disc 1-201124-180901.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180901.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7962:
    filename: "Metal Gear Solid Disc 1-201124-180909.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180909.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7963:
    filename: "Metal Gear Solid Disc 1-201124-180920.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180920.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7964:
    filename: "Metal Gear Solid Disc 1-201124-180943.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180943.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7965:
    filename: "Metal Gear Solid Disc 1-201124-180948.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180948.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7966:
    filename: "Metal Gear Solid Disc 1-201124-180953.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180953.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7967:
    filename: "Metal Gear Solid Disc 1-201124-180959.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-180959.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7968:
    filename: "Metal Gear Solid Disc 1-201124-181003.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181003.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7969:
    filename: "Metal Gear Solid Disc 1-201124-181010.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181010.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7970:
    filename: "Metal Gear Solid Disc 1-201124-181018.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181018.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7971:
    filename: "Metal Gear Solid Disc 1-201124-181025.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181025.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7972:
    filename: "Metal Gear Solid Disc 1-201124-181031.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181031.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7973:
    filename: "Metal Gear Solid Disc 1-201124-181036.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181036.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7974:
    filename: "Metal Gear Solid Disc 1-201124-181042.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181042.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7975:
    filename: "Metal Gear Solid Disc 1-201124-181047.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181047.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7976:
    filename: "Metal Gear Solid Disc 1-201124-181050.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181050.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7977:
    filename: "Metal Gear Solid Disc 1-201124-181055.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181055.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7978:
    filename: "Metal Gear Solid Disc 1-201124-181102.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181102.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7979:
    filename: "Metal Gear Solid Disc 1-201124-181106.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181106.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7980:
    filename: "Metal Gear Solid Disc 1-201124-181111.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181111.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7981:
    filename: "Metal Gear Solid Disc 1-201124-181116.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181116.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7982:
    filename: "Metal Gear Solid Disc 1-201124-181122.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181122.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7983:
    filename: "Metal Gear Solid Disc 1-201124-181127.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181127.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7984:
    filename: "Metal Gear Solid Disc 1-201124-181133.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181133.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7985:
    filename: "Metal Gear Solid Disc 1-201124-181140.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181140.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7986:
    filename: "Metal Gear Solid Disc 1-201124-181145.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181145.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7987:
    filename: "Metal Gear Solid Disc 1-201124-181150.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181150.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7988:
    filename: "Metal Gear Solid Disc 1-201124-181155.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181155.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7989:
    filename: "Metal Gear Solid Disc 1-201124-181201.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181201.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7990:
    filename: "Metal Gear Solid Disc 1-201124-181209.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181209.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7991:
    filename: "Metal Gear Solid Disc 1-201124-181216.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181216.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7992:
    filename: "Metal Gear Solid Disc 1-201124-181223.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181223.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7993:
    filename: "Metal Gear Solid Disc 1-201124-181231.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181231.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7994:
    filename: "Metal Gear Solid Disc 1-201124-181237.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181237.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7995:
    filename: "Metal Gear Solid Disc 1-201124-181245.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181245.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7996:
    filename: "Metal Gear Solid Disc 1-201124-181250.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181250.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7997:
    filename: "Metal Gear Solid Disc 1-201124-181258.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181258.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7998:
    filename: "Metal Gear Solid Disc 1-201124-181307.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181307.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  7999:
    filename: "Metal Gear Solid Disc 1-201124-181314.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181314.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8000:
    filename: "Metal Gear Solid Disc 1-201124-181321.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181321.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8001:
    filename: "Metal Gear Solid Disc 1-201124-181328.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181328.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8002:
    filename: "Metal Gear Solid Disc 1-201124-181336.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181336.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8003:
    filename: "Metal Gear Solid Disc 1-201124-181346.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181346.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8004:
    filename: "Metal Gear Solid Disc 1-201124-181355.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181355.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8005:
    filename: "Metal Gear Solid Disc 1-201124-181401.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181401.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8006:
    filename: "Metal Gear Solid Disc 1-201124-181407.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181407.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8007:
    filename: "Metal Gear Solid Disc 1-201124-181415.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181415.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8008:
    filename: "Metal Gear Solid Disc 1-201124-181420.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181420.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8009:
    filename: "Metal Gear Solid Disc 1-201124-181426.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181426.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8010:
    filename: "Metal Gear Solid Disc 1-201124-181432.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181432.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8011:
    filename: "Metal Gear Solid Disc 1-201124-181435.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181435.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8012:
    filename: "Metal Gear Solid Disc 1-201124-181442.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181442.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8013:
    filename: "Metal Gear Solid Disc 1-201124-181447.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181447.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8014:
    filename: "Metal Gear Solid Disc 1-201124-181453.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181453.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8015:
    filename: "Metal Gear Solid Disc 1-201124-181500.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181500.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8016:
    filename: "Metal Gear Solid Disc 1-201124-181509.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181509.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8017:
    filename: "Metal Gear Solid Disc 1-201124-181515.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181515.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8018:
    filename: "Metal Gear Solid Disc 1-201124-181520.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181520.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8019:
    filename: "Metal Gear Solid Disc 1-201124-181527.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181527.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8020:
    filename: "Metal Gear Solid Disc 1-201124-181531.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181531.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8021:
    filename: "Metal Gear Solid Disc 1-201124-181536.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181536.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8022:
    filename: "Metal Gear Solid Disc 1-201124-181543.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181543.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8023:
    filename: "Metal Gear Solid Disc 1-201124-181548.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181548.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8024:
    filename: "Metal Gear Solid Disc 1-201124-181554.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181554.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8025:
    filename: "Metal Gear Solid Disc 1-201124-181559.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181559.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8026:
    filename: "Metal Gear Solid Disc 1-201124-181603.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181603.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8027:
    filename: "Metal Gear Solid Disc 1-201124-181609.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181609.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8028:
    filename: "Metal Gear Solid Disc 1-201124-181615.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181615.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8029:
    filename: "Metal Gear Solid Disc 1-201124-181621.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181621.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8030:
    filename: "Metal Gear Solid Disc 1-201124-181630.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181630.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8031:
    filename: "Metal Gear Solid Disc 1-201124-181637.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181637.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8032:
    filename: "Metal Gear Solid Disc 1-201124-181645.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181645.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8033:
    filename: "Metal Gear Solid Disc 1-201124-181651.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181651.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8034:
    filename: "Metal Gear Solid Disc 1-201124-181658.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181658.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8035:
    filename: "Metal Gear Solid Disc 1-201124-181706.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181706.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8036:
    filename: "Metal Gear Solid Disc 1-201124-181712.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181712.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8037:
    filename: "Metal Gear Solid Disc 1-201124-181718.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181718.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8038:
    filename: "Metal Gear Solid Disc 1-201124-181727.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181727.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8039:
    filename: "Metal Gear Solid Disc 1-201124-181735.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181735.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8040:
    filename: "Metal Gear Solid Disc 1-201124-181742.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181742.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8041:
    filename: "Metal Gear Solid Disc 1-201124-181750.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181750.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8042:
    filename: "Metal Gear Solid Disc 1-201124-181803.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181803.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8043:
    filename: "Metal Gear Solid Disc 1-201124-181812.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181812.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8044:
    filename: "Metal Gear Solid Disc 1-201124-181818.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181818.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8045:
    filename: "Metal Gear Solid Disc 1-201124-181824.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181824.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8046:
    filename: "Metal Gear Solid Disc 1-201124-181836.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181836.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8047:
    filename: "Metal Gear Solid Disc 1-201124-181852.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181852.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8048:
    filename: "Metal Gear Solid Disc 1-201124-181910.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181910.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8049:
    filename: "Metal Gear Solid Disc 1-201124-181917.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181917.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8050:
    filename: "Metal Gear Solid Disc 1-201124-181921.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181921.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8051:
    filename: "Metal Gear Solid Disc 1-201124-181927.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181927.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8052:
    filename: "Metal Gear Solid Disc 1-201124-181931.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181931.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8053:
    filename: "Metal Gear Solid Disc 1-201124-181936.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181936.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8054:
    filename: "Metal Gear Solid Disc 1-201124-181941.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181941.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8055:
    filename: "Metal Gear Solid Disc 1-201124-181949.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181949.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8056:
    filename: "Metal Gear Solid Disc 1-201124-181958.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-181958.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8057:
    filename: "Metal Gear Solid Disc 1-201124-182005.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-182005.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8058:
    filename: "Metal Gear Solid Disc 1-201124-182014.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-182014.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8059:
    filename: "Metal Gear Solid Disc 1-201124-182024.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-182024.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8060:
    filename: "Metal Gear Solid Disc 1-201124-182031.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-182031.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
  8061:
    filename: "Metal Gear Solid Disc 1-201124-182043.png"
    date: "24/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Metal Gear Solid PS1 2020 - Screenshots/Metal Gear Solid Disc 1-201124-182043.png"
    path: "Metal Gear Solid PS1 2020 - Screenshots"
playlists:
tags:
  - "TonyTonyX042"
categories:
  - "PS1"
updates:
  - "Metal Gear Solid PS1 2020 - Screenshots"
---
{% include 'article.html' %}
