---
title: "The Lord of the Rings The Third Age"
french_title: "Le Seigneur des Anneaux Le Tiers-Âge"
slug: "the-lord-of-the-rings-the-third-age"
post_date: "02/11/2004"
files:
  3136:
    filename: "2020_9_2_12_30_43.bmp"
    date: "02/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots/2020_9_2_12_30_43.bmp"
    path: "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  3137:
    filename: "2020_9_2_12_31_21.bmp"
    date: "02/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots/2020_9_2_12_31_21.bmp"
    path: "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  3138:
    filename: "2020_9_2_12_31_34.bmp"
    date: "02/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots/2020_9_2_12_31_34.bmp"
    path: "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  3139:
    filename: "2020_9_2_12_31_51.bmp"
    date: "02/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots/2020_9_2_12_31_51.bmp"
    path: "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  3140:
    filename: "2020_9_2_12_31_59.bmp"
    date: "02/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots/2020_9_2_12_31_59.bmp"
    path: "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  3141:
    filename: "2020_9_2_12_32_11.bmp"
    date: "02/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots/2020_9_2_12_32_11.bmp"
    path: "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  3142:
    filename: "2020_9_2_12_32_21.bmp"
    date: "02/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots/2020_9_2_12_32_21.bmp"
    path: "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  3143:
    filename: "2020_9_2_12_32_25.bmp"
    date: "02/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots/2020_9_2_12_32_25.bmp"
    path: "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  3144:
    filename: "2020_9_2_12_32_37.bmp"
    date: "02/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots/2020_9_2_12_32_37.bmp"
    path: "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  3145:
    filename: "2020_9_2_12_32_48.bmp"
    date: "02/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots/2020_9_2_12_32_48.bmp"
    path: "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  3146:
    filename: "2020_9_2_12_32_8.bmp"
    date: "02/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots/2020_9_2_12_32_8.bmp"
    path: "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  3147:
    filename: "2020_9_2_12_33_24.bmp"
    date: "02/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots/2020_9_2_12_33_24.bmp"
    path: "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  3148:
    filename: "2020_9_2_12_33_31.bmp"
    date: "02/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots/2020_9_2_12_33_31.bmp"
    path: "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  3149:
    filename: "2020_9_2_12_33_58.bmp"
    date: "02/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots/2020_9_2_12_33_58.bmp"
    path: "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  3150:
    filename: "2020_9_2_12_33_6.bmp"
    date: "02/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots/2020_9_2_12_33_6.bmp"
    path: "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  3151:
    filename: "2020_9_2_12_34_14.bmp"
    date: "02/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots/2020_9_2_12_34_14.bmp"
    path: "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  3152:
    filename: "2020_9_2_12_34_21.bmp"
    date: "02/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots/2020_9_2_12_34_21.bmp"
    path: "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  3153:
    filename: "2020_9_2_12_34_24.bmp"
    date: "02/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots/2020_9_2_12_34_24.bmp"
    path: "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  3154:
    filename: "2020_9_2_12_34_32.bmp"
    date: "02/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots/2020_9_2_12_34_32.bmp"
    path: "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  3155:
    filename: "2020_9_2_12_34_39.bmp"
    date: "02/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots/2020_9_2_12_34_39.bmp"
    path: "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  3156:
    filename: "2020_9_2_12_34_44.bmp"
    date: "02/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots/2020_9_2_12_34_44.bmp"
    path: "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  3157:
    filename: "2020_9_2_12_34_6.bmp"
    date: "02/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots/2020_9_2_12_34_6.bmp"
    path: "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  3158:
    filename: "2020_9_2_12_35_17.bmp"
    date: "02/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots/2020_9_2_12_35_17.bmp"
    path: "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  3159:
    filename: "2020_9_2_12_35_19.bmp"
    date: "02/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots/2020_9_2_12_35_19.bmp"
    path: "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  3160:
    filename: "2020_9_2_12_35_25.bmp"
    date: "02/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots/2020_9_2_12_35_25.bmp"
    path: "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  3161:
    filename: "2020_9_2_12_35_35.bmp"
    date: "02/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots/2020_9_2_12_35_35.bmp"
    path: "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  3162:
    filename: "2020_9_2_12_36_2.bmp"
    date: "02/09/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots/2020_9_2_12_36_2.bmp"
    path: "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  11399:
    filename: "The Lord of the Rings The Third Age-201115-115955.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-115955.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11400:
    filename: "The Lord of the Rings The Third Age-201115-120001.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120001.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11401:
    filename: "The Lord of the Rings The Third Age-201115-120007.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120007.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11402:
    filename: "The Lord of the Rings The Third Age-201115-120013.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120013.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11403:
    filename: "The Lord of the Rings The Third Age-201115-120023.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120023.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11404:
    filename: "The Lord of the Rings The Third Age-201115-120030.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120030.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11405:
    filename: "The Lord of the Rings The Third Age-201115-120036.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120036.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11406:
    filename: "The Lord of the Rings The Third Age-201115-120043.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120043.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11407:
    filename: "The Lord of the Rings The Third Age-201115-120100.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120100.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11408:
    filename: "The Lord of the Rings The Third Age-201115-120105.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120105.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11409:
    filename: "The Lord of the Rings The Third Age-201115-120116.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120116.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11410:
    filename: "The Lord of the Rings The Third Age-201115-120124.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120124.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11411:
    filename: "The Lord of the Rings The Third Age-201115-120132.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120132.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11412:
    filename: "The Lord of the Rings The Third Age-201115-120148.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120148.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11413:
    filename: "The Lord of the Rings The Third Age-201115-120202.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120202.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11414:
    filename: "The Lord of the Rings The Third Age-201115-120213.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120213.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11415:
    filename: "The Lord of the Rings The Third Age-201115-120225.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120225.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11416:
    filename: "The Lord of the Rings The Third Age-201115-120237.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120237.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11417:
    filename: "The Lord of the Rings The Third Age-201115-120252.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120252.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11418:
    filename: "The Lord of the Rings The Third Age-201115-120300.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120300.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11419:
    filename: "The Lord of the Rings The Third Age-201115-120323.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120323.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11420:
    filename: "The Lord of the Rings The Third Age-201115-120334.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120334.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11421:
    filename: "The Lord of the Rings The Third Age-201115-120340.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120340.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11422:
    filename: "The Lord of the Rings The Third Age-201115-120347.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120347.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11423:
    filename: "The Lord of the Rings The Third Age-201115-120356.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120356.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11424:
    filename: "The Lord of the Rings The Third Age-201115-120404.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120404.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11425:
    filename: "The Lord of the Rings The Third Age-201115-120413.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120413.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11426:
    filename: "The Lord of the Rings The Third Age-201115-120430.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120430.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11427:
    filename: "The Lord of the Rings The Third Age-201115-120439.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120439.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11428:
    filename: "The Lord of the Rings The Third Age-201115-120447.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120447.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11429:
    filename: "The Lord of the Rings The Third Age-201115-120455.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120455.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11430:
    filename: "The Lord of the Rings The Third Age-201115-120515.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120515.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11431:
    filename: "The Lord of the Rings The Third Age-201115-120524.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120524.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11432:
    filename: "The Lord of the Rings The Third Age-201115-120536.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120536.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  11433:
    filename: "The Lord of the Rings The Third Age-201115-120543.png"
    date: "15/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/The Lord of the Rings The Third Age GBA 2020 - Screenshots/The Lord of the Rings The Third Age-201115-120543.png"
    path: "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
playlists:
  2733:
    title: "Le Seigneur des Anneaux Le Tiers Âge GC 2024"
    publishedAt: "25/09/2024"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLnONUUd6ZMuv1WC1BvGOHe" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
  735:
    title: "The Lord of the Rings The Third Age GBA 2020"
    publishedAt: "08/12/2020"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJbTmsM2RcvEvPVwKL4J-mO" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "GC"
  - "GBA"
updates:
  - "Le Seigneur des Anneaux Le Tiers-Âge GC 2020 - Screenshots"
  - "The Lord of the Rings The Third Age GBA 2020 - Screenshots"
  - "Le Seigneur des Anneaux Le Tiers Âge GC 2024"
  - "The Lord of the Rings The Third Age GBA 2020"
---
{% include 'article.html' %}