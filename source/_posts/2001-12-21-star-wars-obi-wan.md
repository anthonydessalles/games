---
title: "Star Wars Obi-Wan"
slug: "star-wars-obi-wan"
post_date: "21/12/2001"
files:
playlists:
  976:
    title: "Star Wars Obi-Wan XB 2020"
    publishedAt: "25/01/2021"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJ50Y9m1ZtHgFj4Iqkuy1s2" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "XB"
updates:
  - "Star Wars Obi-Wan XB 2020"
---
{% include 'article.html' %}
