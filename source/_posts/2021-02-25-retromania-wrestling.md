---
title: "RetroMania Wrestling"
slug: "retromania-wrestling"
post_date: "25/02/2021"
files:
  34996:
    filename: "20231124122801_1.jpg"
    date: "24/11/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/RetroMania Wrestling Steam 2023 - Screenshots/20231124122801_1.jpg"
    path: "RetroMania Wrestling Steam 2023 - Screenshots"
  34997:
    filename: "20231124122818_1.jpg"
    date: "24/11/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/RetroMania Wrestling Steam 2023 - Screenshots/20231124122818_1.jpg"
    path: "RetroMania Wrestling Steam 2023 - Screenshots"
  34998:
    filename: "20231124122845_1.jpg"
    date: "24/11/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/RetroMania Wrestling Steam 2023 - Screenshots/20231124122845_1.jpg"
    path: "RetroMania Wrestling Steam 2023 - Screenshots"
  34999:
    filename: "20231124122909_1.jpg"
    date: "24/11/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/RetroMania Wrestling Steam 2023 - Screenshots/20231124122909_1.jpg"
    path: "RetroMania Wrestling Steam 2023 - Screenshots"
  35000:
    filename: "20231124123015_1.jpg"
    date: "24/11/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/RetroMania Wrestling Steam 2023 - Screenshots/20231124123015_1.jpg"
    path: "RetroMania Wrestling Steam 2023 - Screenshots"
  35001:
    filename: "20231124123023_1.jpg"
    date: "24/11/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/RetroMania Wrestling Steam 2023 - Screenshots/20231124123023_1.jpg"
    path: "RetroMania Wrestling Steam 2023 - Screenshots"
  35002:
    filename: "20231124123031_1.jpg"
    date: "24/11/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/RetroMania Wrestling Steam 2023 - Screenshots/20231124123031_1.jpg"
    path: "RetroMania Wrestling Steam 2023 - Screenshots"
  35003:
    filename: "20231124123247_1.jpg"
    date: "24/11/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/RetroMania Wrestling Steam 2023 - Screenshots/20231124123247_1.jpg"
    path: "RetroMania Wrestling Steam 2023 - Screenshots"
playlists:
  2329:
    title: "RetroMania Wrestling Steam 2023"
    publishedAt: "24/11/2023"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyKnZmb9n8kAUqC6K8qfAB1G" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "Steam"
updates:
  - "RetroMania Wrestling Steam 2023 - Screenshots"
  - "RetroMania Wrestling Steam 2023"
---
{% include 'article.html' %}