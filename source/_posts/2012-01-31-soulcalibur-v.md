---
title: "SoulCalibur V"
slug: "soulcalibur-v"
post_date: "31/01/2012"
files:
  35965:
    filename: "202401220212 (1).png"
    date: "22/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur V XB360 2024 - Screenshots/202401220212 (1).png"
    path: "SoulCalibur V XB360 2024 - Screenshots"
  35966:
    filename: "202401220212 (2).png"
    date: "22/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur V XB360 2024 - Screenshots/202401220212 (2).png"
    path: "SoulCalibur V XB360 2024 - Screenshots"
  35967:
    filename: "202401220212 (3).png"
    date: "22/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur V XB360 2024 - Screenshots/202401220212 (3).png"
    path: "SoulCalibur V XB360 2024 - Screenshots"
  35968:
    filename: "202401220212 (4).png"
    date: "22/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur V XB360 2024 - Screenshots/202401220212 (4).png"
    path: "SoulCalibur V XB360 2024 - Screenshots"
  35969:
    filename: "202401220212 (5).png"
    date: "22/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur V XB360 2024 - Screenshots/202401220212 (5).png"
    path: "SoulCalibur V XB360 2024 - Screenshots"
playlists:
  2469:
    title: "SoulCalibur V XB360 2024"
    publishedAt: "29/01/2024"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLI5c0AzOGsAMRtEdJM_HRq" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "XB360"
updates:
  - "SoulCalibur V XB360 2024 - Screenshots"
  - "SoulCalibur V XB360 2024"
---
{% include 'article.html' %}