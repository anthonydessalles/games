---
title: "Ridge Racer"
slug: "ridge-racer"
post_date: "01/09/2005"
files:
  20460:
    filename: "Ridge Racer-211001-235508.png"
    date: "02/10/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Ridge Racer PSP 2021 - Screenshots/Ridge Racer-211001-235508.png"
    path: "Ridge Racer PSP 2021 - Screenshots"
  20461:
    filename: "Ridge Racer-211001-235525.png"
    date: "02/10/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Ridge Racer PSP 2021 - Screenshots/Ridge Racer-211001-235525.png"
    path: "Ridge Racer PSP 2021 - Screenshots"
  20462:
    filename: "Ridge Racer-211001-235602.png"
    date: "02/10/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Ridge Racer PSP 2021 - Screenshots/Ridge Racer-211001-235602.png"
    path: "Ridge Racer PSP 2021 - Screenshots"
  20463:
    filename: "Ridge Racer-211001-235616.png"
    date: "02/10/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Ridge Racer PSP 2021 - Screenshots/Ridge Racer-211001-235616.png"
    path: "Ridge Racer PSP 2021 - Screenshots"
  20464:
    filename: "Ridge Racer-211001-235627.png"
    date: "02/10/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Ridge Racer PSP 2021 - Screenshots/Ridge Racer-211001-235627.png"
    path: "Ridge Racer PSP 2021 - Screenshots"
  20465:
    filename: "Ridge Racer-211001-235637.png"
    date: "02/10/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Ridge Racer PSP 2021 - Screenshots/Ridge Racer-211001-235637.png"
    path: "Ridge Racer PSP 2021 - Screenshots"
  20466:
    filename: "Ridge Racer-211001-235652.png"
    date: "02/10/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Ridge Racer PSP 2021 - Screenshots/Ridge Racer-211001-235652.png"
    path: "Ridge Racer PSP 2021 - Screenshots"
  20467:
    filename: "Ridge Racer-211001-235701.png"
    date: "02/10/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Ridge Racer PSP 2021 - Screenshots/Ridge Racer-211001-235701.png"
    path: "Ridge Racer PSP 2021 - Screenshots"
  20468:
    filename: "Ridge Racer-211001-235711.png"
    date: "02/10/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Ridge Racer PSP 2021 - Screenshots/Ridge Racer-211001-235711.png"
    path: "Ridge Racer PSP 2021 - Screenshots"
  20469:
    filename: "Ridge Racer-211001-235758.png"
    date: "02/10/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Ridge Racer PSP 2021 - Screenshots/Ridge Racer-211001-235758.png"
    path: "Ridge Racer PSP 2021 - Screenshots"
  20470:
    filename: "Ridge Racer-211001-235809.png"
    date: "02/10/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Ridge Racer PSP 2021 - Screenshots/Ridge Racer-211001-235809.png"
    path: "Ridge Racer PSP 2021 - Screenshots"
  20471:
    filename: "Ridge Racer-211001-235825.png"
    date: "02/10/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Ridge Racer PSP 2021 - Screenshots/Ridge Racer-211001-235825.png"
    path: "Ridge Racer PSP 2021 - Screenshots"
  20472:
    filename: "Ridge Racer-211001-235857.png"
    date: "02/10/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Ridge Racer PSP 2021 - Screenshots/Ridge Racer-211001-235857.png"
    path: "Ridge Racer PSP 2021 - Screenshots"
  20473:
    filename: "Ridge Racer-211001-235916.png"
    date: "02/10/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Ridge Racer PSP 2021 - Screenshots/Ridge Racer-211001-235916.png"
    path: "Ridge Racer PSP 2021 - Screenshots"
  20474:
    filename: "Ridge Racer-211001-235957.png"
    date: "02/10/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Ridge Racer PSP 2021 - Screenshots/Ridge Racer-211001-235957.png"
    path: "Ridge Racer PSP 2021 - Screenshots"
  20475:
    filename: "Ridge Racer-211002-000026.png"
    date: "02/10/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Ridge Racer PSP 2021 - Screenshots/Ridge Racer-211002-000026.png"
    path: "Ridge Racer PSP 2021 - Screenshots"
  20476:
    filename: "Ridge Racer-211002-000042.png"
    date: "02/10/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Ridge Racer PSP 2021 - Screenshots/Ridge Racer-211002-000042.png"
    path: "Ridge Racer PSP 2021 - Screenshots"
  20477:
    filename: "Ridge Racer-211002-000137.png"
    date: "02/10/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Ridge Racer PSP 2021 - Screenshots/Ridge Racer-211002-000137.png"
    path: "Ridge Racer PSP 2021 - Screenshots"
  20478:
    filename: "Ridge Racer-211002-000235.png"
    date: "02/10/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Ridge Racer PSP 2021 - Screenshots/Ridge Racer-211002-000235.png"
    path: "Ridge Racer PSP 2021 - Screenshots"
  20479:
    filename: "Ridge Racer-211002-000402.png"
    date: "02/10/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Ridge Racer PSP 2021 - Screenshots/Ridge Racer-211002-000402.png"
    path: "Ridge Racer PSP 2021 - Screenshots"
  20480:
    filename: "Ridge Racer-211002-000413.png"
    date: "02/10/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Ridge Racer PSP 2021 - Screenshots/Ridge Racer-211002-000413.png"
    path: "Ridge Racer PSP 2021 - Screenshots"
  20481:
    filename: "Ridge Racer-211002-000427.png"
    date: "02/10/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Ridge Racer PSP 2021 - Screenshots/Ridge Racer-211002-000427.png"
    path: "Ridge Racer PSP 2021 - Screenshots"
  20482:
    filename: "Ridge Racer-211002-000439.png"
    date: "02/10/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Ridge Racer PSP 2021 - Screenshots/Ridge Racer-211002-000439.png"
    path: "Ridge Racer PSP 2021 - Screenshots"
playlists:
tags:
  - "TonyTonyX042"
categories:
  - "PSP"
updates:
  - "Ridge Racer PSP 2021 - Screenshots"
---
{% include 'article.html' %}
