---
title: "X-Men Legends II Rise of Apocalypse"
french_title: "X-Men Legends II L'Avènement d'Apocalypse"
slug: "x-men-legends-ii-rise-of-apocalypse"
post_date: "14/10/2005"
files:
  20991:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214038.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214038.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  20992:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214053.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214053.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  20993:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214107.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214107.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  20994:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214122.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214122.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  20995:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214133.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214133.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  20996:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214143.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214143.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  20997:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214154.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214154.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  20998:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214206.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214206.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  20999:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214213.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214213.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21000:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214224.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214224.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21001:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214233.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214233.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21002:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214242.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214242.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21003:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214253.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214253.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21004:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214303.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214303.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21005:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214313.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214313.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21006:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214335.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214335.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21007:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214345.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214345.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21008:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214401.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214401.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21009:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214409.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214409.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21010:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214420.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214420.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21011:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214429.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214429.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21012:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214502.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214502.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21013:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214511.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214511.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21014:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214522.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214522.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21015:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214534.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214534.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21016:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214548.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214548.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21017:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214601.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214601.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21018:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214623.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214623.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21019:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214644.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214644.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21020:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214658.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214658.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21021:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214723.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214723.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21022:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214738.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214738.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21023:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214751.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214751.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21024:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214805.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214805.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21025:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214835.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214835.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21026:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214844.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214844.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21027:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214857.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214857.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21028:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214922.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214922.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21029:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214937.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214937.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21030:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214947.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214947.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21031:
    filename: "X-Men Legends II Rise of Apocalypse-211108-214958.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-214958.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21032:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215020.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215020.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21033:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215118.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215118.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21034:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215128.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215128.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21035:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215218.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215218.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21036:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215232.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215232.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21037:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215241.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215241.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21038:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215252.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215252.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21039:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215310.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215310.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21040:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215323.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215323.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21041:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215336.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215336.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21042:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215350.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215350.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21043:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215414.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215414.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21044:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215429.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215429.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21045:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215440.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215440.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21046:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215453.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215453.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21047:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215502.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215502.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21048:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215512.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215512.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21049:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215521.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215521.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21050:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215530.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215530.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21051:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215540.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215540.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21052:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215549.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215549.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21053:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215558.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215558.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21054:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215640.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215640.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21055:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215650.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215650.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21056:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215701.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215701.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21057:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215713.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215713.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21058:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215730.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215730.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21059:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215740.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215740.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21060:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215755.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215755.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21061:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215805.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215805.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21062:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215815.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215815.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21063:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215823.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215823.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21064:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215858.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215858.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  21065:
    filename: "X-Men Legends II Rise of Apocalypse-211108-215907.png"
    date: "08/11/2021"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots/X-Men Legends II Rise of Apocalypse-211108-215907.png"
    path: "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
playlists:
  924:
    title: "X-Men Legends II L'Avènement d'Apocalypse PS2 2020"
    publishedAt: "25/01/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyK_V80xUC-gUvv7zzDZa4aI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PSP"
  - "PS2"
updates:
  - "X-Men Legends II Rise of Apocalypse PSP 2021 - Screenshots"
  - "X-Men Legends II L'Avènement d'Apocalypse PS2 2020"
---
{% include 'article.html' %}
