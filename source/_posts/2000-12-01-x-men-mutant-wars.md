---
title: "X-Men Mutant Wars"
slug: "x-men-mutant-wars"
post_date: "01/12/2000"
files:
playlists:
  1565:
    title: "X-Men Mutant Wars GBC 2022"
    publishedAt: "07/04/2022"
    itemsCount: "3"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJXvoW8kzeJV4CcYINkJkQI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "GBC"
updates:
  - "X-Men Mutant Wars GBC 2022"
---
{% include 'article.html' %}
