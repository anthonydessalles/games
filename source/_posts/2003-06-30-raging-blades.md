---
title: "Raging Blades"
slug: "raging-blades"
post_date: "30/06/2003"
files:
playlists:
  909:
    title: "Raging Blades PS2 2021"
    publishedAt: "26/01/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyJUSRFB8D6csgotgfKblMWE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS2"
updates:
  - "Raging Blades PS2 2021"
---
{% include 'article.html' %}
