---
title: "Grand Theft Auto 2"
slug: "grand-theft-auto-2"
post_date: "22/10/1999"
files:
playlists:
  85:
    title: "Grand Theft Auto 2 PS1 2020"
    publishedAt: "12/09/2020"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyIWh6-ohnO81Lhadu6ZMTog" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS1"
updates:
  - "Grand Theft Auto 2 PS1 2020"
---
{% include 'article.html' %}
