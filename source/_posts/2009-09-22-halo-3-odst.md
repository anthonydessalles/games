---
title: "Halo 3 ODST"
slug: "halo-3-odst"
post_date: "22/09/2009"
files:
  36141:
    filename: "202401232231 (01).png"
    date: "23/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo 3 ODST XB360 2024 - Screenshots/202401232231 (01).png"
    path: "Halo 3 ODST XB360 2024 - Screenshots"
  36142:
    filename: "202401232231 (02).png"
    date: "23/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo 3 ODST XB360 2024 - Screenshots/202401232231 (02).png"
    path: "Halo 3 ODST XB360 2024 - Screenshots"
  36143:
    filename: "202401232231 (03).png"
    date: "23/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo 3 ODST XB360 2024 - Screenshots/202401232231 (03).png"
    path: "Halo 3 ODST XB360 2024 - Screenshots"
  36144:
    filename: "202401232231 (04).png"
    date: "23/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo 3 ODST XB360 2024 - Screenshots/202401232231 (04).png"
    path: "Halo 3 ODST XB360 2024 - Screenshots"
  36145:
    filename: "202401232231 (05).png"
    date: "23/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo 3 ODST XB360 2024 - Screenshots/202401232231 (05).png"
    path: "Halo 3 ODST XB360 2024 - Screenshots"
  36146:
    filename: "202401232231 (06).png"
    date: "23/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo 3 ODST XB360 2024 - Screenshots/202401232231 (06).png"
    path: "Halo 3 ODST XB360 2024 - Screenshots"
  36147:
    filename: "202401232231 (07).png"
    date: "23/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo 3 ODST XB360 2024 - Screenshots/202401232231 (07).png"
    path: "Halo 3 ODST XB360 2024 - Screenshots"
  36148:
    filename: "202401232231 (08).png"
    date: "23/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo 3 ODST XB360 2024 - Screenshots/202401232231 (08).png"
    path: "Halo 3 ODST XB360 2024 - Screenshots"
  36149:
    filename: "202401232231 (09).png"
    date: "23/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo 3 ODST XB360 2024 - Screenshots/202401232231 (09).png"
    path: "Halo 3 ODST XB360 2024 - Screenshots"
  36150:
    filename: "202401232231 (10).png"
    date: "23/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo 3 ODST XB360 2024 - Screenshots/202401232231 (10).png"
    path: "Halo 3 ODST XB360 2024 - Screenshots"
  36151:
    filename: "202401232231 (11).png"
    date: "23/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Halo 3 ODST XB360 2024 - Screenshots/202401232231 (11).png"
    path: "Halo 3 ODST XB360 2024 - Screenshots"
playlists:
tags:
  - "TonyTonyX042"
categories:
  - "XB360"
updates:
  - "Halo 3 ODST XB360 2024 - Screenshots"
---
{% include 'article.html' %}