---
title: "Disney's Aladdin"
french_title: "Aladdin"
slug: "disney-s-aladdin"
post_date: "11/11/1993"
files:
playlists:
  1027:
    title: "Aladdin GB 2021"
    publishedAt: "15/01/2021"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyIH2oJa7kNc1WSk0igF08cB" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "GB"
updates:
  - "Aladdin GB 2021"
---
{% include 'article.html' %}
