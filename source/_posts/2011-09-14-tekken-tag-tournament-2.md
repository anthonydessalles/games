---
title: "Tekken Tag Tournament 2"
slug: "tekken-tag-tournament-2"
post_date: "14/09/2011"
files:
  35970:
    filename: "202401220150 (1).png"
    date: "22/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Tag Tournament 2 XB360 2024 - Screenshots/202401220150 (1).png"
    path: "Tekken Tag Tournament 2 XB360 2024 - Screenshots"
  35971:
    filename: "202401220150 (2).png"
    date: "22/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Tag Tournament 2 XB360 2024 - Screenshots/202401220150 (2).png"
    path: "Tekken Tag Tournament 2 XB360 2024 - Screenshots"
  35972:
    filename: "202401220150 (3).png"
    date: "22/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Tag Tournament 2 XB360 2024 - Screenshots/202401220150 (3).png"
    path: "Tekken Tag Tournament 2 XB360 2024 - Screenshots"
  35973:
    filename: "202401220150 (4).png"
    date: "22/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Tag Tournament 2 XB360 2024 - Screenshots/202401220150 (4).png"
    path: "Tekken Tag Tournament 2 XB360 2024 - Screenshots"
  35974:
    filename: "202401220150 (5).png"
    date: "22/01/2024"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tekken Tag Tournament 2 XB360 2024 - Screenshots/202401220150 (5).png"
    path: "Tekken Tag Tournament 2 XB360 2024 - Screenshots"
playlists:
  2467:
    title: "Tekken Tag Tournament 2 XB360 2024"
    publishedAt: "29/01/2024"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyKiQNdXFrv1BMBVugE4Lpgn" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "XB360"
updates:
  - "Tekken Tag Tournament 2 XB360 2024 - Screenshots"
  - "Tekken Tag Tournament 2 XB360 2024"
---
{% include 'article.html' %}