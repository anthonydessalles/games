---
title: "X2 Wolverine's Revenge"
french_title: "X-Men 2 La Vengeance de Wolverine"
slug: "x2-wolverine-s-revenge"
post_date: "25/04/2003"
files:
playlists:
  926:
    title: "X-Men 2 La Vengeance de Wolverine PS2 2020"
    publishedAt: "25/01/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyIoeW7GvIxeA6CbUee3a8lw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS2"
updates:
  - "X-Men 2 La Vengeance de Wolverine PS2 2020"
---
{% include 'article.html' %}
