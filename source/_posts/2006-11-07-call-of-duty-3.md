---
title: "Call of Duty 3"
french_title: "Call of Duty 3 En Marche vers Paris"
slug: "call-of-duty-3"
post_date: "07/11/2006"
files:
playlists:
  1549:
    title: "Call of Duty 3 Wii 2022"
    publishedAt: "22/03/2022"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyKFpDkuCnqAiPcMNqJj7hy_" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "Wii"
updates:
  - "Call of Duty 3 Wii 2022"
---
{% include 'article.html' %}
