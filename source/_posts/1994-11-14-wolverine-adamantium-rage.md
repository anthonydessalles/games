---
title: "Wolverine Adamantium Rage"
slug: "wolverine-adamantium-rage"
post_date: "14/11/1994"
files:
  34061:
    filename: "Wolverine Adamantium Rage-231024-223339.png"
    date: "24/10/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wolverine Adamantium Rage SNES 2023 - Screenshots/Wolverine Adamantium Rage-231024-223339.png"
    path: "Wolverine Adamantium Rage SNES 2023 - Screenshots"
  34062:
    filename: "Wolverine Adamantium Rage-231024-223353.png"
    date: "24/10/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wolverine Adamantium Rage SNES 2023 - Screenshots/Wolverine Adamantium Rage-231024-223353.png"
    path: "Wolverine Adamantium Rage SNES 2023 - Screenshots"
  34063:
    filename: "Wolverine Adamantium Rage-231024-223406.png"
    date: "24/10/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wolverine Adamantium Rage SNES 2023 - Screenshots/Wolverine Adamantium Rage-231024-223406.png"
    path: "Wolverine Adamantium Rage SNES 2023 - Screenshots"
  34064:
    filename: "Wolverine Adamantium Rage-231024-223434.png"
    date: "24/10/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wolverine Adamantium Rage SNES 2023 - Screenshots/Wolverine Adamantium Rage-231024-223434.png"
    path: "Wolverine Adamantium Rage SNES 2023 - Screenshots"
  34065:
    filename: "Wolverine Adamantium Rage-231024-223447.png"
    date: "24/10/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wolverine Adamantium Rage SNES 2023 - Screenshots/Wolverine Adamantium Rage-231024-223447.png"
    path: "Wolverine Adamantium Rage SNES 2023 - Screenshots"
  34066:
    filename: "Wolverine Adamantium Rage-231024-223501.png"
    date: "24/10/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wolverine Adamantium Rage SNES 2023 - Screenshots/Wolverine Adamantium Rage-231024-223501.png"
    path: "Wolverine Adamantium Rage SNES 2023 - Screenshots"
  34067:
    filename: "Wolverine Adamantium Rage-231024-223511.png"
    date: "24/10/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wolverine Adamantium Rage SNES 2023 - Screenshots/Wolverine Adamantium Rage-231024-223511.png"
    path: "Wolverine Adamantium Rage SNES 2023 - Screenshots"
  34068:
    filename: "Wolverine Adamantium Rage-231024-223522.png"
    date: "24/10/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wolverine Adamantium Rage SNES 2023 - Screenshots/Wolverine Adamantium Rage-231024-223522.png"
    path: "Wolverine Adamantium Rage SNES 2023 - Screenshots"
  34069:
    filename: "Wolverine Adamantium Rage-231024-223531.png"
    date: "24/10/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wolverine Adamantium Rage SNES 2023 - Screenshots/Wolverine Adamantium Rage-231024-223531.png"
    path: "Wolverine Adamantium Rage SNES 2023 - Screenshots"
  34070:
    filename: "Wolverine Adamantium Rage-231024-223545.png"
    date: "24/10/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wolverine Adamantium Rage SNES 2023 - Screenshots/Wolverine Adamantium Rage-231024-223545.png"
    path: "Wolverine Adamantium Rage SNES 2023 - Screenshots"
  34071:
    filename: "Wolverine Adamantium Rage-231024-223616.png"
    date: "24/10/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wolverine Adamantium Rage SNES 2023 - Screenshots/Wolverine Adamantium Rage-231024-223616.png"
    path: "Wolverine Adamantium Rage SNES 2023 - Screenshots"
  34072:
    filename: "Wolverine Adamantium Rage-231024-223632.png"
    date: "24/10/2023"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Wolverine Adamantium Rage SNES 2023 - Screenshots/Wolverine Adamantium Rage-231024-223632.png"
    path: "Wolverine Adamantium Rage SNES 2023 - Screenshots"
playlists:
tags:
  - "TonyTonyX042"
categories:
  - "SNES"
updates:
  - "Wolverine Adamantium Rage SNES 2023 - Screenshots"
---
{% include 'article.html' %}