---
title: "Tomb Raider III Adventures of Lara Croft"
french_title: "Tomb Raider III Les Aventures de Lara Croft"
slug: "tomb-raider-iii-adventures-of-lara-croft"
post_date: "20/11/1998"
files:
  11750:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172536.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172536.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11751:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172550.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172550.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11752:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172612.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172612.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11753:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172621.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172621.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11754:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172628.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172628.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11755:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172634.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172634.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11756:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172642.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172642.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11757:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172648.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172648.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11758:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172655.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172655.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11759:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172704.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172704.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11760:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172713.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172713.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11761:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172718.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172718.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11762:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172726.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172726.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11763:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172737.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172737.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11764:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172756.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172756.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11765:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172805.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172805.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11766:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172816.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172816.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11767:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172830.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172830.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11768:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172839.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172839.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11769:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172849.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172849.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11770:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172856.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172856.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11771:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172906.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172906.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11772:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172915.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172915.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11773:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172922.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172922.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11774:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172928.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172928.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11775:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172933.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172933.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11776:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172945.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172945.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11777:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172952.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172952.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11778:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-172957.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-172957.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11779:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173008.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173008.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11780:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173013.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173013.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11781:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173019.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173019.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11782:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173025.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173025.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11783:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173034.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173034.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11784:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173042.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173042.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11785:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173047.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173047.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11786:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173052.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173052.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11787:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173059.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173059.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11788:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173104.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173104.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11789:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173121.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173121.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11790:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173130.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173130.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11791:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173140.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173140.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11792:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173155.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173155.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11793:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173205.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173205.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11794:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173216.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173216.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11795:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173229.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173229.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11796:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173239.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173239.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11797:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173247.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173247.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11798:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173333.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173333.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11799:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173346.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173346.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11800:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173406.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173406.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11801:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173422.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173422.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11802:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173436.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173436.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11803:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173501.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173501.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11804:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173513.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173513.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11805:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173520.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173520.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11806:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173536.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173536.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11807:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173613.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173613.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
  11808:
    filename: "Tomb Raider III Adventures of Lara Croft-201127-173636.png"
    date: "27/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots/Tomb Raider III Adventures of Lara Croft-201127-173636.png"
    path: "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
playlists:
tags:
  - "TonyTonyX042"
categories:
  - "PS1"
updates:
  - "Tomb Raider III Adventures of Lara Croft PS1 2020 - Screenshots"
---
{% include 'article.html' %}
