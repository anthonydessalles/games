---
title: "WWE Legends of WrestleMania"
slug: "wwe-legends-of-wrestlemania"
post_date: "19/03/2009"
files:
  22816:
    filename: "2022_5_7_10_18_26.bmp"
    date: "07/05/2022"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/WWE Legends of WrestleMania PS3 2022 - Screenshots/2022_5_7_10_18_26.bmp"
    path: "WWE Legends of WrestleMania PS3 2022 - Screenshots"
  22817:
    filename: "2022_5_7_10_18_30.bmp"
    date: "07/05/2022"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/WWE Legends of WrestleMania PS3 2022 - Screenshots/2022_5_7_10_18_30.bmp"
    path: "WWE Legends of WrestleMania PS3 2022 - Screenshots"
  22818:
    filename: "2022_5_7_10_18_39.bmp"
    date: "07/05/2022"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/WWE Legends of WrestleMania PS3 2022 - Screenshots/2022_5_7_10_18_39.bmp"
    path: "WWE Legends of WrestleMania PS3 2022 - Screenshots"
  22819:
    filename: "2022_5_7_10_18_41.bmp"
    date: "07/05/2022"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/WWE Legends of WrestleMania PS3 2022 - Screenshots/2022_5_7_10_18_41.bmp"
    path: "WWE Legends of WrestleMania PS3 2022 - Screenshots"
  22820:
    filename: "2022_5_7_10_18_47.bmp"
    date: "07/05/2022"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/WWE Legends of WrestleMania PS3 2022 - Screenshots/2022_5_7_10_18_47.bmp"
    path: "WWE Legends of WrestleMania PS3 2022 - Screenshots"
playlists:
  962:
    title: "WWE Legends of WrestleMania PS3 2020"
    publishedAt: "25/01/2021"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLi575OvzpQZ23JGmbMHWZc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  1669:
    title: "WWE Legends of WrestleMania PS3 2022"
    publishedAt: "17/05/2022"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLwh23OspdVO93TCWjSq600" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "PS3"
updates:
  - "WWE Legends of WrestleMania PS3 2022 - Screenshots"
  - "WWE Legends of WrestleMania PS3 2020"
  - "WWE Legends of WrestleMania PS3 2022"
---
{% include 'article.html' %}
