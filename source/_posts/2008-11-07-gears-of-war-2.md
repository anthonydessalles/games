---
title: "Gears of War 2"
slug: "gears-of-war-2"
post_date: "07/11/2008"
files:
playlists:
  2073:
    title: "Gears of War 2 XB360 2023"
    publishedAt: "23/06/2023"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyLpLb--AuZQjksYLGxlZYvZ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "XB360"
updates:
  - "Gears of War 2 XB360 2023"
---
{% include 'article.html' %}
