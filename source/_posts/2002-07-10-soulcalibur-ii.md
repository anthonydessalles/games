---
title: "SoulCalibur II"
slug: "soulcalibur-ii"
post_date: "10/07/2002"
files:
  14704:
    filename: "2020_12_21_2_10_3.bmp"
    date: "21/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur II GC 2020 - Screenshots/2020_12_21_2_10_3.bmp"
    path: "SoulCalibur II GC 2020 - Screenshots"
  14705:
    filename: "2020_12_21_2_10_51.bmp"
    date: "21/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur II GC 2020 - Screenshots/2020_12_21_2_10_51.bmp"
    path: "SoulCalibur II GC 2020 - Screenshots"
  14706:
    filename: "2020_12_21_2_10_58.bmp"
    date: "21/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur II GC 2020 - Screenshots/2020_12_21_2_10_58.bmp"
    path: "SoulCalibur II GC 2020 - Screenshots"
  14707:
    filename: "2020_12_21_2_10_8.bmp"
    date: "21/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur II GC 2020 - Screenshots/2020_12_21_2_10_8.bmp"
    path: "SoulCalibur II GC 2020 - Screenshots"
  14708:
    filename: "2020_12_21_2_11_2.bmp"
    date: "21/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur II GC 2020 - Screenshots/2020_12_21_2_11_2.bmp"
    path: "SoulCalibur II GC 2020 - Screenshots"
  14709:
    filename: "2020_12_21_2_11_28.bmp"
    date: "21/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur II GC 2020 - Screenshots/2020_12_21_2_11_28.bmp"
    path: "SoulCalibur II GC 2020 - Screenshots"
  14710:
    filename: "2020_12_21_2_11_34.bmp"
    date: "21/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur II GC 2020 - Screenshots/2020_12_21_2_11_34.bmp"
    path: "SoulCalibur II GC 2020 - Screenshots"
  14711:
    filename: "2020_12_21_2_11_8.bmp"
    date: "21/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur II GC 2020 - Screenshots/2020_12_21_2_11_8.bmp"
    path: "SoulCalibur II GC 2020 - Screenshots"
  14712:
    filename: "2020_12_21_2_9_53.bmp"
    date: "21/12/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/SoulCalibur II GC 2020 - Screenshots/2020_12_21_2_9_53.bmp"
    path: "SoulCalibur II GC 2020 - Screenshots"
playlists:
  983:
    title: "SoulCalibur II GC 2020"
    publishedAt: "22/01/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyKa8GYLnaqDVss9hPU6kUc7" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  982:
    title: "SoulCalibur II PS2 2020"
    publishedAt: "22/01/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyK-X1RIYczBx9dLxYw09lQs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  981:
    title: "SoulCalibur II XB 2020"
    publishedAt: "22/01/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyInlGgiZONNxWObNsdUWC0A" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "GC"
  - "PS2"
  - "XB"
updates:
  - "SoulCalibur II GC 2020 - Screenshots"
  - "SoulCalibur II GC 2020"
  - "SoulCalibur II PS2 2020"
  - "SoulCalibur II XB 2020"
---
{% include 'article.html' %}
