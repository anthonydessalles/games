---
title: "Dragon Ball Z Budokai 2"
slug: "dragon-ball-z-budokai-2"
post_date: "13/11/2003"
files:
playlists:
  1013:
    title: "Dragon Ball Z Budokai 2 GC 2020"
    publishedAt: "20/01/2021"
    itemsCount: "2"
    embedHtml: <iframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PLhfLdgVmwjyKCu0rNRwO_5CIRLkF8Ul3j" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
tags:
  - "TonyTonyX042"
categories:
  - "GC"
updates:
  - "Dragon Ball Z Budokai 2 GC 2020"
---
{% include 'article.html' %}
