---
title: "Asterix The Gallic War"
french_title: "Astérix"
slug: "asterix-the-gallic-war"
post_date: "01/04/1999"
files:
  4877:
    filename: "Astérix-201121-180724.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-180724.png"
    path: "Astérix PS1 2020 - Screenshots"
  4878:
    filename: "Astérix-201121-180730.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-180730.png"
    path: "Astérix PS1 2020 - Screenshots"
  4879:
    filename: "Astérix-201121-180737.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-180737.png"
    path: "Astérix PS1 2020 - Screenshots"
  4880:
    filename: "Astérix-201121-180758.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-180758.png"
    path: "Astérix PS1 2020 - Screenshots"
  4881:
    filename: "Astérix-201121-180805.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-180805.png"
    path: "Astérix PS1 2020 - Screenshots"
  4882:
    filename: "Astérix-201121-180814.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-180814.png"
    path: "Astérix PS1 2020 - Screenshots"
  4883:
    filename: "Astérix-201121-180823.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-180823.png"
    path: "Astérix PS1 2020 - Screenshots"
  4884:
    filename: "Astérix-201121-180832.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-180832.png"
    path: "Astérix PS1 2020 - Screenshots"
  4885:
    filename: "Astérix-201121-180845.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-180845.png"
    path: "Astérix PS1 2020 - Screenshots"
  4886:
    filename: "Astérix-201121-180854.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-180854.png"
    path: "Astérix PS1 2020 - Screenshots"
  4887:
    filename: "Astérix-201121-180904.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-180904.png"
    path: "Astérix PS1 2020 - Screenshots"
  4888:
    filename: "Astérix-201121-180912.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-180912.png"
    path: "Astérix PS1 2020 - Screenshots"
  4889:
    filename: "Astérix-201121-180920.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-180920.png"
    path: "Astérix PS1 2020 - Screenshots"
  4890:
    filename: "Astérix-201121-180928.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-180928.png"
    path: "Astérix PS1 2020 - Screenshots"
  4891:
    filename: "Astérix-201121-180934.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-180934.png"
    path: "Astérix PS1 2020 - Screenshots"
  4892:
    filename: "Astérix-201121-180944.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-180944.png"
    path: "Astérix PS1 2020 - Screenshots"
  4893:
    filename: "Astérix-201121-180952.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-180952.png"
    path: "Astérix PS1 2020 - Screenshots"
  4894:
    filename: "Astérix-201121-181013.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181013.png"
    path: "Astérix PS1 2020 - Screenshots"
  4895:
    filename: "Astérix-201121-181049.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181049.png"
    path: "Astérix PS1 2020 - Screenshots"
  4896:
    filename: "Astérix-201121-181106.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181106.png"
    path: "Astérix PS1 2020 - Screenshots"
  4897:
    filename: "Astérix-201121-181126.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181126.png"
    path: "Astérix PS1 2020 - Screenshots"
  4898:
    filename: "Astérix-201121-181139.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181139.png"
    path: "Astérix PS1 2020 - Screenshots"
  4899:
    filename: "Astérix-201121-181202.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181202.png"
    path: "Astérix PS1 2020 - Screenshots"
  4900:
    filename: "Astérix-201121-181213.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181213.png"
    path: "Astérix PS1 2020 - Screenshots"
  4901:
    filename: "Astérix-201121-181237.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181237.png"
    path: "Astérix PS1 2020 - Screenshots"
  4902:
    filename: "Astérix-201121-181243.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181243.png"
    path: "Astérix PS1 2020 - Screenshots"
  4903:
    filename: "Astérix-201121-181250.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181250.png"
    path: "Astérix PS1 2020 - Screenshots"
  4904:
    filename: "Astérix-201121-181311.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181311.png"
    path: "Astérix PS1 2020 - Screenshots"
  4905:
    filename: "Astérix-201121-181336.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181336.png"
    path: "Astérix PS1 2020 - Screenshots"
  4906:
    filename: "Astérix-201121-181342.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181342.png"
    path: "Astérix PS1 2020 - Screenshots"
  4907:
    filename: "Astérix-201121-181351.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181351.png"
    path: "Astérix PS1 2020 - Screenshots"
  4908:
    filename: "Astérix-201121-181359.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181359.png"
    path: "Astérix PS1 2020 - Screenshots"
  4909:
    filename: "Astérix-201121-181417.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181417.png"
    path: "Astérix PS1 2020 - Screenshots"
  4910:
    filename: "Astérix-201121-181436.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181436.png"
    path: "Astérix PS1 2020 - Screenshots"
  4911:
    filename: "Astérix-201121-181449.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181449.png"
    path: "Astérix PS1 2020 - Screenshots"
  4912:
    filename: "Astérix-201121-181456.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181456.png"
    path: "Astérix PS1 2020 - Screenshots"
  4913:
    filename: "Astérix-201121-181503.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181503.png"
    path: "Astérix PS1 2020 - Screenshots"
  4914:
    filename: "Astérix-201121-181510.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181510.png"
    path: "Astérix PS1 2020 - Screenshots"
  4915:
    filename: "Astérix-201121-181525.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181525.png"
    path: "Astérix PS1 2020 - Screenshots"
  4916:
    filename: "Astérix-201121-181534.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181534.png"
    path: "Astérix PS1 2020 - Screenshots"
  4917:
    filename: "Astérix-201121-181548.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181548.png"
    path: "Astérix PS1 2020 - Screenshots"
  4918:
    filename: "Astérix-201121-181555.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181555.png"
    path: "Astérix PS1 2020 - Screenshots"
  4919:
    filename: "Astérix-201121-181605.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181605.png"
    path: "Astérix PS1 2020 - Screenshots"
  4920:
    filename: "Astérix-201121-181629.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181629.png"
    path: "Astérix PS1 2020 - Screenshots"
  4921:
    filename: "Astérix-201121-181651.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181651.png"
    path: "Astérix PS1 2020 - Screenshots"
  4922:
    filename: "Astérix-201121-181712.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181712.png"
    path: "Astérix PS1 2020 - Screenshots"
  4923:
    filename: "Astérix-201121-181718.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181718.png"
    path: "Astérix PS1 2020 - Screenshots"
  4924:
    filename: "Astérix-201121-181725.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181725.png"
    path: "Astérix PS1 2020 - Screenshots"
  4925:
    filename: "Astérix-201121-181740.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181740.png"
    path: "Astérix PS1 2020 - Screenshots"
  4926:
    filename: "Astérix-201121-181749.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181749.png"
    path: "Astérix PS1 2020 - Screenshots"
  4927:
    filename: "Astérix-201121-181810.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181810.png"
    path: "Astérix PS1 2020 - Screenshots"
  4928:
    filename: "Astérix-201121-181818.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181818.png"
    path: "Astérix PS1 2020 - Screenshots"
  4929:
    filename: "Astérix-201121-181915.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181915.png"
    path: "Astérix PS1 2020 - Screenshots"
  4930:
    filename: "Astérix-201121-181942.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181942.png"
    path: "Astérix PS1 2020 - Screenshots"
  4931:
    filename: "Astérix-201121-181948.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-181948.png"
    path: "Astérix PS1 2020 - Screenshots"
  4932:
    filename: "Astérix-201121-182007.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-182007.png"
    path: "Astérix PS1 2020 - Screenshots"
  4933:
    filename: "Astérix-201121-182022.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-182022.png"
    path: "Astérix PS1 2020 - Screenshots"
  4934:
    filename: "Astérix-201121-182034.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-182034.png"
    path: "Astérix PS1 2020 - Screenshots"
  4935:
    filename: "Astérix-201121-182042.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-182042.png"
    path: "Astérix PS1 2020 - Screenshots"
  4936:
    filename: "Astérix-201121-182054.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-182054.png"
    path: "Astérix PS1 2020 - Screenshots"
  4937:
    filename: "Astérix-201121-182101.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-182101.png"
    path: "Astérix PS1 2020 - Screenshots"
  4938:
    filename: "Astérix-201121-182119.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-182119.png"
    path: "Astérix PS1 2020 - Screenshots"
  4939:
    filename: "Astérix-201121-182140.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-182140.png"
    path: "Astérix PS1 2020 - Screenshots"
  4940:
    filename: "Astérix-201121-182147.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-182147.png"
    path: "Astérix PS1 2020 - Screenshots"
  4941:
    filename: "Astérix-201121-182152.png"
    date: "21/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Astérix PS1 2020 - Screenshots/Astérix-201121-182152.png"
    path: "Astérix PS1 2020 - Screenshots"
playlists:
tags:
  - "TonyTonyX042"
categories:
  - "PS1"
updates:
  - "Astérix PS1 2020 - Screenshots"
---
{% include 'article.html' %}
