---
title: "Sonic the Hedgehog 2"
slug: "sonic-the-hedgehog-2"
post_date: "16/10/1992"
files:
  8752:
    filename: "Sonic the Hedgehog 2-201117-120026.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog 2 MD 2020 - Screenshots/Sonic the Hedgehog 2-201117-120026.png"
    path: "Sonic the Hedgehog 2 MD 2020 - Screenshots"
  8753:
    filename: "Sonic the Hedgehog 2-201117-120031.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog 2 MD 2020 - Screenshots/Sonic the Hedgehog 2-201117-120031.png"
    path: "Sonic the Hedgehog 2 MD 2020 - Screenshots"
  8754:
    filename: "Sonic the Hedgehog 2-201117-120037.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog 2 MD 2020 - Screenshots/Sonic the Hedgehog 2-201117-120037.png"
    path: "Sonic the Hedgehog 2 MD 2020 - Screenshots"
  8755:
    filename: "Sonic the Hedgehog 2-201117-120045.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog 2 MD 2020 - Screenshots/Sonic the Hedgehog 2-201117-120045.png"
    path: "Sonic the Hedgehog 2 MD 2020 - Screenshots"
  8756:
    filename: "Sonic the Hedgehog 2-201117-120050.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog 2 MD 2020 - Screenshots/Sonic the Hedgehog 2-201117-120050.png"
    path: "Sonic the Hedgehog 2 MD 2020 - Screenshots"
  8757:
    filename: "Sonic the Hedgehog 2-201117-120058.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog 2 MD 2020 - Screenshots/Sonic the Hedgehog 2-201117-120058.png"
    path: "Sonic the Hedgehog 2 MD 2020 - Screenshots"
  8758:
    filename: "Sonic the Hedgehog 2-201117-120104.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog 2 MD 2020 - Screenshots/Sonic the Hedgehog 2-201117-120104.png"
    path: "Sonic the Hedgehog 2 MD 2020 - Screenshots"
  8759:
    filename: "Sonic the Hedgehog 2-201117-120111.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog 2 MD 2020 - Screenshots/Sonic the Hedgehog 2-201117-120111.png"
    path: "Sonic the Hedgehog 2 MD 2020 - Screenshots"
  8760:
    filename: "Sonic the Hedgehog 2-201117-120124.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog 2 MD 2020 - Screenshots/Sonic the Hedgehog 2-201117-120124.png"
    path: "Sonic the Hedgehog 2 MD 2020 - Screenshots"
  8761:
    filename: "Sonic the Hedgehog 2-201117-120136.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog 2 MD 2020 - Screenshots/Sonic the Hedgehog 2-201117-120136.png"
    path: "Sonic the Hedgehog 2 MD 2020 - Screenshots"
  8762:
    filename: "Sonic the Hedgehog 2-201117-120143.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog 2 MD 2020 - Screenshots/Sonic the Hedgehog 2-201117-120143.png"
    path: "Sonic the Hedgehog 2 MD 2020 - Screenshots"
  8763:
    filename: "Sonic the Hedgehog 2-201117-120159.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog 2 MD 2020 - Screenshots/Sonic the Hedgehog 2-201117-120159.png"
    path: "Sonic the Hedgehog 2 MD 2020 - Screenshots"
  8764:
    filename: "Sonic the Hedgehog 2-201117-120252.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog 2 MD 2020 - Screenshots/Sonic the Hedgehog 2-201117-120252.png"
    path: "Sonic the Hedgehog 2 MD 2020 - Screenshots"
  8765:
    filename: "Sonic the Hedgehog 2-201117-120309.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog 2 MD 2020 - Screenshots/Sonic the Hedgehog 2-201117-120309.png"
    path: "Sonic the Hedgehog 2 MD 2020 - Screenshots"
  8766:
    filename: "Sonic the Hedgehog 2-201117-120354.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog 2 MD 2020 - Screenshots/Sonic the Hedgehog 2-201117-120354.png"
    path: "Sonic the Hedgehog 2 MD 2020 - Screenshots"
  8767:
    filename: "Sonic the Hedgehog 2-201117-120403.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog 2 MD 2020 - Screenshots/Sonic the Hedgehog 2-201117-120403.png"
    path: "Sonic the Hedgehog 2 MD 2020 - Screenshots"
  8768:
    filename: "Sonic the Hedgehog 2-201117-120410.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog 2 MD 2020 - Screenshots/Sonic the Hedgehog 2-201117-120410.png"
    path: "Sonic the Hedgehog 2 MD 2020 - Screenshots"
  8769:
    filename: "Sonic the Hedgehog 2-201117-120419.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog 2 MD 2020 - Screenshots/Sonic the Hedgehog 2-201117-120419.png"
    path: "Sonic the Hedgehog 2 MD 2020 - Screenshots"
  8770:
    filename: "Sonic the Hedgehog 2-201117-120429.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog 2 MD 2020 - Screenshots/Sonic the Hedgehog 2-201117-120429.png"
    path: "Sonic the Hedgehog 2 MD 2020 - Screenshots"
  8771:
    filename: "Sonic the Hedgehog 2-201117-120436.png"
    date: "17/11/2020"
    slug: "https://s3.eu-west-3.amazonaws.com/games.anthony-dessalles.com/Sonic the Hedgehog 2 MD 2020 - Screenshots/Sonic the Hedgehog 2-201117-120436.png"
    path: "Sonic the Hedgehog 2 MD 2020 - Screenshots"
playlists:
tags:
  - "TonyTonyX042"
categories:
  - "MD"
updates:
  - "Sonic the Hedgehog 2 MD 2020 - Screenshots"
---
{% include 'article.html' %}
