Games
=====================
Powered by [Sculpin](http://sculpin.io).

Build
-----

### Dev

    make start

The website is accessible at `http://localhost:8000/`.

### Deploy

    git add -A
    git commit -m "v..."
    make deploy

The website is accessible at `http://games.anthony-dessalles.com`.